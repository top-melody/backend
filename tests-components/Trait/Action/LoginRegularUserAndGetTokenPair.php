<?php

namespace TestsComponents\Trait\Action;

use App\Api\V1\Application\Request\User as Request;
use App\Api\V1\Domain\DTO\TokenPair;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use TestsComponents\Data;
use TestsComponents\Trait;

trait LoginRegularUserAndGetTokenPair
{
    private function loginRegularUserAndGetTokenPair(KernelBrowser $kernelBrowser): TokenPair
    {
        $request = new Request\Login();
        $request->email = Data\RegularUser::EMAIL;
        $request->password = Data\RegularUser::PASSWORD;

        $kernelBrowser->request('POST', Data\V1\Url::USER_LOGIN, (array) $request);
        $this->assertResponseIsSuccessful();
        $content = json_decode($kernelBrowser->getResponse()->getContent(), true);

        $tokenPair = new TokenPair();
        $tokenPair->token = $content['token'];
        $tokenPair->refreshToken = $content['refreshToken'];

        return $tokenPair;
    }
}
