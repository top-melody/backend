<?php

namespace TestsComponents\Trait\Entity\Create;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use App\Shared\Domain\Enum as SharedDomainEnum;
use TestsComponents\Data;

trait ErrorOperationScan
{
    private function createErrorOperationScan(EntityManagerInterface $entityManager, Entity\Integration $integration, Entity\User $user): void
    {
        $entity = new Entity\OperationScan();
        $entity->setStatus(SharedDomainEnum\OperationStatus::CompletedWithError->value);
        $entity->setErrors([Data\ErrorOperationScan::ERROR]);
        $entity->setStackTrace(Data\ErrorOperationScan::STACK_TRACE);
        $entity->setIntegration($integration);
        $entity->setUser($user);

        $entityManager->persist($entity);
        $entityManager->flush();
    }
}
