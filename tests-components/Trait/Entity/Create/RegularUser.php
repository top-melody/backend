<?php

namespace TestsComponents\Trait\Entity\Create;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use TestsComponents\Data;

trait RegularUser
{
    private function createRegularUser(EntityManagerInterface $entityManager, UserPasswordHasherInterface $hasher): void
    {
        $user = new Entity\User();
        $user->setEmail(Data\RegularUser::EMAIL);
        $user->setPassword($hasher->hashPassword($user, Data\RegularUser::PASSWORD));

        $entityManager->persist($user);
        $entityManager->flush();
    }
}
