<?php

namespace TestsComponents\Trait\Entity\Create;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait RegularPlaylist
{
    private function createRegularPlaylist(EntityManagerInterface $entityManager, ?Entity\User $user = null): void
    {
        $entity = new Entity\Playlist();
        $entity->setName(Data\RegularPlaylist::NAME);

        if ($user) {
            $entity->addUser($user);
        }

        $entityManager->persist($entity);
        $entityManager->flush();

        if ($user) {
            $user->addPlaylist($entity);

            $entityManager->persist($user);
            $entityManager->flush();
        }
    }
}
