<?php

namespace TestsComponents\Trait\Entity\Create;

use App\Api\V1\Domain\Enum as ApiV1DomainEnum;
use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;

trait TrackToPlaylist
{
    private function createTrackToPlaylist(EntityManagerInterface $entityManager, Entity\Track $track, Entity\Playlist $playlist): void
    {
        $entity = new Entity\TrackToPlaylist();
        $entity->setTrack($track);
        $entity->setPlaylist($playlist);

        $entityManager->persist($entity);
        $entityManager->flush();
    }
}
