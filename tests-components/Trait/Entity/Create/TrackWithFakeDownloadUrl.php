<?php

namespace TestsComponents\Trait\Entity\Create;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait TrackWithFakeDownloadUrl
{
    private function createTrackWithFakeDownloadUrl(EntityManagerInterface $entityManager, Entity\Integration $integration, ?Entity\Author $author = null): void
    {
        $track = new Entity\Track();
        $track->setIntegration($integration);
        $track->setName(Data\TrackWithFakeDownloadUrl::NAME);
        $track->setDuration(Data\TrackWithFakeDownloadUrl::DURATION);
        $track->setDownloadUrl(Data\TrackWithFakeDownloadUrl::DOWNLOAD_URL);

        if ($author) {
            $track->addAuthor($author);
        }

        $entityManager->persist($track);
        $entityManager->flush();
    }
}
