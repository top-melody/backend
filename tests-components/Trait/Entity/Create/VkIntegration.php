<?php

namespace TestsComponents\Trait\Entity\Create;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait VkIntegration
{
    private function createVkIntegration(EntityManagerInterface $entityManager): void
    {
        $entity = new Entity\Integration();
        $entity->setName(Data\VkIntegration::NAME);
        $entity->setConfig(Data\VkIntegration::CONFIG);
        $entity->setClass(Data\VkIntegration::CLASS_PATH);

        $entityManager->persist($entity);
        $entityManager->flush();
    }
}
