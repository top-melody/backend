<?php

namespace TestsComponents\Trait\Entity\Create;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait TrackWithIntegrationWhereFakeClassPath
{
    private function createTrackWithIntegrationWhereFakeClassPath(EntityManagerInterface $entityManager, Entity\Integration $integration, ?Entity\Author $author = null): void
    {
        $track = new Entity\Track();
        $track->setIntegration($integration);
        $track->setName(Data\TrackWithIntegrationWhereFakeClassPath::NAME);
        $track->setDuration(Data\TrackWithIntegrationWhereFakeClassPath::DURATION);
        $track->setDownloadUrl(Data\TrackWithIntegrationWhereFakeClassPath::DOWNLOAD_URL);

        if ($author) {
            $track->addAuthor($author);
        }

        $entityManager->persist($track);
        $entityManager->flush();
    }
}
