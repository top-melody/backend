<?php

namespace TestsComponents\Trait\Entity\Create;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait Author
{
    private function createAuthor(EntityManagerInterface $entityManager): void
    {
        $track = new Entity\Author();
        $track->setName(Data\Author::NAME);

        $entityManager->persist($track);
        $entityManager->flush();
    }
}
