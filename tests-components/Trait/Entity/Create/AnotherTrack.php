<?php

namespace TestsComponents\Trait\Entity\Create;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait AnotherTrack
{
    private function createAnotherTrack(EntityManagerInterface $entityManager, Entity\Integration $integration, ?Entity\Author $author = null): void
    {
        $track = new Entity\Track();
        $track->setIntegration($integration);
        $track->setName(Data\AnotherTrack::NAME);
        $track->setDuration(Data\AnotherTrack::DURATION);
        $track->setDownloadUrl(Data\AnotherTrack::DOWNLOAD_URL);

        if ($author) {
            $track->addAuthor($author);
        }

        $entityManager->persist($track);
        $entityManager->flush();
    }
}
