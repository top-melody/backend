<?php

namespace TestsComponents\Trait\Entity\Create;

use App\Shared\Domain\Enum as SharedDomainEnum;
use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;

trait MyPlaylist
{
    private function createMyPlaylist(EntityManagerInterface $entityManager, Entity\User $user): void
    {
        $entity = new Entity\Playlist();
        $entity->setName(SharedDomainEnum\Playlist::My->value);
        $entity->addUser($user);

        $entityManager->persist($entity);
        $entityManager->flush();

        $user->addPlaylist($entity);

        $entityManager->persist($user);
        $entityManager->flush();
    }
}
