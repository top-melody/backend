<?php

namespace TestsComponents\Trait\Entity\Create;

use Doctrine\ORM\EntityManagerInterface;
use App\Shared\Domain\Entity;

trait UserIntegrationConfig
{
    private function createUserIntegrationConfig(EntityManagerInterface $entityManager, Entity\Integration $integration, Entity\User $user): void
    {
        $entity = new Entity\UserIntegrationConfig();
        $entity->setUser($user);
        $entity->setIntegration($integration);
        $entity->setConfig(['userUrl' => 'https://vk.com/id146590181']);

        $entityManager->persist($entity);
        $entityManager->flush();
    }
}
