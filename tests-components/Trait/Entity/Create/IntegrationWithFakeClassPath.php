<?php

namespace TestsComponents\Trait\Entity\Create;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait IntegrationWithFakeClassPath
{
    private function createIntegrationWithFakeClassPath(EntityManagerInterface $entityManager): void
    {
        $entity = new Entity\Integration();
        $entity->setName(Data\IntegrationWithFakeClassPath::NAME);
        $entity->setConfig(Data\IntegrationWithFakeClassPath::CONFIG);
        $entity->setClass(Data\IntegrationWithFakeClassPath::CLASS_PATH);

        $entityManager->persist($entity);
        $entityManager->flush();
    }
}
