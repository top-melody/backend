<?php

namespace TestsComponents\Trait\Entity\Create;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait PromoDjIntegration
{
    private function createPromoDjIntegration(EntityManagerInterface $entityManager): void
    {
        $entity = new Entity\Integration();
        $entity->setName(Data\PromoDjIntegration::NAME);
        $entity->setConfig(Data\PromoDjIntegration::CONFIG);
        $entity->setClass(Data\PromoDjIntegration::CLASS_PATH);

        $entityManager->persist($entity);
        $entityManager->flush();
    }
}
