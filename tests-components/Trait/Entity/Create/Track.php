<?php

namespace TestsComponents\Trait\Entity\Create;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait Track
{
    private function createTrack(EntityManagerInterface $entityManager, Entity\Integration $integration, ?Entity\Author $author = null): void
    {
        $track = new Entity\Track();
        $track->setIntegration($integration);
        $track->setName(Data\Track::NAME);
        $track->setDuration(Data\Track::DURATION);
        $track->setDownloadUrl(Data\Track::DOWNLOAD_URL);

        if ($author) {
            $track->addAuthor($author);
        }

        $entityManager->persist($track);
        $entityManager->flush();
    }
}
