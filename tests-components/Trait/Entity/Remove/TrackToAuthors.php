<?php

namespace TestsComponents\Trait\Entity\Remove;

use Doctrine\ORM\EntityManagerInterface;
use App\Shared\Domain\Entity;

trait TrackToAuthors
{
    private function removeTrackToAuthors(EntityManagerInterface $entityManager): void
    {
        $sql = 'truncate table track_to_author';
        $entityManager->getConnection()->prepare($sql)->executeQuery();
    }
}
