<?php

namespace TestsComponents\Trait\Entity\Remove;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;

trait RefreshTokens
{
    private function removeRefreshTokens(EntityManagerInterface $entityManager): void
    {
        $entityManager->createQueryBuilder()
            ->delete(Entity\RefreshToken::class)
            ->getQuery()
            ->execute();
    }
}
