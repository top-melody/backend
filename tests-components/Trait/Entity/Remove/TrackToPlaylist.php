<?php

namespace TestsComponents\Trait\Entity\Remove;

use Doctrine\ORM\EntityManagerInterface;
use App\Shared\Domain\Entity;

trait TrackToPlaylist
{
    private function removeTrackToPlaylists(EntityManagerInterface $entityManager): void
    {
        $entityManager->createQueryBuilder()
            ->delete(Entity\TrackToPlaylist::class)
            ->getQuery()
            ->execute();
    }
}
