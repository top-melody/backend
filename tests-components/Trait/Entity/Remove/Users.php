<?php

namespace TestsComponents\Trait\Entity\Remove;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;

trait Users
{
    private function removeUsers(EntityManagerInterface $entityManager): void
    {
        $entityManager->createQueryBuilder()
            ->delete(Entity\User::class)
            ->getQuery()
            ->execute();
    }
}
