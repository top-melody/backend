<?php

namespace TestsComponents\Trait\Entity\Remove;

use Doctrine\ORM\EntityManagerInterface;

trait UserToPlaylists
{
    private function removeUserToPlaylists(EntityManagerInterface $entityManager): void
    {
        $sql = "delete from user_to_playlist";
        $entityManager->getConnection()->prepare($sql)->executeQuery();
    }
}
