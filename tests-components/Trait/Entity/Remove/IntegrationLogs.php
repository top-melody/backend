<?php

namespace TestsComponents\Trait\Entity\Remove;

use Doctrine\ORM\EntityManagerInterface;
use App\Shared\Domain\Entity;

trait IntegrationLogs
{
    private function removeIntegrationLogs(EntityManagerInterface $entityManager): void
    {
        $entityManager->createQueryBuilder()
            ->delete(Entity\IntegrationLog::class)
            ->getQuery()
            ->execute();
    }
}
