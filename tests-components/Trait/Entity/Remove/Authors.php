<?php

namespace TestsComponents\Trait\Entity\Remove;

use Doctrine\ORM\EntityManagerInterface;
use App\Shared\Domain\Entity;

trait Authors
{
    private function removeAuthors(EntityManagerInterface $entityManager): void
    {
        $entityManager->createQueryBuilder()
            ->delete(Entity\Author::class)
            ->getQuery()
            ->execute();
    }
}
