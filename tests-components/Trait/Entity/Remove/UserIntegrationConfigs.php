<?php

namespace TestsComponents\Trait\Entity\Remove;

use Doctrine\ORM\EntityManagerInterface;
use App\Shared\Domain\Entity;

trait UserIntegrationConfigs
{
    private function removeUserIntegrationConfigs(EntityManagerInterface $entityManager): void
    {
        $entityManager->createQueryBuilder()
            ->delete(Entity\UserIntegrationConfig::class)
            ->getQuery()
            ->execute();
    }
}
