<?php

namespace TestsComponents\Trait\Dependency;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait SetUpAuthor
{
    private ?Entity\Author $author = null;

    private function setUpAuthor(EntityManagerInterface $entityManager): void
    {
        $repository = $entityManager->getRepository(Entity\Author::class);
        $this->author = $repository->findOneBy(['name' => Data\Author::NAME]);
    }
}
