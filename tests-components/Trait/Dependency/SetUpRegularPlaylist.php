<?php

namespace TestsComponents\Trait\Dependency;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait SetUpRegularPlaylist
{
    private ?Entity\Playlist $regularPlaylist = null;

    private function setUpRegularPlaylist(EntityManagerInterface $entityManager): void
    {
        $repository = $entityManager->getRepository(Entity\Playlist::class);
        $this->regularPlaylist = $repository->getByName(Data\RegularPlaylist::NAME);
    }
}
