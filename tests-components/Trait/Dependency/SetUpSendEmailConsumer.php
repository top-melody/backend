<?php

namespace TestsComponents\Trait\Dependency;

use App\Email\Application\Consumer;

trait SetUpSendEmailConsumer
{
    private ?Consumer\Send $sendEmailConsumer = null;

    private function setUpSendEmailConsumer(): void
    {
        if (!self::$booted) {
            self::bootKernel();
        }

        $this->sendEmailConsumer = self::$kernel->getContainer()
            ->get(Consumer\Send::class);
    }
}
