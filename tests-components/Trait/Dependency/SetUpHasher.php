<?php

namespace TestsComponents\Trait\Dependency;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

trait SetUpHasher
{
    private ?UserPasswordHasherInterface $hasher = null;
    private function setUpHasher(): void
    {
        if (!self::$booted) {
            self::bootKernel();
        }

        $this->hasher = self::$kernel->getContainer()
            ->get(UserPasswordHasherInterface::class);
    }
}
