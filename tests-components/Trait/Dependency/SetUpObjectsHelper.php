<?php

namespace TestsComponents\Trait\Dependency;

use App\Shared\Infrastructure\Helper;

trait SetUpObjectsHelper
{
    private ?Helper\Objects $objectsHelper = null;

    private function setUpObjectsHelper(): void
    {
        if (!self::$booted) {
            self::bootKernel();
        }

        $this->objectsHelper = self::$kernel->getContainer()
            ->get(Helper\Objects::class);
    }
}
