<?php

namespace TestsComponents\Trait\Dependency;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait SetUpPromoDjIntegration
{
    private ?Entity\Integration $promoDjIntegration = null;

    private function setUpPromoDjIntegration(EntityManagerInterface $entityManager): void
    {
        $repository = $entityManager->getRepository(Entity\Integration::class);
        $this->promoDjIntegration = $repository->findOneBy(['name' => Data\PromoDjIntegration::NAME]);
    }
}
