<?php

namespace TestsComponents\Trait\Dependency;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait SetUpVkIntegration
{
    private ?Entity\Integration $vkIntegration = null;

    private function setUpVkIntegration(EntityManagerInterface $entityManager): void
    {
        $repository = $entityManager->getRepository(Entity\Integration::class);
        $this->vkIntegration = $repository->findOneBy(['name' => Data\VkIntegration::NAME]);
    }
}
