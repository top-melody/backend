<?php

namespace TestsComponents\Trait\Dependency;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait SetUpTrack
{
    private ?Entity\Track $track = null;

    private function setUpTrack(EntityManagerInterface $entityManager): void
    {
        $repository = $entityManager->getRepository(Entity\Track::class);
        $this->track = $repository->findOneBy(['name' => Data\Track::NAME]);
    }
}
