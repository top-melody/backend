<?php

namespace TestsComponents\Trait\Dependency;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait SetUpAnotherTrack
{
    private ?Entity\Track $anotherTrack = null;

    private function setUpAnotherTrack(EntityManagerInterface $entityManager): void
    {
        $repository = $entityManager->getRepository(Entity\Track::class);
        $this->anotherTrack = $repository->findOneBy(['name' => Data\AnotherTrack::NAME]);
    }
}
