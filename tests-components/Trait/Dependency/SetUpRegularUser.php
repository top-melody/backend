<?php

namespace TestsComponents\Trait\Dependency;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait SetUpRegularUser
{
    private ?Entity\User $regularUser = null;

    private function setUpRegularUser(EntityManagerInterface $entityManager): void
    {
        $userRepository = $entityManager->getRepository(Entity\User::class);
        $this->regularUser = $userRepository->findOneBy(['email' => Data\RegularUser::EMAIL]);
    }
}
