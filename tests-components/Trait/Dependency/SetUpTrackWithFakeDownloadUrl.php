<?php

namespace TestsComponents\Trait\Dependency;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait SetUpTrackWithFakeDownloadUrl
{
    private ?Entity\Track $trackWithFakeDownloadUrl = null;

    private function setUpTrackWithFakeDownloadUrl(EntityManagerInterface $entityManager): void
    {
        $repository = $entityManager->getRepository(Entity\Track::class);
        $this->trackWithFakeDownloadUrl = $repository->findOneBy(['name' => Data\TrackWithFakeDownloadUrl::NAME]);
    }
}
