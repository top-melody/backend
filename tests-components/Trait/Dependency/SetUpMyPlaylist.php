<?php

namespace TestsComponents\Trait\Dependency;

use App\Shared\Domain\Entity;
use App\Shared\Domain\Enum as SharedDomainEnum;
use Doctrine\ORM\EntityManagerInterface;

trait SetUpMyPlaylist
{
    private ?Entity\Playlist $myPlaylist = null;

    private function setUpMyPlaylist(EntityManagerInterface $entityManager, Entity\User $user): void
    {
        $repository = $entityManager->getRepository(Entity\Playlist::class);
        $this->myPlaylist = $repository->getByUserAndName($user, SharedDomainEnum\Playlist::My->value);
    }
}
