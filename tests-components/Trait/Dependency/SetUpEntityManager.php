<?php

namespace TestsComponents\Trait\Dependency;

use Doctrine\ORM\EntityManagerInterface;

trait SetUpEntityManager
{
    private ?EntityManagerInterface $entityManager = null;

    private function setUpEntityManager(): void
    {
        if (!self::$booted) {
            self::bootKernel();
        }

        $this->entityManager = self::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }
}
