<?php

namespace TestsComponents\Trait\Dependency;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait SetUpIntegrationWithFakeClassPath
{
    private ?Entity\Integration $integrationWithFakeClassPath = null;

    private function setUpIntegrationWithFakeClassPath(EntityManagerInterface $entityManager): void
    {
        $repository = $entityManager->getRepository(Entity\Integration::class);
        $this->integrationWithFakeClassPath = $repository->findOneBy(['name' => Data\IntegrationWithFakeClassPath::NAME]);
    }
}
