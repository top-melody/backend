<?php

namespace TestsComponents\Trait\Dependency;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;

trait SetUpKernelBrowser
{
    private ?KernelBrowser $kernelBrowser = null;

    private function setUpKernelBrowser(): void
    {
        $this->kernelBrowser = self::$booted
            ? self::$kernel->getContainer()->get('test.client')
            : self::createClient();
    }
}
