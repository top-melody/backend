<?php

namespace TestsComponents\Trait\Dependency;

use App\Shared\Domain\Entity;
use Doctrine\ORM\EntityManagerInterface;
use TestsComponents\Data;

trait SetUpTrackWithIntegrationWhereFakeClassPath
{
    private ?Entity\Track $trackWithIntegrationWhereFakeClassPath = null;

    private function setUpTrackWithIntegrationWhereFakeClassPath(EntityManagerInterface $entityManager): void
    {
        $repository = $entityManager->getRepository(Entity\Track::class);
        $this->trackWithIntegrationWhereFakeClassPath = $repository->findOneBy(['name' => Data\TrackWithIntegrationWhereFakeClassPath::NAME]);
    }
}
