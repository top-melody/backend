<?php

namespace TestsComponents\Data;

class TrackWithFakeDownloadUrl
{
    public const NAME = 'Трек с фейковой ссылкой на скачивание';
    public const DURATION = 100000;
    public const DOWNLOAD_URL = 'https://test.download.local.domain/123/download';
}
