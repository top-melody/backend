<?php

namespace TestsComponents\Data\V1;

class Url
{
    public const USER_LOGIN = '/v1/user/login';
    public const USER_LOGOUT = '/v1/user/logout';
    public const USER_REGISTER = '/v1/user/register';
    public const USER_REFRESH_TOKEN_PAIR = '/v1/user/refresh-token-pair';
    public const USER_PASSWORD_RECOVERY = '/v1/user/password-recovery';
    public const USER_SET_NEW_PASSWORD = '/v1/user/set-new-password';
    public const USER_GET_PROFILE = '/v1/user/profile';

    public const DICTIONARY_INTEGRATIONS = '/v1/dictionary/integrations';

    public const INTEGRATION_USER_CONFIG_LIST = '/v1/integration/user-config-list';
    public const INTEGRATION_LAST_USER_SCAN_LIST = '/v1/integration/last-user-scan-list';

    public const TRACK_GET_STREAM = '/v1/track/%s/stream';

    public const PLAYLIST_GET_ONE = '/v1/playlist/one';
    public const PLAYLIST_GET_LIST = '/v1/playlist/list';
    public const PLAYLIST_ADD_TRACK = '/v1/playlist/%s/track/%s';
    public const PLAYLIST_DELETE_TRACK = '/v1/playlist/%s/track/%s';
}
