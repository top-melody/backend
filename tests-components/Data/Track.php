<?php

namespace TestsComponents\Data;

class Track
{
    public const NAME = 'Мой трек';
    public const DURATION = 255;
    public const DOWNLOAD_URL = 'https://cs2-2v4.vkuseraudio.net/s/v1/acmp/9BvF10HzDg1pzhGgN2cW8MXFGcIrTsBrWF_BSvnMx6qH8tOzVpiE-KSXhpcP0k5XDfuPcP08glDAwdfZ-P32iY9pwviSbEckosxKCuAhPISgGusHOakbznrT-Y_ZxSHJ1Qd8kvsMD799f0hUriz-dSyA64H-inwfo2MP-BaWASmTxFm6kA.mp3';
}
