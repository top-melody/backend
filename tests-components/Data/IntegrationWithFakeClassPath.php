<?php

namespace TestsComponents\Data;

class IntegrationWithFakeClassPath
{
    public const NAME = 'Интеграция с фейковым путем класса инициализации';
    public const CONFIG = ['Ну конечно нет конфига, о чем речь?'];
    public const CLASS_PATH = 'Undefined\Path\To\File';
}
