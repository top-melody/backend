<?php

namespace TestsComponents\Data;

class TrackWithIntegrationWhereFakeClassPath
{
    public const NAME = 'Мой трек с интеграцией, у которой неверно указан класс';
    public const DURATION = 255;
    public const DOWNLOAD_URL = 'https://promodj.com/source/7420211/ASPARAGUSproject%20-%20Na%20Na%20Na%20%28promodj.com%29.mp3';
}
