<?php

namespace TestsComponents\Data;

class VkIntegration
{
    public const NAME = 'ВКонтакте';
    public const CONFIG = [];
    public const CLASS_PATH = \VkSDK\Application\VkIntegration::class;
}
