<?php

namespace App\Email\Infrastructure\Service;

use App\Shared\Meta\ServiceInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use App\Email\Domain\DTO;

class Send implements ServiceInterface
{
    private ?DTO\Email $email = null;

    public function __construct(
        readonly private MailerInterface $mailer,
    ) {
    }

    public function setEmail(DTO\Email $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function service(): void
    {
        $packageEmail = (new TemplatedEmail())
            ->from($this->email->from->value)
            ->to($this->email->to)
            ->subject($this->email->subject->value)
            ->htmlTemplate($this->email->templatePath)
            ->context($this->email->templateContext);
        $this->mailer->send($packageEmail);
    }
}
