<?php

namespace App\Email\Domain\Service;

use App\Email\Domain\Enum as DomainEnum;
use App\Email\Domain\Exception as DomainException;
use App\Email\Domain\Request;
use App\Shared\Infrastructure\Helper as SharedInfrastructureHelper;
use App\Shared\Meta\ServiceInterface;

class PrepareRequest implements ServiceInterface
{
    private array $data = [];

    public function __construct(
        readonly private SharedInfrastructureHelper\Objects $objectsHelper,
    ) {
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @throws DomainException\RequestNotFound
     * @throws DomainException\TypeNotFound
     */
    public function service(): Request\Base
    {
        return $this->objectsHelper->fill($this->getRequestClass(), $this->data);
    }

    /**
     * @throws DomainException\TypeNotFound
     * @throws DomainException\RequestNotFound
     */
    private function getRequestClass(): string
    {
        $type = $this->data['type'] ?? null;
        if (!$type) {
            throw new DomainException\TypeNotFound();
        }

        return match ($type) {
            DomainEnum\Type::PASSWORD_RECOVERY => Request\PasswordRecovery::class,
            default => throw new DomainException\RequestNotFound(),
        };
    }
}
