<?php

namespace App\Email\Domain\Service;

use App\Email\Domain\Component\LetterGenerator as DomainComponent;
use App\Email\Domain\Exception as DomainException;
use App\Shared\Domain\Entity;
use App\Shared\Domain\Enum as SharedDomainEnum;
use App\Shared\Meta\ServiceInterface;
use Throwable;
use App\Email\Domain\DTO;
use App\Email\Infrastructure\Service as InfrastructureService;
use App\Shared\Infrastructure\Repository;

class Send implements ServiceInterface
{
    private array $request = [];

    public function __construct(
        readonly private CreateGenerator $createGeneratorService,
        readonly private PrepareRequest  $prepareRequestService,
        readonly private Repository\User $userRepository,
        readonly private InfrastructureService\Send $sendService,
    ) {
    }

    public function setRequestArray(array $request): self
    {
        $this->request = $request;

        return $this;
    }

    public function service(): SharedDomainEnum\QueueStatus
    {
        try {
            $request = $this->prepareRequestService
                ->setData($this->request)
                ->service();
            $user = $this->userRepository
                ->getById($request->userId);
            if (!$user) {
                throw new DomainException\UserNotFound();
            }

            $generator = $this->createGeneratorService
                ->setRequest($request)
                ->service();
            $email = $this->getEmailDTO($user, $generator);

            $this->sendService
                ->setEmail($email)
                ->service();

            return SharedDomainEnum\QueueStatus::Ok;
        } catch (Throwable) {
            return SharedDomainEnum\QueueStatus::Fail;
        }
    }

    private function getEmailDTO(Entity\User $user, DomainComponent\LetterGeneratorInterface $generator): DTO\Email
    {
        $email = new DTO\Email();
        $email->from = $generator->getFrom();
        $email->to = $user->getEmail();
        $email->subject = $generator->getSubject();
        $email->templatePath = $generator->getTemplatePath();
        $email->templateContext = $generator->getTemplateContext();

        return $email;
    }
}
