<?php

namespace App\Email\Domain\Service;

use App\Email\Domain\Enum as DomainEnum;
use App\Email\Domain\Exception as DomainException;
use App\Email\Domain\Request;
use App\Shared\Meta\ServiceInterface;
use App\Email\Domain\Component\LetterGenerator as DomainComponent;

class CreateGenerator implements ServiceInterface
{
    private ?Request\Base $request = null;

    public function setRequest(Request\Base $request): self
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @throws DomainException\TypeNotFound
     * @throws DomainException\GeneratorNotFound
     */
    public function service(): DomainComponent\LetterGeneratorInterface
    {
        if (!$this->request->type) {
            throw new DomainException\TypeNotFound();
        }

        return match ($this->request->type) {
            DomainEnum\Type::PASSWORD_RECOVERY => new DomainComponent\PasswordRecovery($this->request),
            default => throw new DomainException\GeneratorNotFound(),
        };
    }
}
