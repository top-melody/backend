<?php

namespace App\Email\Domain\DTO;

use App\Email\Domain\Enum as DomainEnum;

class Email
{
    public ?DomainEnum\FromAddress $from = null;
    public ?string $to = null;
    public ?DomainEnum\Subject $subject = null;
    public ?string $templatePath = null;
    public array $templateContext = [];
}
