<?php

namespace App\Email\Domain\Request;

class PasswordRecovery extends Base
{
    public ?string $frontendUrl = null;
    public ?string $token = null;
    public ?string $userEmail = null;
}
