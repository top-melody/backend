<?php

namespace App\Email\Domain\Request;

abstract class Base
{
    public ?string $type = null;
    public ?int $userId = null;
}
