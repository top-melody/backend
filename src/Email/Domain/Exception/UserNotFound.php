<?php

namespace App\Email\Domain\Exception;

use App\Shared\Domain\Enum as SharedDomainEnum;

class UserNotFound extends Email
{
    public function getMessageEnum(): SharedDomainEnum\Message
    {
        return SharedDomainEnum\Message::UserNotFound;
    }
}
