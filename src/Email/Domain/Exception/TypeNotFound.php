<?php

namespace App\Email\Domain\Exception;

use App\Shared\Domain\Enum as SharedDomainEnum;

class TypeNotFound extends Email
{
    public function getMessageEnum(): SharedDomainEnum\Message
    {
        return SharedDomainEnum\Message::EmailTypeNotFound;
    }
}
