<?php

namespace App\Email\Domain\Component\LetterGenerator;

use App\Email\Domain\Request;
use App\Email\Domain\Enum;

/**
 * @property Request\PasswordRecovery $request
 */
class PasswordRecovery implements LetterGeneratorInterface
{
    public function __construct(
        readonly private Request\Base $request,
    ) {
    }

    public function getFrom(): Enum\FromAddress
    {
        return Enum\FromAddress::NoReply;
    }

    public function getSubject(): Enum\Subject
    {
        return Enum\Subject::PasswordRecovery;
    }

    public function getTemplatePath(): string
    {
        return 'Email/PasswordRecovery.html.twig';
    }

    public function getTemplateContext(): array
    {
        return (array) $this->request;
    }
}
