<?php

namespace App\Email\Domain\Component\LetterGenerator;

use App\Email\Domain\Enum as DomainEnum;
use App\Email\Domain\Request as DomainRequest;

interface LetterGeneratorInterface
{
    public function __construct(
        DomainRequest\Base $request,
    );

    public function getFrom(): DomainEnum\FromAddress;
    public function getSubject(): DomainEnum\Subject;
    public function getTemplatePath(): string;
    public function getTemplateContext(): array;
}
