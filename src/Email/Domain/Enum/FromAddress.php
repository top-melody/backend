<?php

namespace App\Email\Domain\Enum;

enum FromAddress: string
{
    case NoReply = 'no-reply@top-melody.ru';
}
