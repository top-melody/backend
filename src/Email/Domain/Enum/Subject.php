<?php

namespace App\Email\Domain\Enum;

enum Subject: string
{
    case PasswordRecovery = 'Восстановление пароля | Top Melody';
}
