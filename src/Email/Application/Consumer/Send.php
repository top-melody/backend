<?php

namespace App\Email\Application\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use App\Email\Domain\Service as DomainService;

class Send implements ConsumerInterface
{
    public function __construct(
        readonly private DomainService\Send $sendService,
    ) {
    }

    public function execute(AMQPMessage $msg): int
    {
        $requestArray = json_decode($msg->getBody(), true);
        $status = $this->sendService->setRequestArray($requestArray)->service();
        return $status->value;
    }
}
