<?php

namespace App\Integration\Domain\Exception\SdkFacade;

use App\Shared\Domain\Enum as DomainEnum;

class AuthFail extends OperationFail
{
    public function getMessageEnum(): DomainEnum\Message
    {
        return DomainEnum\Message::AuthFail;
    }
}
