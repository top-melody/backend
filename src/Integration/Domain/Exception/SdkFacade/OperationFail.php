<?php

namespace App\Integration\Domain\Exception\SdkFacade;

use App\Integration\Domain\Exception\Integration;

abstract class OperationFail extends Integration
{
    /**
     * @var string[]
     */
    private array $errors = [];

    /**
     * @param string[] $errors
     */
    public function setErrors(array $errors): self
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
