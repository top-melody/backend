<?php

namespace App\Integration\Domain\Exception;

use App\Shared\Domain\Enum as DomainEnum;

class MaxRecursionTimes extends Integration
{
    public function getMessageEnum(): DomainEnum\Message
    {
        return DomainEnum\Message::MaxRecursionTimes;
    }
}
