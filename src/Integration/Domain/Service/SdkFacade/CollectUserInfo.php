<?php

namespace App\Integration\Domain\Service\SdkFacade;

use App\Integration\Domain\Exception\SdkFacade\CollectUserInfoFail;
use App\Integration\Domain\Service as DomainService;
use App\Shared\Domain\Entity;
use App\Shared\Infrastructure\Repository;
use App\Shared\Meta\ServiceInterface;
use IntegrationCore\Domain\Component as IntegrationCoreComponent;

class CollectUserInfo implements \App\Shared\Meta\ServiceInterface
{
    private ?Entity\Integration $integration = null;
    private ?IntegrationCoreComponent\Integration $sdk = null;
    private ?Entity\User $user = null;

    public function __construct(
        readonly private Repository\UserIntegrationConfig $userIntegrationConfigRepository,
        readonly private DomainService\Log                $logService,
    ) {
    }

    public function setIntegration(Entity\Integration $integration): self
    {
        $this->integration = $integration;

        return $this;
    }

    public function setSdk(IntegrationCoreComponent\Integration $sdk): self
    {
        $this->sdk = $sdk;

        return $this;
    }

    public function setUser(Entity\User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function service(): void
    {
        $getUserInfoResult = $this->sdk->getUserInfo();
        $this->log($getUserInfoResult->getLogList());

        if (!$getUserInfoResult->getSuccess()) {
            $exception = new CollectUserInfoFail();
            $exception->setErrors($getUserInfoResult->getErrorList());
            throw $exception;
        }

        $uic = $this->getUserIntegrationConfig();
        $uic->setConfig($getUserInfoResult->getUserConfig());
        $this->userIntegrationConfigRepository->save($uic);
    }

    private function log(array $logList): void
    {
        $this->logService
            ->setLogList($logList)
            ->setIntegrationEntity($this->integration)
            ->service();
    }

    private function getUserIntegrationConfig(): Entity\UserIntegrationConfig
    {
        $uic = $this->userIntegrationConfigRepository->getForIntegrationAndUser($this->integration, $this->user);

        if (!$uic) {
            $uic = new Entity\UserIntegrationConfig();
            $uic->setUser($this->user);
            $uic->setIntegration($this->integration);
        }

        return $uic;
    }
}
