<?php

namespace App\Integration\Domain\Service\SdkFacade;

use App\Integration\Domain\Exception\SdkFacade as SdkFacadeException;
use App\Integration\Domain\Service as DomainService;
use App\Shared\Meta\ServiceInterface;
use IntegrationCore\Domain\Component as IntegrationCoreComponent;
use App\Shared\Domain\Entity;
use IntegrationCore\Domain\Response as IntegrationCoreResponse;

class GetStream implements ServiceInterface
{
    private ?IntegrationCoreComponent\Integration $sdk = null;
    private ?string $trackDownloadUrl = null;
    private ?Entity\Integration $integration = null;

    public function __construct(
        readonly private DomainService\Log $integrationLogService,
    ) {
    }

    public function setSdk(IntegrationCoreComponent\Integration $sdk): self
    {
        $this->sdk = $sdk;

        return $this;
    }

    public function setIntegration(Entity\Integration $integration): self
    {
        $this->integration = $integration;

        return $this;
    }

    public function setTrackDownloadUrl(string $trackDownloadUrl): self
    {
        $this->trackDownloadUrl = $trackDownloadUrl;

        return $this;
    }

    /**
     * @return IntegrationCoreResponse\GetStream
     * @throws SdkFacadeException\GetStreamFail
     */
    public function service(): IntegrationCoreResponse\GetStream
    {
        $getStreamResponse = $this->sdk->getStream($this->trackDownloadUrl);
        $this->integrationLogService
            ->setLogList($getStreamResponse->getLogList())
            ->setIntegrationEntity($this->integration)
            ->service();

        if (!$getStreamResponse->getSuccess()) {
            $exception = new SdkFacadeException\GetStreamFail();
            $exception->setErrors($getStreamResponse->getErrorList());
            throw $exception;
        }

        return $getStreamResponse;
    }
}
