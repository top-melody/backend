<?php

namespace App\Integration\Domain\Service\SdkFacade;

use App\Integration\Domain\Exception\SdkFacade as SdkFacadeException;
use App\Integration\Domain\Service as DomainService;
use App\Shared\Domain\Entity;
use App\Shared\Infrastructure\Repository;
use App\Shared\Meta\ServiceInterface;
use IntegrationCore\Domain\Component as IntegrationCoreComponent;

class Auth implements ServiceInterface
{
    private ?Entity\Integration $integration = null;
    private ?IntegrationCoreComponent\Integration $sdk = null;

    public function __construct(
        readonly private Repository\Integration $integrationRepository,
        readonly private DomainService\Log $logService,
    ) {
    }

    public function setIntegration(Entity\Integration $integration): self
    {
        $this->integration = $integration;

        return $this;
    }

    public function setSdk(IntegrationCoreComponent\Integration $sdk): self
    {
        $this->sdk = $sdk;

        return $this;
    }

    /**
     * @throws SdkFacadeException\AuthFail
     */
    public function service(): void
    {
        $authResult = $this->sdk->auth();
        $this->log($authResult->getLogList());

        if (!$authResult->getSuccess()) {
            $exception = new SdkFacadeException\AuthFail();
            $exception->setErrors($authResult->getErrorList());
            throw $exception;
        }

        $this->integration->setConfig($authResult->getCommonConfig());
        $this->integrationRepository->save($this->integration);
    }

    private function log(array $logList): void
    {
        $this->logService
            ->setLogList($logList)
            ->setIntegrationEntity($this->integration)
            ->service();
    }
}
