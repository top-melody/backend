<?php

namespace App\Integration\Domain\Service;

use App\Integration\Domain\Exception as DomainException;
use App\Shared\Domain\Entity;
use App\Shared\Meta\ServiceInterface;
use App\Shared\Infrastructure\Repository;
use IntegrationCore\Domain\Component as IntegrationCoreDomainComponent;

class CreateSdk implements ServiceInterface
{
    private ?Entity\Integration $integration = null;
    private ?Entity\User $user = null;

    public function __construct(
        readonly private Repository\UserIntegrationConfig $userIntegrationConfigRepository,
    ) {
    }

    public function setIntegration(Entity\Integration $integration): self
    {
        $this->integration = $integration;

        return $this;
    }

    public function setUser(?Entity\User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @throws DomainException\IntegrationNotFound
     */
    public function service(): IntegrationCoreDomainComponent\Integration
    {
        $class = $this->integration->getClass();

        if (!class_exists($class)) {
            throw new DomainException\IntegrationNotFound();
        }

        if (!in_array(IntegrationCoreDomainComponent\Integration::class, class_implements($class))) {
            throw new DomainException\IntegrationNotFound();
        }

        $commonConfig = $this->integration->getConfig();
        $userConfig = $this->user
            && ($uic = $this->userIntegrationConfigRepository->getForIntegrationAndUser($this->integration, $this->user))
            ? $uic->getConfig()
            : [];

        /** @var IntegrationCoreDomainComponent\Integration $sdk */
        $sdk = new $class($commonConfig, $userConfig);
        return $sdk;
    }
}
