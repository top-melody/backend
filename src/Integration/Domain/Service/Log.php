<?php

namespace App\Integration\Domain\Service;

use App\Shared\Domain\Entity;
use App\Shared\Meta\ServiceInterface;
use IntegrationCore\Domain\DataProvider as IntegrationCoreDataProvider;
use App\Shared\Infrastructure\Repository;

class Log implements ServiceInterface
{
    /**
     * @var IntegrationCoreDataProvider\Log[] $logList
     */
    private array $logList = [];

    private ?Entity\Integration $integration = null;

    public function __construct(
        readonly private Repository\IntegrationLog $integrationLogRepository,
    ) {
    }

    /**
     * @param IntegrationCoreDataProvider\Log[] $logList
     */
    public function setLogList(array $logList): self
    {
        $this->logList = $logList;

        return $this;
    }

    public function setIntegrationEntity(Entity\Integration $integration): self
    {
        $this->integration = $integration;

        return $this;
    }

    public function service(): void
    {
        if (!$this->logList) {
            return;
        }

        $entities = array_map(
            fn (IntegrationCoreDataProvider\Log $log) => $this->getEntity($log),
            $this->logList,
        );
        $this->integrationLogRepository->saveMany($entities);
    }

    private function getEntity(IntegrationCoreDataProvider\Log $log): Entity\IntegrationLog
    {
        $integrationLog = new Entity\IntegrationLog();
        $integrationLog->setIsSuccess($log->isSuccess());
        $integrationLog->setErrors($log->getErrors());
        $integrationLog->setType($log->getType()->value);
        $integrationLog->setRequestUrl($log->getRequestUrl());
        $integrationLog->setRequest($log->getRawRequest());
        $integrationLog->setResponse($log->getRawResponse());
        $integrationLog->setRequestHeaders($log->getRequestHeaders());
        $integrationLog->setResponseHeaders($log->getResponseHeaders());
        $integrationLog->setExecutionStartDateTime($log->getExecutionEndDateTime());
        $integrationLog->setExecutionEndDateTime($log->getExecutionEndDateTime());
        $integrationLog->setIntegration($this->integration);

        return $integrationLog;
    }
}
