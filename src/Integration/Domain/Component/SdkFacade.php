<?php

namespace App\Integration\Domain\Component;

use App\Integration\Domain\Exception as IntegrationDomainException;
use App\Integration\Domain\Service as DomainService;
use App\Shared\Domain\Entity;
use IntegrationCore\Domain\Component as IntegrationCoreComponent;
use IntegrationCore\Domain\DataProvider as IntegrationCoreDataProvider;
use IntegrationCore\Domain\Exception as IntegrationCoreException;
use IntegrationCore\Domain\Response;

class SdkFacade
{
    private const MAX_RECURSION_TIMES = 5;

    private ?Entity\Integration $integration = null;
    private ?Entity\User $user = null;
    private ?IntegrationCoreComponent\Integration $sdk = null;
    private int $recursionTimes = 0;

    public function __construct(
        readonly private DomainService\CreateSdk                   $createSdkService,
        readonly private DomainService\Log                         $integrationLogService,
        readonly private DomainService\SdkFacade\Auth              $authService,
        readonly private DomainService\SdkFacade\GetStream         $getStreamService,
        readonly private DomainService\SdkFacade\CollectUserInfo   $collectUserInfoService,
    ) {
    }

    /**
     * @throws IntegrationDomainException\IntegrationNotFound
     */
    public function init(Entity\Integration $integration, ?Entity\User $user): self
    {
        $this->integration = $integration;
        $this->user = $user;
        $this->sdk = $this->createSdkService
            ->setIntegration($integration)
            ->setUser($user)
            ->service();

        return $this;
    }

    /**
     * @throws IntegrationDomainException\MaxRecursionTimes
     * @throws IntegrationDomainException\SdkFacade\OperationFail
     */
    private function try(\Closure $closure, mixed ...$args): mixed
    {
        $this->recursionTimes++;
        if ($this->recursionTimes > self::MAX_RECURSION_TIMES) {
            $this->recursionTimes = 0;
            throw new IntegrationDomainException\MaxRecursionTimes();
        }

        try {
            $result = $closure(...$args);
            $this->recursionTimes = 0;
            return $result;
        } catch (IntegrationCoreException\AuthenticationRequired $exception) {
            $this->log($exception->getLogs());
            $this->auth();
        } catch (IntegrationCoreException\UserInfoRequired $exception) {
            $this->log($exception->getLogs());
            $this->collectUserInfo();
        }

        return $this->try($closure, ...$args);
    }

    /**
     * @param IntegrationCoreDataProvider\Log[] $logs
     */
    private function log(array $logs): void
    {
        $this->integrationLogService
            ->setIntegrationEntity($this->integration)
            ->setLogList($logs)
            ->service();
    }

    /**
     * @throws IntegrationDomainException\MaxRecursionTimes
     * @throws IntegrationDomainException\SdkFacade\AuthFail
     */
    private function auth(): void
    {
        $this->try(
            fn () => $this->authService
                ->setIntegration($this->integration)
                ->setSdk($this->sdk)
                ->service()
        );
    }

    /**
     * @throws IntegrationDomainException\MaxRecursionTimes
     * @throws IntegrationDomainException\SdkFacade\CollectUserInfoFail
     */
    private function collectUserInfo(): void
    {
        $this->try(
            fn () => $this->collectUserInfoService
                ->setIntegration($this->integration)
                ->setSdk($this->sdk)
                ->service()
        );
    }

    /**
     * @throws IntegrationDomainException\MaxRecursionTimes
     * @throws IntegrationDomainException\SdkFacade\OperationFail
     */
    public function getStream(string $trackDownloadUrl): Response\GetStream
    {
        return $this->try(
            fn (string $trackDownloadUrl) => $this->getStreamService
                ->setSdk($this->sdk)
                ->setIntegration($this->integration)
                ->setTrackDownloadUrl($trackDownloadUrl)
                ->service(),
            $trackDownloadUrl,
        );
    }
}
