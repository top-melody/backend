<?php

namespace App\Api\Shared\Application\EventListener\JWT;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

use App\Api\Shared\Domain\Service\EventListener\JWT as DomainService;

class Created
{
    public function __construct(
        readonly private DomainService\ModifyJWTData $modifyJWTDataService
    ) {
    }

    public function __invoke(JWTCreatedEvent $event): void
    {
        $data = $this->modifyJWTDataService
            ->setUser($event->getUser())
            ->setTokenData($event->getData())
            ->service();

        $event->setData($data);
    }
}
