<?php

namespace App\Api\Shared\Application\EventListener\JWT;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTFailureEventInterface;
use App\Api\Shared\Domain\Service\EventListener\JWT as DomainService;

class AuthFailure
{
    public function __construct(
        readonly private DomainService\GetAuthenticationFailureResponse $getAuthenticationFailureResponseService,
        readonly private DomainService\GetJWTExpiredResponse $getJWTExpiredResponseService,
        readonly private DomainService\GetJWTInvalidResponse $getJWTInvalidResponseService,
        readonly private DomainService\GetJWTNotFoundResponse $getJWTNotFoundResponseService,
    ) {
    }

    public function onAuthenticationFailureResponse(JWTFailureEventInterface $event): void
    {
        $response = $this->getAuthenticationFailureResponseService->service();
        $event->setResponse($response);
    }

    public function onJWTExpired(JWTFailureEventInterface $event): void
    {
        $response = $this->getJWTExpiredResponseService->service();
        $event->setResponse($response);
    }

    public function onJWTInvalid(JWTFailureEventInterface $event): void
    {
        $response = $this->getJWTInvalidResponseService->service();
        $event->setResponse($response);
    }

    public function onJWTNotFound(JWTFailureEventInterface $event): void
    {
        $response = $this->getJWTNotFoundResponseService->service();
        $event->setResponse($response);
    }
}
