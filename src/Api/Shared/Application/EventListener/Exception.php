<?php

namespace App\Api\Shared\Application\EventListener;

use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use App\Api\Shared\Domain\Service\EventListener as DomainService;

class Exception
{
    public function __construct(
        readonly private DomainService\GetExceptionResponse $getExceptionResponseService,
    ) {
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        if ('application/json' === $event->getRequest()->headers->get('Content-Type')) {
            $jsonResponse = $this->getExceptionResponseService->setThrowable($event->getThrowable())->service();
            $event->setResponse($jsonResponse);
        }
    }
}
