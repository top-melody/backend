<?php

namespace App\Api\Shared\Infrastructure\Response;

use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Mime\MimeTypes;

abstract class Stream extends StreamedResponse
{
    protected function setHeaders(array $headers): self
    {
        $this->headers->replace($headers);

        return $this;
    }

    protected function getContentDisposition(string $fileName, string $fileExtension): string
    {
        $fullFileName = implode('.', [$fileName, $fileExtension]);
        $filenameFallback = preg_replace('#^.*\.#', md5($fullFileName) . '.', $fullFileName);

        return $this->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fullFileName,
            $filenameFallback
        );
    }

    protected function getExtensionByMimeType(string $mimeType): string
    {
        $extList = (new MimeTypes())->getExtensions($mimeType);
        if (!$extList) {
            return 'mp3';
        }

        return $extList[0];
    }
}
