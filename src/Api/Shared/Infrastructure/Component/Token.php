<?php

namespace App\Api\Shared\Infrastructure\Component;

use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class Token
{
    public function __construct(
        readonly private JWTTokenManagerInterface $jwtTokenManager,
    ) {
    }

    public function create(UserInterface $user): string
    {
        return $this->jwtTokenManager->create($user);
    }
}
