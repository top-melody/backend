<?php

namespace App\Api\Shared\Infrastructure\Component;

use Gesdinet\JWTRefreshTokenBundle\Generator\RefreshTokenGeneratorInterface;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenInterface;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class RefreshToken
{
    private const REFRESH_TOKEN_TTL = 2592000; // 1 month

    public function __construct(
        readonly private RefreshTokenManagerInterface $refreshTokenManager,
        readonly private RefreshTokenGeneratorInterface $refreshTokenGenerator,
    ) {
    }

    public function create(UserInterface $user): RefreshTokenInterface
    {
        return $this->refreshTokenGenerator->createForUserWithTtl($user, self::REFRESH_TOKEN_TTL);
    }

    public function getLast(UserInterface $user): ?RefreshTokenInterface
    {
        return $this->refreshTokenManager->getLastFromUsername($user->getUserIdentifier());
    }

    public function save(RefreshTokenInterface $refreshToken): void
    {
        $this->refreshTokenManager->save($refreshToken);
    }

    public function remove(RefreshTokenInterface $refreshToken): void
    {
        $this->refreshTokenManager->delete($refreshToken);
    }

    public function getByString(string $refreshToken): ?RefreshTokenInterface
    {
        return $this->refreshTokenManager->get($refreshToken);
    }
}
