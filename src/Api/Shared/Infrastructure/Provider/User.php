<?php

namespace App\Api\Shared\Infrastructure\Provider;

use App\Shared\Domain\Entity;
use App\Shared\Infrastructure\Repository;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\PayloadAwareUserProviderInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements PayloadAwareUserProviderInterface
{
    public function __construct(
        private Repository\User $userRepository
    ) {
    }

    public function loadUserByUsernameAndPayload(string $username, array $payload): ?UserInterface
    {
        return null;
    }

    public function refreshUser(UserInterface $user): ?UserInterface
    {
        return null;
    }

    public function supportsClass(string $class): bool
    {
        return $class === Entity\User::class || is_subclass_of($class, Entity\User::class);
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        return $this->getUser('email', $identifier);
    }

    public function loadUserByIdentifierAndPayload(string $identifier, array $payload): UserInterface
    {
        return $this->getUser('id', $payload['id']);
    }

    private function getUser(string $key, $value): UserInterface
    {
        $user = $this->userRepository->findOneBy([$key => $value]);
        if (null === $user) {
            $e = new UserNotFoundException(sprintf('User "%s" not found.', $value));
            $e->setUserIdentifier($value);

            throw $e;
        }

        return $user;
    }
}
