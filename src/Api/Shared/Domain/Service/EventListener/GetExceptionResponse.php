<?php

namespace App\Api\Shared\Domain\Service\EventListener;

use App\Api\Shared\Domain\Enum;
use App\Api\Shared\Domain\Response;
use App\Shared\Domain\Enum as SharedDomainEnum;
use App\Shared\Meta\ServiceInterface;
use Throwable;

class GetExceptionResponse implements ServiceInterface
{
    private const ENV_PROD = 'prod';

    public function __construct(
        readonly private string $environment,
    ) {
    }

    private ?Throwable $exception = null;

    public function setThrowable(Throwable $exception): self
    {
        $this->exception = $exception;

        return $this;
    }

    public function service(): Response\Json
    {
        $exceptionResponse = new Response\Exception();
        $message = implode(': ', [SharedDomainEnum\Message::ServerError->value, $this->exception->getMessage()]);
        $exceptionResponse->addStringMessage($message);

        if ($this->environment !== self::ENV_PROD) {
            $exceptionResponse->file = $this->exception->getFile();
            $exceptionResponse->line = $this->exception->getLine();
            $exceptionResponse->type = get_class($this->exception);
            $exceptionResponse->trace = $this->exception->getTrace();
        }

        $response = $exceptionResponse->makeResponse();
        $response->setStatus(Enum\HttpCode::InternalServerError);

        return $response;
    }
}
