<?php

namespace App\Api\Shared\Domain\Service\EventListener\JWT;

use App\Shared\Meta\ServiceInterface;
use App\Shared\Domain\Enum as SharedDomainEnum;
use App\Api\Shared\Domain\Response;
use App\Api\Shared\Domain\Enum;

abstract class Base implements ServiceInterface
{
    public function service(): Response\Json
    {
        $responseDTO = new Response\InvalidToken();
        $responseDTO->addMessage($this->getMessage());

        return $responseDTO
            ->makeResponse()
            ->setStatus($this->getResponseCode());
    }

    abstract protected function getMessage(): SharedDomainEnum\Message;

    abstract protected function getResponseCode(): Enum\HttpCode;
}
