<?php

namespace App\Api\Shared\Domain\Service\EventListener\JWT;

use App\Api\Shared\Domain\Enum;
use App\Shared\Domain\Enum as SharedDomainEnum;

class GetJWTInvalidResponse extends Base
{
    protected function getMessage(): SharedDomainEnum\Message
    {
        return SharedDomainEnum\Message::InvalidToken;
    }

    protected function getResponseCode(): Enum\HttpCode
    {
        return Enum\HttpCode::Forbidden;
    }
}
