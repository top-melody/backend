<?php

namespace App\Api\Shared\Domain\Service\EventListener\JWT;

use App\Shared\Domain\Entity;
use App\Shared\Meta\ServiceInterface;

class ModifyJWTData implements ServiceInterface
{
    private ?Entity\User $user = null;
    private ?array $tokenData = null;

    public function setUser(Entity\User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function setTokenData(array $tokenData): self
    {
        $this->tokenData = $tokenData;

        return $this;
    }

    public function service(): array
    {
        $this->tokenData['id'] = $this->user->getId();
        return $this->tokenData;
    }
}
