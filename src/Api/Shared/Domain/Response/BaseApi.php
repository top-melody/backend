<?php

namespace App\Api\Shared\Domain\Response;

use App\Shared\Domain\Enum as SharedDomainEnum;

abstract class BaseApi
{
    public bool $isSuccess = true;

    /** @var string[] */
    public array $messages = [];

    /** @var string[][] */
    public array $validationErrors = [];

    public function addStringMessage(string $message): void
    {
        $this->messages[] = $message;
        $this->isSuccess = false;
    }

    public function addMessage(SharedDomainEnum\Message $message): void
    {
        $this->addStringMessage($message->value);
    }

    /**
     * @param SharedDomainEnum\Message[] $messages
     * @return void
     */
    public function addMessages(array $messages): void
    {
        foreach ($messages as $message) {
            $this->addMessage($message);
        }
    }

    public function setValidationErrors(array $validationErrors): void
    {
        $this->validationErrors = $validationErrors;
        $this->isSuccess = empty($validationErrors);
    }

    public function makeResponse(): Json
    {
        $data = get_object_vars($this);
        return new Json($data);
    }
}
