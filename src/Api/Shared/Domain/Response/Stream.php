<?php

namespace App\Api\Shared\Domain\Response;

use App\Api\Shared\Domain\Enum;
use App\Api\Shared\Infrastructure\Response as ApiSharedInfrastructureResponse;
use IntegrationCore\Domain\DataProvider as IntegrationCoreDataProvider;

class Stream extends ApiSharedInfrastructureResponse\Stream
{
    private const READ_STEP_IN_BYTES = 1024;

    public function __construct(
        readonly private IntegrationCoreDataProvider\Stream $stream,
        readonly private string $fileName,
        readonly private string $mimeType,
        readonly private ?int $sizeInBytes
    ) {
        parent::__construct($this->getCallback());
        $this->setHeaders($this->getHeaders());
    }

    public function setStatus(Enum\HttpCode $code): self
    {
        $this->setStatusCode($code->value);

        return $this;
    }

    private function getCallback(): callable
    {
        return function () {
            while (!$this->stream->isEndOfFile()) {
                echo $this->stream->read(self::READ_STEP_IN_BYTES);
            }
        };
    }

    /**
     * @return string[]
     */
    private function getHeaders(): array
    {
        return [
            'Content-Disposition' => $this->getContentDisposition($this->fileName, $this->getExtensionByMimeType($this->mimeType)),
            'Content-Type' => $this->mimeType,
            'Content-Length' => $this->sizeInBytes,
        ];
    }
}
