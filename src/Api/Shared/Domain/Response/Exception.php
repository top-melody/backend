<?php

namespace App\Api\Shared\Domain\Response;

class Exception extends BaseApi
{
    public ?string $file = null;
    public ?int $line = null;
    public ?string $type = null;
    /** @var string[][]|null  */
    public ?array $trace = null;
}
