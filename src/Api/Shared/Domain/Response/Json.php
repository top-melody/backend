<?php

namespace App\Api\Shared\Domain\Response;

use App\Api\Shared\Domain\Enum;
use App\Api\Shared\Infrastructure\Response as ApiSharedInfrastructureResponse;

class Json extends ApiSharedInfrastructureResponse\Json
{
    public function setStatus(Enum\HttpCode $code): self
    {
        $this->setStatusCode($code->value);

        return $this;
    }
}
