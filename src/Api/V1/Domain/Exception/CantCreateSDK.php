<?php

namespace App\Api\V1\Domain\Exception;

use App\Shared\Domain\Enum as DomainEnum;
use App\Api\Shared\Domain\Enum as ApiSharedDomainEnum;

class CantCreateSDK extends ApiV1
{
    public function getMessageEnum(): DomainEnum\Message
    {
        return DomainEnum\Message::CantCreateSDK;
    }

    public function getCodeEnum(): ApiSharedDomainEnum\HttpCode
    {
        return ApiSharedDomainEnum\HttpCode::InternalServerError;
    }
}
