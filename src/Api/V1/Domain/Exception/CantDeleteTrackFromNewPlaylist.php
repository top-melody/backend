<?php

namespace App\Api\V1\Domain\Exception;

use App\Api\Shared\Domain\Enum as ApiSharedDomainEnum;
use App\Shared\Domain\Enum as DomainEnum;

class CantDeleteTrackFromNewPlaylist extends ApiV1
{
    public function getCodeEnum(): ApiSharedDomainEnum\HttpCode
    {
        return ApiSharedDomainEnum\HttpCode::UnprocessableEntity;
    }

    public function getMessageEnum(): DomainEnum\Message
    {
        return DomainEnum\Message::CantDeleteTrackFromNewPlaylist;
    }
}
