<?php

namespace App\Api\V1\Domain\Exception;

use App\Api\Shared\Domain\Enum as ApiSharedDomainEnum;
use App\Shared\Domain\Enum as DomainEnum;

class PlaylistIdNotValid extends ApiV1
{
    public function getMessageEnum(): DomainEnum\Message
    {
        return DomainEnum\Message::PlaylistIdNotValid;
    }

    public function getCodeEnum(): ApiSharedDomainEnum\HttpCode
    {
        return ApiSharedDomainEnum\HttpCode::UnprocessableEntity;
    }
}
