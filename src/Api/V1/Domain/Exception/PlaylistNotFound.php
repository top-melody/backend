<?php

namespace App\Api\V1\Domain\Exception;

use App\Api\Shared\Domain\Enum as ApiSharedDomainEnum;
use App\Shared\Domain\Enum as DomainEnum;

class PlaylistNotFound extends ApiV1
{
    public function getMessageEnum(): DomainEnum\Message
    {
        return DomainEnum\Message::PlaylistNotFound;
    }

    public function getCodeEnum(): ApiSharedDomainEnum\HttpCode
    {
        return ApiSharedDomainEnum\HttpCode::UnprocessableEntity;
    }
}
