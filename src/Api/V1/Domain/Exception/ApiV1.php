<?php

namespace App\Api\V1\Domain\Exception;

use App\Api\Shared\Domain\Enum as ApiSharedDomainEnum;
use App\Shared\Domain\Exception as SharedDomainException;

abstract class ApiV1 extends SharedDomainException\Logic
{
    abstract public function getCodeEnum(): ApiSharedDomainEnum\HttpCode;
}
