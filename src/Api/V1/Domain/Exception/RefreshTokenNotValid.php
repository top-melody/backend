<?php

namespace App\Api\V1\Domain\Exception;

use App\Shared\Domain\Enum as SharedDomainEnum;
use App\Api\Shared\Domain\Enum as ApiSharedDomainEnum;

class RefreshTokenNotValid extends ApiV1
{
    public function getMessageEnum(): SharedDomainEnum\Message
    {
        return SharedDomainEnum\Message::RefreshTokenExpired;
    }

    public function getCodeEnum(): ApiSharedDomainEnum\HttpCode
    {
        return ApiSharedDomainEnum\HttpCode::NotAcceptable;
    }
}
