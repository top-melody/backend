<?php

namespace App\Api\V1\Domain\DTO;

class TrackListWithPagination
{
    /**
     * @var Track[]
     */
    public array $trackList = [];
    public ?Pagination $pagination = null;
}
