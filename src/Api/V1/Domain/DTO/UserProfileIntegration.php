<?php

namespace App\Api\V1\Domain\DTO;

class UserProfileIntegration
{
    public ?int $id = null;

    public ?string $name = null;

    /**
     * @var string[]
     */
    public array $userConfig = [];

    public ?UserScan $lastUserScan = null;
}
