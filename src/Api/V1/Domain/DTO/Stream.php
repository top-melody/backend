<?php

namespace App\Api\V1\Domain\DTO;

use IntegrationCore\Domain\DataProvider as IntegrationCoreDataProvider;

class Stream
{
    public ?IntegrationCoreDataProvider\Stream $stream = null;
    public ?string $mimeType = null;
    public ?int $sizeInBytes = null;
    public ?string $fileName = null;
}
