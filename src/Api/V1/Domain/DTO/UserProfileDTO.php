<?php

namespace App\Api\V1\Domain\DTO;

class UserProfileDTO
{
    /**
     * @var UserIntegrationConfig[]
     */
    public array $integrations = [];
}
