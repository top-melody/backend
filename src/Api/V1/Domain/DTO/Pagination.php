<?php

namespace App\Api\V1\Domain\DTO;

class Pagination
{
    public ?int $currentPage = null;
    public ?int $perPage = null;
    public ?int $totalCount = null;
}
