<?php

namespace App\Api\V1\Domain\DTO;

class TokenPair
{
    public ?string $token = null;
    public ?string $refreshToken = null;
}
