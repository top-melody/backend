<?php

namespace App\Api\V1\Domain\DTO;

class DictionaryItem
{
    public int|string|null $id = null;
    public ?string $name = null;
}
