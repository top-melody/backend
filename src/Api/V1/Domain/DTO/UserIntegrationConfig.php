<?php

namespace App\Api\V1\Domain\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class UserIntegrationConfig
{
    #[Assert\NotBlank]
    public ?int $integrationId = null;

    /**
     * @var string[]
     */
    public ?array $userConfig = [];
}
