<?php

namespace App\Api\V1\Domain\DTO;

class Track
{
    public ?int $id = null;
    public ?string $name = null;
    /** @var string[] */
    public array $authorNames = [];
    public ?int $duration = null;
    public bool $isOnMyPlaylist = false;
}
