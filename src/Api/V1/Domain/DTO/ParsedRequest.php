<?php

namespace App\Api\V1\Domain\DTO;

class ParsedRequest
{
    public mixed $request = null;
    /** string[][] */
    public array $errors = [];
}
