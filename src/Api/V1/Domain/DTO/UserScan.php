<?php

namespace App\Api\V1\Domain\DTO;

class UserScan
{
    public ?int $integrationId = null;
    public ?string $status = null;
    public ?string $createDateTime = null;
    public ?string $endDateTime = null;
}
