<?php

namespace App\Api\V1\Domain\Service\Track;

use App\Integration\Domain\Component as IntegrationComponent;
use App\Shared\Meta\ServiceInterface;
use IntegrationCore\Domain\Response as IntegrationCoreResponse;
use App\Api\V1\Infrastructure\Service as InfrastructureService;
use App\Api\V1\Infrastructure\Exception as InfrastructureException;
use App\Integration\Domain\Service as IntegrationDomainService;
use App\Shared\Domain\Entity;
use App\Api\V1\Domain\Exception as DomainException;
use App\Integration\Domain\Exception as IntegrationDomainException;
use App\Shared\Infrastructure\Repository;
use App\Shared\Domain\Service as SharedDomainService;
use App\Api\V1\Domain\DTO;

class GetStream implements ServiceInterface
{
    private ?int $trackId = null;

    public function __construct(
        readonly private Repository\Track $trackRepository,
        readonly private InfrastructureService\GetCurrentUser $getCurrentUserService,
        readonly private SharedDomainService\GetTrackFileName $getTrackFileNameService,
        readonly private IntegrationComponent\SdkFacade $sdkFacade,
    ) {
    }

    public function setTrackId(int $trackId): self
    {
        $this->trackId = $trackId;

        return $this;
    }

    /**
     * @throws DomainException\UserNotFound
     * @throws DomainException\CantCreateSDK
     * @throws DomainException\IntegrationError
     * @throws DomainException\TrackNotFound
     */
    public function service(): DTO\Stream
    {
        $track = $this->getTrack();
        $integration = $track->getIntegration();
        $user = $this->getUser();

        try {
            $getStreamResponse = $this->sdkFacade
                ->init($integration, $user)
                ->getStream($track->getDownloadUrl());
        } catch (IntegrationDomainException\SdkFacade\OperationFail | IntegrationDomainException\MaxRecursionTimes) {
            throw new DomainException\IntegrationError();
        } catch (IntegrationDomainException\IntegrationNotFound $exception) {
            throw new DomainException\CantCreateSDK();
        }

        if (!$getStreamResponse->getStream()) {
            throw new DomainException\IntegrationError();
        }

        $trackFileName = $this->getTrackFileNameService->setTrack($track)->service();
        return $this->buildResultDTO($getStreamResponse, $trackFileName);
    }

    /**
     * @return Entity\Track
     * @throws DomainException\TrackNotFound
     */
    private function getTrack(): Entity\Track
    {
        $track = $this->trackRepository->getById($this->trackId);
        if (!$track) {
            throw new DomainException\TrackNotFound();
        }

        return $track;
    }

    /**
     * @return Entity\User
     * @throws DomainException\UserNotFound
     */
    private function getUser(): Entity\User
    {
        try {
            return $this->getCurrentUserService->service();
        } catch (InfrastructureException\UserNotFound) {
            throw new DomainException\UserNotFound();
        }
    }

    private function buildResultDTO(IntegrationCoreResponse\GetStream $getStreamResponse, string $trackFileName): DTO\Stream
    {
        $dto = new DTO\Stream();
        $dto->stream = $getStreamResponse->getStream();
        $dto->sizeInBytes = $getStreamResponse->getSizeInBytes();
        $dto->mimeType = $getStreamResponse->getMimeType();
        $dto->fileName = $trackFileName;

        return $dto;
    }
}
