<?php

namespace App\Api\V1\Domain\Service;

use App\Api\V1\Domain\DTO;
use App\Api\V1\Infrastructure\Service as InfrastructureService;
use App\Shared\Infrastructure\Helper as InfrastructureHelper;
use App\Shared\Meta\ServiceInterface;

class PrepareRequest implements ServiceInterface
{
    private ?string $class = null;

    public function __construct(
        readonly private InfrastructureHelper\Objects $objectsHelper,
        readonly private InfrastructureService\GetRequest $getRequestService,
    ) {
    }

    public function setClass(string $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function service(): DTO\ParsedRequest
    {
        $response = new DTO\ParsedRequest();
        $response->request = $this->objectsHelper->fill($this->class, $this->getRequestService->service());
        $response->errors = $this->objectsHelper->getErrors($response->request);

        return $response;
    }
}
