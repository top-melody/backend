<?php

namespace App\Api\V1\Domain\Service\User;

use App\Api\V1\Domain\DataProvider;
use App\Api\V1\Domain\DTO;
use App\Api\V1\Domain\Exception as DomainException;
use App\Api\Shared\Infrastructure\Component as ApiSharedInfrastructureComponent;
use App\Shared\Domain\Entity;
use App\Shared\Meta\ServiceInterface;

class RefreshTokenPair implements ServiceInterface
{
    private ?DataProvider\RefreshToken $refreshTokenProvider = null;

    public function __construct(
        readonly private ApiSharedInfrastructureComponent\Token $tokenComponent,
        readonly private ApiSharedInfrastructureComponent\RefreshToken $refreshTokenComponent,
    ) {
    }

    public function setRefreshTokenProvider(DataProvider\RefreshToken $refreshTokenProvider): self
    {
        $this->refreshTokenProvider = $refreshTokenProvider;

        return $this;
    }

    /**
     * @throws DomainException\RefreshTokenNotFound
     * @throws DomainException\RefreshTokenNotValid
     */
    public function service(): DTO\TokenPair
    {
        /** @var Entity\RefreshToken $refreshToken */
        $refreshToken = $this->refreshTokenComponent->getByString($this->refreshTokenProvider->getRefreshToken());
        if (!$refreshToken) {
            throw new DomainException\RefreshTokenNotFound();
        }

        if (!$refreshToken->isValid()) {
            throw new DomainException\RefreshTokenNotValid();
        }

        $user = $refreshToken->getUser();
        $this->refreshTokenComponent->remove($refreshToken);

        $tokenPair = new DTO\TokenPair();
        $tokenPair->token = $this->tokenComponent->create($user);
        $tokenPair->refreshToken = $this->refreshTokenComponent->create($user);

        return $tokenPair;
    }
}
