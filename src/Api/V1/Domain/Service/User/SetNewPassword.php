<?php

namespace App\Api\V1\Domain\Service\User;

use App\Api\V1\Infrastructure\Service as InfrastructureService;
use App\Shared\Meta\ServiceInterface;
use App\Api\V1\Domain\DataProvider;
use App\Shared\Infrastructure\Component as SharedInfrastructureComponent;
use App\Api\V1\Domain\Exception;
use App\Api\V1\Infrastructure\Exception as InfrastructureException;
use App\Shared\Infrastructure\Repository;

class SetNewPassword implements ServiceInterface
{
    private ?DataProvider\Password $passwordProvider = null;

    public function __construct(
        readonly private Repository\User $userRepository,
        readonly private SharedInfrastructureComponent\Password $passwordComponent,
        readonly private InfrastructureService\GetCurrentUser $getCurrentUserService,
    ) {
    }

    public function setAuthDataProvider(DataProvider\Password $passwordProvider): self
    {
        $this->passwordProvider = $passwordProvider;

        return $this;
    }

    /**
     * @throws Exception\UserNotFound
     */
    public function service(): void
    {
        try {
            $user = $this->getCurrentUserService->service();
        } catch (InfrastructureException\UserNotFound) {
            throw new Exception\UserNotFound();
        }

        $newPasswordHash = $this->passwordComponent->getHash($user, $this->passwordProvider->getPassword());
        $user->setPassword($newPasswordHash);
        $this->userRepository->save($user);
    }
}
