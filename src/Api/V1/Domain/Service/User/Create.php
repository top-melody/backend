<?php

namespace App\Api\V1\Domain\Service\User;

use App\Api\V1\Domain\DataProvider;
use App\Api\V1\Domain\Exception as DomainException;
use App\Shared\Domain\Entity\User;
use App\Shared\Meta\ServiceInterface;
use App\Shared\Infrastructure\Component as SharedInfrastructureComponent;
use App\Shared\Infrastructure\Repository;

class Create implements ServiceInterface
{
    private ?DataProvider\AuthData $authDataProvider = null;

    public function __construct(
        readonly private Repository\User $userRepository,
        readonly private SharedInfrastructureComponent\Password $passwordComponent,
    ) {
    }

    public function setAuthDataProvider(DataProvider\AuthData $authDataProvider): self
    {
        $this->authDataProvider = $authDataProvider;

        return $this;
    }

    /**
     * @throws DomainException\UserAlreadyExist
     */
    public function service(): void
    {
        if ($this->userRepository->isExistByEmail($this->authDataProvider->getEmail())) {
            throw new DomainException\UserAlreadyExist();
        }

        $user = new User();
        $user->setEmail($this->authDataProvider->getEmail());
        $user->setPassword($this->passwordComponent->getHash($user, $this->authDataProvider->getPassword()));

        $this->userRepository->save($user);
    }
}
