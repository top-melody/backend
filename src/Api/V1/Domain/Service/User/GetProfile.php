<?php

namespace App\Api\V1\Domain\Service\User;

use App\Api\V1\Domain\DTO;
use App\Shared\Meta\ServiceInterface;
use App\Api\V1\Infrastructure\Service as InfrastructureService;
use App\Api\V1\Infrastructure\Exception as InfrastructureException;
use App\Api\V1\Domain\Exception;
use App\Shared\Domain\Entity;
use App\Shared\Infrastructure\Repository;
use App\Shared\Domain\Enum as SharedDomainEnum;

class GetProfile implements ServiceInterface
{
    public function __construct(
        readonly private InfrastructureService\GetCurrentUser $getCurrentUserService,
        readonly private Repository\Integration $integrationRepository,
        readonly private Repository\OperationScan $operationScanRepository,
    ) {
    }

    /**
     * @throws Exception\UserNotFound
     */
    public function service(): DTO\UserProfileDTO
    {
        $user = $this->getUser();

        $dto = new DTO\UserProfileDTO();
        $dto->integrations = array_map(
            fn (Entity\Integration $integration) => $this->getUserProfileIntegrationDTO($integration, $user),
            $this->integrationRepository->getAll()
        );

        return $dto;
    }

    /**
     * @throws Exception\UserNotFound
     */
    private function getUser(): Entity\User
    {
        try {
            return $this->getCurrentUserService->service();
        } catch (InfrastructureException\UserNotFound) {
            throw new Exception\UserNotFound();
        }
    }

    private function getUserProfileIntegrationDTO(Entity\Integration $integration, Entity\User $user): DTO\UserProfileIntegration
    {
        $dto = new DTO\UserProfileIntegration();
        $dto->id = $integration->getId();
        $dto->name = $integration->getName();
        $dto->userConfig = $this->getUserIntegrationConfig($user, $integration);
        $dto->lastUserScan = $this->getLastUserScanDTO($user, $integration);

        return $dto;
    }

    private function getUserIntegrationConfig(Entity\User $user, Entity\Integration $integration): array
    {
        foreach ($user->getUserIntegrationConfigs() as $userIntegrationConfig) {
            if ($userIntegrationConfig->getIntegration()->getId() === $integration->getId()) {
                return $userIntegrationConfig->getConfig() ?? [];
            }
        }

        return [];
    }

    private function getLastUserScanDTO(Entity\User $user, Entity\Integration $integration): DTO\UserScan
    {
        $operationScan = $this->operationScanRepository->getLastUserScanForIntegration($user, $integration);

        $dto = new DTO\UserScan();
        $dto->integrationId = $operationScan->getIntegration()->getId();
        $dto->status = $operationScan->getStatus();
        $dto->createDateTime = $operationScan->getCreatedAt()->format(SharedDomainEnum\Format::DATETIME);
        $dto->endDateTime = $operationScan->getUpdatedAt()->format(SharedDomainEnum\Format::DATETIME);

        return $dto;
    }
}
