<?php

namespace App\Api\V1\Domain\Service\User;

use App\Api\V1\Domain\Exception as DomainException;
use App\Api\Shared\Infrastructure\Component as ApiSharedInfrastructureComponent;
use App\Api\V1\Infrastructure\Exception as InfrastructureException;
use App\Api\V1\Infrastructure\Service as InfrastructureService;
use App\Shared\Meta\ServiceInterface;

class Logout implements ServiceInterface
{
    public function __construct(
        readonly private ApiSharedInfrastructureComponent\RefreshToken $refreshTokenComponent,
        readonly private InfrastructureService\GetCurrentUser $getCurrentUserService,
    ) {
    }

    /**
     * @throws DomainException\UserRefreshTokenNotFound
     * @throws DomainException\UserNotFound
     */
    public function service(): void
    {
        try {
            $activeRefreshToken = $this->refreshTokenComponent->getLast($this->getCurrentUserService->service());
        } catch (InfrastructureException\UserNotFound) {
            throw new DomainException\UserNotFound();
        }

        if ($activeRefreshToken) {
            $this->refreshTokenComponent->remove($activeRefreshToken);
            return;
        }

        throw new DomainException\UserRefreshTokenNotFound();
    }
}
