<?php

namespace App\Api\V1\Domain\Service\User;

use App\Api\V1\Domain\DataProvider;
use App\Api\V1\Domain\DTO;
use App\Api\V1\Domain\Exception as DomainException;
use App\Api\Shared\Infrastructure\Component as ApiSharedInfrastructureComponent;
use App\Shared\Infrastructure\Component as SharedInfrastructureComponent;
use App\Shared\Meta\ServiceInterface;
use App\Shared\Infrastructure\Repository;

class Login implements ServiceInterface
{
    private ?DataProvider\AuthData $authDataProvider = null;

    public function __construct(
        readonly private Repository\User $userRepository,
        readonly private SharedInfrastructureComponent\Password $passwordComponent,
        readonly private ApiSharedInfrastructureComponent\Token $tokenComponent,
        readonly private ApiSharedInfrastructureComponent\RefreshToken $refreshTokenComponent,
    ) {
    }

    public function setAuthDataProvider(DataProvider\AuthData $authDataProvider): self
    {
        $this->authDataProvider = $authDataProvider;

        return $this;
    }

    /**
     * @throws DomainException\UserNotFound
     * @throws DomainException\UserPasswordNotMatch
     */
    public function service(): DTO\TokenPair
    {
        $user = $this->userRepository->getByEmail($this->authDataProvider->getEmail());

        if (!$user) {
            throw new DomainException\UserNotFound();
        }

        if (!$this->passwordComponent->isValid($user, $this->authDataProvider->getPassword())) {
            throw new DomainException\UserPasswordNotMatch();
        }

        $refreshToken = $this->refreshTokenComponent->getLast($user);
        if (!$refreshToken) {
            $refreshToken = $this->refreshTokenComponent->create($user);
            $this->refreshTokenComponent->save($refreshToken);
        }

        $tokenPair = new DTO\TokenPair();
        $tokenPair->token = $this->tokenComponent->create($user);
        $tokenPair->refreshToken = $refreshToken;

        return $tokenPair;
    }
}
