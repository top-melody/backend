<?php

namespace App\Api\V1\Domain\Service\User;

use App\Api\V1\Domain\DataProvider;
use App\Api\V1\Domain\Exception as DomainException;
use App\Api\Shared\Infrastructure\Component as ApiSharedInfrastructureComponent;
use App\Email\Domain\Enum as EmailDomainEnum;
use App\Email\Domain\Request as EmailDomainRequest;
use App\Email\Infrastructure\Service as EmailInfrastructureService;
use App\Shared\Domain\Data as SharedDomainData;
use App\Shared\Domain\Entity;
use App\Shared\Infrastructure\Repository;
use App\Shared\Meta\ServiceInterface;

class PasswordRecovery implements ServiceInterface
{
    public function __construct(
        readonly private Repository\User $userRepository,
        readonly private EmailInfrastructureService\PushToQueue $sendEmailProducer,
        readonly private ApiSharedInfrastructureComponent\Token $tokenComponent,
    ) {
    }

    private ?DataProvider\Email $emailProvider = null;

    public function setEmailProvider(DataProvider\Email $emailProvider): self
    {
        $this->emailProvider = $emailProvider;

        return $this;
    }

    /**
     * @throws DomainException\UserNotFound
     */
    public function service(): void
    {
        $user = $this->userRepository->getByEmail($this->emailProvider->getEmail());
        if (!$user) {
            throw new DomainException\UserNotFound();
        }

        $request = $this->getRequest($user);
        $this->sendEmailProducer->setRequest($request)->service();
    }

    private function getRequest(Entity\User $user): EmailDomainRequest\PasswordRecovery
    {
        $request = new EmailDomainRequest\PasswordRecovery();
        $request->type = EmailDomainEnum\Type::PASSWORD_RECOVERY;
        $request->userId = $user->getId();
        $request->frontendUrl = implode('', [
            SharedDomainData\FrontendUrl::V1_SCHEMA,
            SharedDomainData\FrontendUrl::V1_DOMAIN,
            SharedDomainData\FrontendUrl::V1_NEW_PASSWORD
        ]);
        $request->token = $this->tokenComponent->create($user);
        $request->userEmail = $user->getEmail();

        return $request;
    }
}
