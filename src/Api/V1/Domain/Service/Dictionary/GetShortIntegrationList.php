<?php

namespace App\Api\V1\Domain\Service\Dictionary;

use App\Api\V1\Domain\DTO;
use App\Shared\Meta\ServiceInterface;
use App\Shared\Infrastructure\Repository;

class GetShortIntegrationList implements ServiceInterface
{
    public function __construct(
        readonly private Repository\Integration $integrationRepository,
    ) {
    }

    /**
     * @return DTO\DictionaryItem[]
     */
    public function service(): array
    {
        return array_map(
            function (array $integrationArray) {
                $dto = new DTO\DictionaryItem();
                $dto->id = $integrationArray['id'];
                $dto->name = $integrationArray['name'];

                return $dto;
            },
            $this->integrationRepository->getIdNameArray()
        );
    }
}
