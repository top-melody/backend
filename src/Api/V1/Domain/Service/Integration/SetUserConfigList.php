<?php

namespace App\Api\V1\Domain\Service\Integration;

use App\Api\V1\Domain\DataProvider;
use App\Api\V1\Domain\Exception as DomainException;
use App\Shared\Infrastructure\Exception as SharedInfrastructureException;
use App\Shared\Meta\ServiceInterface;
use App\Api\V1\Infrastructure\Service;
use App\Shared\Domain\Entity;
use App\Api\V1\Infrastructure\Exception as InfrastructureException;
use App\Shared\Domain\Exception as SharedException;
use App\Shared\Domain\Component as SharedComponent;
use App\Shared\Infrastructure\Repository;

class SetUserConfigList implements ServiceInterface
{
    public function __construct(
        readonly private Service\GetCurrentUser $getCurrentUserService,
        readonly private Repository\Integration $integrationRepository,
        readonly private Repository\UserIntegrationConfig $userIntegrationConfigRepository,
        readonly private SharedComponent\Transaction $transactionComponent,
    ) {
    }

    private ?DataProvider\UserIntegrationConfigList $userConfigListProvider = null;

    public function setUserIntegrationConfigListProvider(DataProvider\UserIntegrationConfigList $userConfigListProvider): self
    {
        $this->userConfigListProvider = $userConfigListProvider;

        return $this;
    }

    /**
     * @throws DomainException\UserNotFound
     * @throws SharedException\Transaction
     * @throws SharedException\Database
     * @throws DomainException\IntegrationNotFound
     */
    public function service(): void
    {
        $this->validateIntegrationIdList();
        $user = $this->getUser();

        try {
            $this->transactionComponent->begin();
            $this->updateUserIntegrationConfigList($user);
            $this->transactionComponent->commit();
        } catch (SharedException\Database $exception) {
            $this->transactionComponent->rollback();
            throw $exception;
        }
    }

    /**
     * @return void
     * @throws DomainException\IntegrationNotFound
     */
    private function validateIntegrationIdList(): void
    {
        $integrationIdList = $this->integrationRepository->getIdList();
        foreach ($this->userConfigListProvider->getUserIntegrationConfigList() as $userIntegrationConfigDTO) {
            if (!in_array($userIntegrationConfigDTO->integrationId, $integrationIdList)) {
                throw new DomainException\IntegrationNotFound();
            }
        }
    }

    /**
     * @throws SharedException\Database
     */
    private function updateUserIntegrationConfigList(Entity\User $user): void
    {
        $this->userIntegrationConfigRepository->removeAllForUser($user);

        $entities = [];
        foreach ($this->userConfigListProvider->getUserIntegrationConfigList() as $userIntegrationConfigDTO) {
            $integration = $this->getIntegration($userIntegrationConfigDTO->integrationId);

            $userIntegrationConfig = new Entity\UserIntegrationConfig();
            $userIntegrationConfig->setUser($user);
            $userIntegrationConfig->setIntegration($integration);
            $userIntegrationConfig->setConfig($userIntegrationConfigDTO->userConfig);
            $entities[] = $userIntegrationConfig;
        }

        $this->userIntegrationConfigRepository->saveMany($entities);
    }

    /**
     * @throws DomainException\UserNotFound
     */
    private function getUser(): Entity\User
    {
        try {
            return $this->getCurrentUserService->service();
        } catch (InfrastructureException\UserNotFound) {
            throw new DomainException\UserNotFound();
        }
    }

    /**
     * @param int $id
     * @return Entity\Integration
     * @throws SharedException\Database
     */
    private function getIntegration(int $id): Entity\Integration
    {
        try {
            return $this->integrationRepository->getProxy($id);
        } catch (SharedInfrastructureException\Database) {
            throw new SharedException\Database();
        }
    }
}
