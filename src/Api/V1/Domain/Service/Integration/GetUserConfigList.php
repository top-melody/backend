<?php

namespace App\Api\V1\Domain\Service\Integration;

use App\Shared\Domain\Entity;
use App\Shared\Meta\ServiceInterface;
use App\Api\V1\Domain\DTO;
use App\Api\V1\Infrastructure\Service as InfrastructureService;
use App\Shared\Infrastructure\Repository;
use App\Api\V1\Infrastructure\Exception as InfrastructureException;
use App\Api\V1\Domain\Exception as DomainException;

class GetUserConfigList implements ServiceInterface
{
    public function __construct(
        readonly private InfrastructureService\GetCurrentUser $getCurrentUserService,
        readonly private Repository\Integration $integrationRepository,
    ) {
    }

    /**
     * @return DTO\UserIntegrationConfig[]
     * @throws DomainException\UserNotFound
     */
    public function service(): array
    {
        try {
            $user = $this->getCurrentUserService->service();
        } catch (InfrastructureException\UserNotFound) {
            throw new DomainException\UserNotFound();
        }

        return array_map(
            function (Entity\Integration $integration) use ($user) {
                $userIntegrationConfig = $this->getOrCreateConfig($integration, $user);

                $dto = new DTO\UserIntegrationConfig();
                $dto->integrationId = $integration->getId();
                $dto->userConfig = $userIntegrationConfig->getConfig() ?? [];
                return $dto;
            },
            $this->integrationRepository->getAll()
        );
    }

    private function getOrCreateConfig(Entity\Integration $integration, Entity\User $user): Entity\UserIntegrationConfig
    {
        foreach ($user->getUserIntegrationConfigs() as $userIntegrationConfig) {
            if ($userIntegrationConfig->getIntegration()->getId() === $integration->getId()) {
                return $userIntegrationConfig;
            }
        }

        $config = new Entity\UserIntegrationConfig();
        $config->setConfig([]);
        $config->setUser($user);
        $config->setIntegration($integration);

        return $config;
    }
}
