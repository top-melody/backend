<?php

namespace App\Api\V1\Domain\Service\Integration;

use App\Api\V1\Domain\DTO;
use App\Api\V1\Domain\Exception\UserNotFound;
use App\Api\V1\Infrastructure\Service as InfrastructureService;
use App\Api\V1\Infrastructure\Exception as InfrastructureException;
use App\Api\V1\Domain\Exception as DomainException;
use App\Shared\Domain\Exception\Database;
use App\Shared\Infrastructure\Exception as SharedInfrastructureException;
use App\Shared\Domain\Exception as SharedDomainException;
use App\Shared\Meta\ServiceInterface;
use App\Shared\Infrastructure\Repository;
use App\Shared\Domain\Enum as SharedDomainEnum;

class GetLastUserScanList implements ServiceInterface
{
    public function __construct(
        readonly private InfrastructureService\GetCurrentUser $getCurrentUserService,
        readonly private Repository\OperationScan $operationScanRepository,
        readonly private Repository\Integration $integrationRepository,
    ) {
    }

    /**
     * @return DTO\UserScan[]
     * @throws UserNotFound
     * @throws Database
     */
    public function service(): array
    {
        try {
            $user = $this->getCurrentUserService->service();
        } catch (InfrastructureException\UserNotFound) {
            throw new DomainException\UserNotFound();
        }

        $userScanList = [];
        try {
            foreach ($this->integrationRepository->getIdListAsProxy() as $integration) {
                $operationScan = $this->operationScanRepository->getLastUserScanForIntegration($user, $integration);
                if (!$operationScan) {
                    continue;
                }

                $dto = new DTO\UserScan();
                $dto->integrationId = $operationScan->getIntegration()->getId();
                $dto->status = $operationScan->getStatus();
                $dto->createDateTime = $operationScan->getCreatedAt()->format(SharedDomainEnum\Format::DATETIME);
                $dto->endDateTime = $operationScan->getUpdatedAt()->format(SharedDomainEnum\Format::DATETIME);

                $userScanList[] = $dto;
            }
        } catch (SharedInfrastructureException\Database) {
            throw new SharedDomainException\Database();
        }

        return $userScanList;
    }
}
