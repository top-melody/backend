<?php

namespace App\Api\V1\Domain\Service\Playlist;

use App\Shared\Domain\Entity;
use App\Api\V1\Domain\DataProvider;

interface TrackListProvider
{
    public function setPlaylist(Entity\Playlist $playlist): self;

    public function setPlaylistParams(DataProvider\PlaylistParams $playlistParams): self;

    /**
     * @return Entity\Track[]
     */
    public function getTrackList(): array;

    public function getCount(): int;
}
