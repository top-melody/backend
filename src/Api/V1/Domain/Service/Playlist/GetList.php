<?php

namespace App\Api\V1\Domain\Service\Playlist;

use App\Api\V1\Domain\DTO;
use App\Api\V1\Infrastructure\Service\GetCurrentUser;
use App\Shared\Meta\ServiceInterface;
use App\Shared\Infrastructure\Repository;
use App\Shared\Domain\Entity;
use App\Api\V1\Infrastructure\Exception as InfrastructureException;
use App\Api\V1\Domain\Exception as DomainException;
use App\Shared\Domain\Enum as SharedDomainEnum;

class GetList implements ServiceInterface
{
    public function __construct(
        readonly private Repository\Playlist $playlistRepository,
        readonly private GetCurrentUser $getCurrentUserService,
    ) {
    }

    /**
     * @return DTO\DictionaryItem[]
     * @throws DomainException\UserNotFound
     */
    public function service(): array
    {
        $user = $this->getUser();
        $playlistsWithId = $this->getPlaylistsWithId($user);
        $enumPlaylists = $this->getEnumPlaylists();

        return array_merge($enumPlaylists, $playlistsWithId);
    }

    /**
     * @throws DomainException\UserNotFound
     */
    private function getUser(): Entity\User
    {
        try {
            return $this->getCurrentUserService->service();
        } catch (InfrastructureException\UserNotFound) {
            throw new DomainException\UserNotFound();
        }
    }

    /**
     * @return DTO\DictionaryItem[]
     */
    private function getPlaylistsWithId(Entity\User $user): array
    {
        $playlists = $this->playlistRepository->getByUser($user);
        $enumPlaylistNames = $this->getEnumPlaylistNames();

        $result = [];
        foreach ($playlists as $playlist) {
            if (!in_array($playlist->getName(), $enumPlaylistNames)) {
                $dto = new DTO\DictionaryItem();
                $dto->id = $playlist->getId();
                $dto->name = $playlist->getName();

                $result[] = $dto;
            }
        }

        return $result;
    }

    /**
     * @return string[]
     */
    private function getEnumPlaylistNames(): array
    {
        return array_column(SharedDomainEnum\Playlist::cases(), 'value');
    }

    private function getEnumPlaylists(): array
    {
        $result = [];
        foreach (SharedDomainEnum\Playlist::cases() as $playlistEnum) {
            $dto = new DTO\DictionaryItem();
            $dto->id = $playlistEnum->value;
            $dto->name = $playlistEnum->getName();

            $result[] = $dto;
        }

        return $result;
    }
}
