<?php

namespace App\Api\V1\Domain\Service\Playlist;

use App\Shared\Meta\ServiceInterface;
use App\Shared\Domain\Entity;
use App\Api\V1\Domain\Exception as DomainException;
use App\Shared\Infrastructure\Repository;
use App\Api\V1\Infrastructure\Service as InfrastructureService;
use App\Api\V1\Infrastructure\Exception as InfrastructureException;
use App\Shared\Domain\Enum as SharedDomainEnum;

class AddTrack implements ServiceInterface
{
    private int|string|null $playlistId = null;
    private ?int $trackId = null;

    public function __construct(
        readonly private InfrastructureService\GetCurrentUser $getCurrentUserService,
        readonly private Repository\Track $trackRepository,
        readonly private Repository\TrackToPlaylist $trackToPlaylistRepository,
        readonly private GetPlaylistById $getPlaylistByIdService,
    ) {
    }

    public function setPlaylistId(string|int $playlistId): self
    {
        $this->playlistId = $playlistId;

        return $this;
    }

    public function setTrackId(int $trackId): self
    {
        $this->trackId = $trackId;

        return $this;
    }

    /**
     * @throws DomainException\CantAddTrackToNewPlaylist
     * @throws DomainException\UserNotFound
     * @throws DomainException\PlaylistIdNotValid
     * @throws DomainException\PlaylistNotFound
     * @throws DomainException\UserHaveNotPlaylist
     * @throws DomainException\TrackNotFound
     * @throws DomainException\TrackAlreadyInPlaylist
     */
    public function service(): void
    {
        if ($this->playlistId == SharedDomainEnum\Playlist::New->value) {
            throw new DomainException\CantAddTrackToNewPlaylist();
        }

        $user = $this->getUser();
        $playlist = $this->getPlaylist($user);
        $track = $this->getTrack();
        if ($this->trackToPlaylistRepository->isTrackInPlaylist($playlist, $track)) {
            throw new DomainException\TrackAlreadyInPlaylist();
        }

        $trackToPlaylist = $this->getTrackToPlaylist($playlist, $track);
        $this->trackToPlaylistRepository->save($trackToPlaylist);
    }

    /**
     * @throws DomainException\UserNotFound
     */
    private function getUser(): Entity\User
    {
        try {
            return $this->getCurrentUserService->service();
        } catch (InfrastructureException\UserNotFound) {
            throw new DomainException\UserNotFound();
        }
    }

    /**
     * @throws DomainException\PlaylistIdNotValid
     * @throws DomainException\PlaylistNotFound
     * @throws DomainException\UserHaveNotPlaylist
     */
    private function getPlaylist(Entity\User $user): Entity\Playlist
    {
        return $this->getPlaylistByIdService
            ->setPlaylistId($this->playlistId)
            ->setUser($user)
            ->service();
    }

    /**
     * @throws DomainException\TrackNotFound
     */
    private function getTrack(): Entity\Track
    {
        $track = $this->trackRepository->getById($this->trackId);
        if (!$track) {
            throw new DomainException\TrackNotFound();
        }

        return $track;
    }

    private function getTrackToPlaylist(Entity\Playlist $playlist, Entity\Track $track): Entity\TrackToPlaylist
    {
        $entity = new Entity\TrackToPlaylist();
        $entity->setPlaylist($playlist);
        $entity->setTrack($track);

        return $entity;
    }
}
