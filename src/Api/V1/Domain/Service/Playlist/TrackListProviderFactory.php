<?php

namespace App\Api\V1\Domain\Service\Playlist;

use App\Api\V1\Domain\Service\Playlist\TrackListProvider\DefaultTrackListProvider;
use App\Api\V1\Domain\Service\Playlist\TrackListProvider\NewPlaylistTrackListProvider;
use App\Shared\Meta\ServiceInterface;
use App\Shared\Domain\Enum as SharedDomainEnum;

class TrackListProviderFactory implements ServiceInterface
{
    private string|int|null $playlistId = null;

    public function __construct(
        readonly private NewPlaylistTrackListProvider $newPlaylistTrackListProvider,
        readonly private DefaultTrackListProvider $defaultTrackListProvider,
    ) {
    }

    public function setPlaylistId(string|int $playlistId): self
    {
        $this->playlistId = $playlistId;

        return $this;
    }

    public function service(): TrackListProvider
    {
        return match ($this->playlistId) {
            SharedDomainEnum\Playlist::New->value => $this->newPlaylistTrackListProvider,
            default => $this->defaultTrackListProvider,
        };
    }
}
