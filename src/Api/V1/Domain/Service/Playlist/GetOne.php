<?php

namespace App\Api\V1\Domain\Service\Playlist;

use App\Api\V1\Domain\DataProvider;
use App\Api\V1\Domain\DTO;
use App\Shared\Infrastructure\Repository;
use App\Shared\Meta\ServiceInterface;
use App\Shared\Domain\Entity;
use App\Api\V1\Infrastructure\Service as InfrastructureService;
use App\Api\V1\Infrastructure\Exception as InfrastructureException;
use App\Api\V1\Domain\Exception as DomainException;
use App\Shared\Domain\Enum as SharedDomainEnum;

class GetOne implements ServiceInterface
{
    private ?DataProvider\PlaylistParams $playlistParams = null;

    public function __construct(
        readonly private Repository\Playlist $playlistRepository,
        readonly private Repository\TrackToPlaylist $trackToPlaylistRepository,
        readonly private InfrastructureService\GetCurrentUser $getCurrentUserService,
        readonly private GetPlaylistById $getPlaylistByIdService,
        readonly private TrackListProviderFactory $trackListProviderFactory,
    ) {
    }

    public function setPlaylistParamsProvider(DataProvider\PlaylistParams $playlistParams): self
    {
        $this->playlistParams = $playlistParams;

        return $this;
    }

    /**
     * @throws DomainException\UserNotFound
     * @throws DomainException\PlaylistIdNotValid
     * @throws DomainException\PlaylistNotFound
     * @throws DomainException\UserHaveNotPlaylist
     */
    public function service(): DTO\TrackListWithPagination
    {
        $user = $this->getUser();
        $playlist = $this->getPlaylist($user);
        $trackListProvider = $this->getTrackListProvider($playlist);
        $trackIdListFromMyPlaylist = $this->getTrackIdListFromMyPlaylist($user);

        $trackList = $this->getTrackList($trackListProvider);
        $pagination = $this->getPagination($trackListProvider);

        $dto = new DTO\TrackListWithPagination();
        $dto->trackList = array_map(
            fn (Entity\Track $track) => $this->mapTrackToDto($track, $trackIdListFromMyPlaylist),
            $trackList
        );
        $dto->pagination = $pagination;

        return $dto;
    }

    /**
     * @throws DomainException\UserNotFound
     */
    private function getUser(): Entity\User
    {
        try {
            return $this->getCurrentUserService->service();
        } catch (InfrastructureException\UserNotFound) {
            throw new DomainException\UserNotFound();
        }
    }

    /**
     * @throws DomainException\PlaylistIdNotValid
     * @throws DomainException\PlaylistNotFound
     * @throws DomainException\UserHaveNotPlaylist
     */
    private function getPlaylist(Entity\User $user): ?Entity\Playlist
    {
        return $this->getPlaylistByIdService
            ->setUser($user)
            ->setPlaylistId($this->playlistParams->getId())
            ->service();
    }

    private function getTrackListProvider(Entity\Playlist $playlist): TrackListProvider
    {
        $provider = $this->trackListProviderFactory
            ->setPlaylistId($this->playlistParams->getId())
            ->service();

        return $provider
            ->setPlaylist($playlist)
            ->setPlaylistParams($this->playlistParams);
    }

    /**
     * @return Entity\Track[]
     */
    private function getTrackList(TrackListProvider $trackListProvider): array
    {
        $trackList = $trackListProvider->getTrackList();

        if ($this->playlistParams->getShuffle()) {
            shuffle($trackList);
        }

        return $trackList;
    }

    private function getPagination(TrackListProvider $trackListProvider): DTO\Pagination
    {
        $dto = new DTO\Pagination();
        $dto->currentPage = $this->playlistParams->getPage();
        $dto->perPage = $this->playlistParams->getPerPage();
        $dto->totalCount = $this->getTrackCount($trackListProvider);

        return $dto;
    }

    private function getTrackCount(TrackListProvider $trackListProvider): int
    {
        return $trackListProvider->getCount();
    }

    /**
     * @param int[] $myPlaylistTrackIdList
     */
    private function mapTrackToDto(Entity\Track $track, array $myPlaylistTrackIdList): DTO\Track
    {
        $dto = new DTO\Track();
        $dto->id = $track->getId();
        $dto->name = $track->getName();
        $dto->duration = $track->getDuration();
        $dto->isOnMyPlaylist = in_array($track->getId(), $myPlaylistTrackIdList);
        $dto->authorNames = array_map(
            fn (Entity\Author $author) => $author->getName(),
            $track->getAuthors()->toArray()
        );

        return $dto;
    }

    /**
     * @return int[]
     */
    private function getTrackIdListFromMyPlaylist(Entity\User $user): array
    {
        $myPlaylist = $this->playlistRepository->getByUserAndName($user, SharedDomainEnum\Playlist::My->value);
        return $this->trackToPlaylistRepository->getTrackIdList($myPlaylist);
    }
}
