<?php

namespace App\Api\V1\Domain\Service\Playlist\TrackListProvider;

use App\Api\V1\Domain\Service\Playlist\TrackListProvider;
use App\Shared\Infrastructure\Repository;

class DefaultTrackListProvider extends AbstractTrackListProvider implements TrackListProvider
{
    public function __construct(
        readonly private Repository\Track $trackRepository,
        readonly private Repository\TrackToPlaylist $trackToPlaylistRepository,
    ) {
    }

    public function getTrackList(): array
    {
        return $this->trackRepository->getByPlaylistWithEagerAuthors(
            $this->playlist,
            $this->getOffset(),
            $this->playlistParams->getPerPage(),
            $this->playlistParams->getQuery()
        );
    }

    public function getCount(): int
    {
        return $this->trackToPlaylistRepository->getTrackCountInPlaylist($this->playlist);
    }
}
