<?php

namespace App\Api\V1\Domain\Service\Playlist\TrackListProvider;

use App\Api\V1\Domain\Service\Playlist\TrackListProvider;
use App\Shared\Infrastructure\Repository;

class NewPlaylistTrackListProvider extends AbstractTrackListProvider implements TrackListProvider
{
    public function __construct(
        readonly private Repository\Track $trackRepository,
    ) {
    }

    public function getTrackList(): array
    {
        return $this->trackRepository->getAllWithEagerAuthors(
            $this->getOffset(),
            $this->playlistParams->getPerPage(),
            $this->playlistParams->getQuery()
        );
    }

    public function getCount(): int
    {
        return $this->trackRepository->getCount();
    }
}
