<?php

namespace App\Api\V1\Domain\Service\Playlist\TrackListProvider;

use App\Api\V1\Domain\DataProvider;
use App\Api\V1\Domain\Service\Playlist\TrackListProvider;
use App\Shared\Domain\Entity;

abstract class AbstractTrackListProvider implements TrackListProvider
{
    protected ?Entity\Playlist $playlist = null;
    protected ?DataProvider\PlaylistParams $playlistParams = null;

    public function setPlaylist(Entity\Playlist $playlist): TrackListProvider
    {
        $this->playlist = $playlist;

        return $this;
    }

    public function setPlaylistParams(DataProvider\PlaylistParams $playlistParams): self
    {
        $this->playlistParams = $playlistParams;

        return $this;
    }

    protected function getOffset(): int
    {
        return ($this->playlistParams->getPage() - 1) * $this->playlistParams->getPerPage();
    }
}
