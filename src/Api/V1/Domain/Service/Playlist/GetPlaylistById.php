<?php

namespace App\Api\V1\Domain\Service\Playlist;

use App\Shared\Meta\ServiceInterface;
use App\Shared\Domain\Entity;
use App\Api\V1\Domain\Exception as DomainException;
use App\Shared\Infrastructure\Repository;
use App\Shared\Domain\Enum as SharedDomainEnum;

class GetPlaylistById implements ServiceInterface
{
    private ?Entity\User $user = null;
    private int|string|null $playlistId = null;

    public function setUser(Entity\User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function setPlaylistId(int|string $playlistId): self
    {
        $this->playlistId = $playlistId;

        return $this;
    }

    public function __construct(
        readonly private Repository\Playlist $playlistRepository,
    ) {
    }

    /**
     * @throws DomainException\PlaylistIdNotValid
     * @throws DomainException\PlaylistNotFound
     * @throws DomainException\UserHaveNotPlaylist
     */
    public function service(): Entity\Playlist
    {
        if (!is_numeric($this->playlistId)) {
            return $this->getEnumPlaylist();
        }

        return $this->getIdPlaylist();
    }

    /**
     * @throws DomainException\PlaylistIdNotValid
     */
    private function getEnumPlaylist(): Entity\Playlist
    {
        $playlistEnum = SharedDomainEnum\Playlist::tryFrom((string)$this->playlistId);
        if (!$playlistEnum) {
            throw new DomainException\PlaylistIdNotValid();
        }

        if ($playlistEnum === SharedDomainEnum\Playlist::New) {
            $playlist = new Entity\Playlist();
            $playlist->setName(SharedDomainEnum\Playlist::New->value);

            return $playlist;
        }

        $playlist = $this->playlistRepository->getByUserAndName($this->user, $playlistEnum->value);
        if (!$playlist) {
            $playlist = new Entity\Playlist();
            $playlist->setName($playlistEnum->value);
            $this->playlistRepository->save($playlist);
        }

        return $playlist;
    }

    /**
     * @throws DomainException\PlaylistNotFound
     * @throws DomainException\UserHaveNotPlaylist
     */
    private function getIdPlaylist(): Entity\Playlist
    {
        $playlist = $this->playlistRepository->getById((int)$this->playlistId);
        if (!$playlist) {
            throw new DomainException\PlaylistNotFound();
        }

        if (!$this->playlistRepository->isUserHavePlaylist($this->user, $playlist)) {
            throw new DomainException\UserHaveNotPlaylist();
        }

        return $playlist;
    }
}
