<?php

namespace App\Api\V1\Domain\DataProvider;

interface PlaylistParams
{
    public function getId(): int|string;
    public function getPage(): int;
    public function getQuery(): ?string;
    public function getShuffle(): bool;
    public function getPerPage(): int;
}
