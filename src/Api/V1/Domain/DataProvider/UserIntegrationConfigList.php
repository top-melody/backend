<?php

namespace App\Api\V1\Domain\DataProvider;

use App\Api\V1\Domain\DTO;

interface UserIntegrationConfigList
{
    /**
     * @return DTO\UserIntegrationConfig[]
     */
    public function getUserIntegrationConfigList(): array;
}
