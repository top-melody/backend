<?php

namespace App\Api\V1\Domain\DataProvider;

interface AuthData
{
    public function getEmail(): string;
    public function getPassword(): string;
}
