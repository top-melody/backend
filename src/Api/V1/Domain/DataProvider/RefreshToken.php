<?php

namespace App\Api\V1\Domain\DataProvider;

interface RefreshToken
{
    public function getRefreshToken(): string;
}
