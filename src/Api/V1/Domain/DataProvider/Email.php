<?php

namespace App\Api\V1\Domain\DataProvider;

interface Email
{
    public function getEmail(): string;
}
