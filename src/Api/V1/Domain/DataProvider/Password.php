<?php

namespace App\Api\V1\Domain\DataProvider;

interface Password
{
    public function getPassword(): string;
}
