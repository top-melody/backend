<?php

namespace App\Api\V1\Infrastructure\Service;

use App\Shared\Domain\Entity;
use App\Api\V1\Infrastructure\Exception as InfrastructureException;
use App\Shared\Meta\ServiceInterface;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class GetCurrentUser implements ServiceInterface
{
    public function __construct(
        readonly private TokenStorageInterface $tokenStorage
    ) {
    }

    /**
     * @throws InfrastructureException\UserNotFound
     */
    public function service(): Entity\User
    {
        $userInterface = $this->tokenStorage->getToken()->getUser();
        if (!$userInterface instanceof Entity\User) {
            throw new InfrastructureException\UserNotFound();
        }

        return $userInterface;
    }
}
