<?php

namespace App\Api\V1\Infrastructure\Service;

use App\Shared\Meta\ServiceInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class GetRequest implements ServiceInterface
{
    public function __construct(
        readonly private RequestStack $requestStack,
    ) {
    }

    public function service(): array
    {
        return $this->requestStack->getCurrentRequest()->request->all();
    }
}
