<?php

namespace App\Api\V1\Infrastructure\Exception;

use App\Shared\Infrastructure\Exception as SharedInfrastructureException;

class UserNotFound extends SharedInfrastructureException\Infrastructure
{
}
