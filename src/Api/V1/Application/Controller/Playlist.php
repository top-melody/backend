<?php

namespace App\Api\V1\Application\Controller;

use App\Api\Shared\Domain\Enum as ApiSharedDomainEnum;
use App\Api\Shared\Domain\Response as SharedDomainResponse;
use App\Api\V1\Domain\Service as DomainService;
use App\Api\V1\Application\Request\Playlist as Request;
use App\Api\V1\Application\Response\Playlist as Response;
use App\Api\V1\Domain\Exception as DomainException;
use App\Shared\Domain\Enum as SharedDomainEnum;

use Nelmio\ApiDocBundle\Annotation as NA;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Playlilst")
 */
#[Route('/v1/playlist')]
#[AsController]
class Playlist
{
    public function __construct(
        readonly private DomainService\PrepareRequest       $prepareRequestService,
        readonly private DomainService\Playlist\GetOne      $getOneService,
        readonly private DomainService\Playlist\GetList     $getListService,
        readonly private DomainService\Playlist\AddTrack    $addTrackService,
        readonly private DomainService\Playlist\DeleteTrack $deleteTrackService,
    ) {
    }

    /**
     * Получение плейлиста
     *
     * Список треков, входящих в плейлист
     *
     * @OA\RequestBody(
     *     @NA\Model(type=Request\GetOne::class)
     * )
     * @OA\Response(
     *     response="200",
     *     description="Успех",
     *     @NA\Model(type=Response\GetOne::class)
     * )
     * @OA\Response(
     *     response="401",
     *     ref="#/components/responses/AuthenticationFailure"
     * )
     * @OA\Response(
     *     response="403",
     *     ref="#/components/responses/JWTInvalid"
     * )
     * @OA\Response(
     *     response="404",
     *     ref="#/components/responses/JWTNotFound"
     * )
     * @OA\Response(
     *     response="406",
     *     ref="#/components/responses/JWTExpired"
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=SharedDomainResponse\Exception::class)
     * )
     * @NA\Security(name="Bearer")
     */
    #[Route('/one', methods: ['POST'])]
    public function getOne(): SharedDomainResponse\Json
    {
        $response = new Response\GetOne();
        $preparedRequest = $this->prepareRequestService
            ->setClass(Request\GetOne::class)
            ->service();

        if ($preparedRequest->errors) {
            $response->addMessage(SharedDomainEnum\Message::ValidationError);
            $response->setValidationErrors($preparedRequest->errors);
            return $response->makeResponse()->setStatus(ApiSharedDomainEnum\HttpCode::UnprocessableEntity);
        }

        try {
            $serviceResult = $this->getOneService
                ->setPlaylistParamsProvider($preparedRequest->request)
                ->service();
            $response->trackList = $serviceResult->trackList;
            $response->pagination = $serviceResult->pagination;
        } catch (
            DomainException\UserNotFound
            | DomainException\PlaylistNotFound
            | DomainException\PlaylistIdNotValid
            | DomainException\UserHaveNotPlaylist
            $exception
        ) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus($exception->getCodeEnum());
        }

        return $response->makeResponse();
    }

    /**
     * Получение списка плейлистов
     *
     * Список плейлистов, доступных пользователю
     *
     * @OA\Response(
     *     response="200",
     *     description="Успех",
     *     @NA\Model(type=Response\GetList::class)
     * )
     * @OA\Response(
     *     response="401",
     *     ref="#/components/responses/AuthenticationFailure"
     * )
     * @OA\Response(
     *     response="403",
     *     ref="#/components/responses/JWTInvalid"
     * )
     * @OA\Response(
     *     response="404",
     *     ref="#/components/responses/JWTNotFound"
     * )
     * @OA\Response(
     *     response="406",
     *     ref="#/components/responses/JWTExpired"
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=SharedDomainResponse\Exception::class)
     * )
     * @NA\Security(name="Bearer")
     */
    #[Route('/list', methods: ['GET'])]
    public function getList(): SharedDomainResponse\Json
    {
        $response = new Response\GetList();

        try {
            $response->items = $this->getListService->service();
        } catch (DomainException\UserNotFound $exception) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus($exception->getCodeEnum());
        }

        return $response->makeResponse();
    }

    /**
     * Добавление трека в плейлист
     *
     * Добавление трека в плейлист
     *
     * @OA\Response(
     *     response="200",
     *     description="Успех",
     *     @NA\Model(type=Response\AddTrack::class)
     * )
     * @OA\Response(
     *     response="401",
     *     ref="#/components/responses/AuthenticationFailure"
     * )
     * @OA\Response(
     *     response="403",
     *     ref="#/components/responses/JWTInvalid"
     * )
     * @OA\Response(
     *     response="404",
     *     ref="#/components/responses/JWTNotFound"
     * )
     * @OA\Response(
     *     response="406",
     *     ref="#/components/responses/JWTExpired"
     * )
     * @OA\Response(
     *     response="422",
     *     description="Нельзя добавлять трек в плейлист 'Новинки' | Неверный id плейлиста | Плейлист не найден | Трек уже добавлен в плейлист",
     *     @NA\Model(type=Response\AddTrack::class)
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=SharedDomainResponse\Exception::class)
     * )
     * @NA\Security(name="Bearer")
     */
    #[Route('/{playlistId}/track/{trackId}', methods: ['PUT'])]
    public function addTrack(int|string $playlistId, int $trackId): SharedDomainResponse\Json
    {
        $response = new Response\AddTrack();

        try {
            $this->addTrackService
                ->setPlaylistId($playlistId)
                ->setTrackId($trackId)
                ->service();
        } catch (
            DomainException\CantAddTrackToNewPlaylist
            | DomainException\UserNotFound
            | DomainException\PlaylistIdNotValid
            | DomainException\PlaylistNotFound
            | DomainException\UserHaveNotPlaylist
            | DomainException\TrackNotFound
            | DomainException\TrackAlreadyInPlaylist
            $exception
        ) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus($exception->getCodeEnum());
        }

        return $response->makeResponse();
    }

    /**
     * Удаление трека из плейлиста
     *
     * Удаление трека из плейлиста
     *
     * @OA\Response(
     *     response="200",
     *     description="Успех",
     *     @NA\Model(type=Response\DeleteTrack::class)
     * )
     * @OA\Response(
     *     response="401",
     *     ref="#/components/responses/AuthenticationFailure"
     * )
     * @OA\Response(
     *     response="403",
     *     ref="#/components/responses/JWTInvalid"
     * )
     * @OA\Response(
     *     response="404",
     *     ref="#/components/responses/JWTNotFound"
     * )
     * @OA\Response(
     *     response="406",
     *     ref="#/components/responses/JWTExpired"
     * )
     * @OA\Response(
     *     response="422",
     *     description="Нельзя удалять треки из плейлиста 'Новинки' | Неверный id плейлиста | Плейлист не найден | Трек не находится в плейлисте",
     *     @NA\Model(type=Response\DeleteTrack::class)
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=SharedDomainResponse\Exception::class)
     * )
     * @NA\Security(name="Bearer")
     */
    #[Route('/{playlistId}/track/{trackId}', methods: ['DELETE'])]
    public function deleteTrack(int|string $playlistId, int $trackId): SharedDomainResponse\Json
    {
        $response = new Response\DeleteTrack();

        try {
            $this->deleteTrackService
                ->setPlaylistId($playlistId)
                ->setTrackId($trackId)
                ->service();
        } catch (
            DomainException\CantDeleteTrackFromNewPlaylist
            | DomainException\UserNotFound
            | DomainException\PlaylistIdNotValid
            | DomainException\PlaylistNotFound
            | DomainException\UserHaveNotPlaylist
            | DomainException\TrackNotFound
            | DomainException\TrackIsNotInPlaylist
            $exception
        ) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus($exception->getCodeEnum());
        }

        return $response->makeResponse();
    }
}
