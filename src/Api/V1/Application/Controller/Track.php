<?php

namespace App\Api\V1\Application\Controller;

use App\Api\Shared\Domain\Enum as ApiSharedDomainEnum;
use App\Api\Shared\Domain\Response as SharedDomainResponse;
use App\Api\V1\Domain\Service\Track as DomainService;
use App\Api\V1\Application\Response\Track as Response;
use App\Api\V1\Domain\Exception as DomainException;

use Nelmio\ApiDocBundle\Annotation as NA;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Track")
 */
#[Route('/v1/track')]
#[AsController]
class Track
{
    public function __construct(
        readonly private DomainService\GetStream $getStreamService,
    ) {
    }

    /**
     * Стрим трека
     *
     * Скачивание трека напрямую из интеграции. Метод работает как прокси.
     *
     * @OA\Response(
     *     response="206",
     *     description="Успех",
     *     @OA\MediaType(
     *           mediaType="audio/mpeg",
     *           @OA\Schema(
     *               type="string",
     *               format="binary"
     *           ),
     *      ),
     * )
     * @OA\Response(
     *     response="401",
     *     ref="#/components/responses/AuthenticationFailure"
     * )
     * @OA\Response(
     *     response="403",
     *     ref="#/components/responses/JWTInvalid"
     * )
     * @OA\Response(
     *     response="404",
     *     ref="#/components/responses/JWTNotFound"
     * )
     * @OA\Response(
     *     response="406",
     *     ref="#/components/responses/JWTExpired"
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера | Проблема с инициализацией SDK",
     *     @NA\Model(type=SharedDomainResponse\Exception::class)
     * )
     * @OA\Response(
     *     response="503",
     *     description="Ошибка доступа к стороннему сервису",
     *     @NA\Model(type=Response\GetStream::class)
     * )
     * @NA\Security(name="Bearer")
     */
    #[Route('/{id}/stream', methods: ['GET'])]
    public function getStream(int $id): SharedDomainResponse\Stream | SharedDomainResponse\Json
    {
        $failResponse = new Response\GetStream();

        try {
            $dto = $this->getStreamService
                ->setTrackId($id)
                ->service();

            $streamResponse = new SharedDomainResponse\Stream(
                $dto->stream,
                $dto->fileName,
                $dto->mimeType,
                $dto->sizeInBytes,
            );
            return $streamResponse->setStatus(ApiSharedDomainEnum\HttpCode::PartialContent);
        } catch (
            DomainException\TrackNotFound
            | DomainException\UserNotFound
            | DomainException\CantCreateSDK
            | DomainException\IntegrationError $exception
        ) {
            $failResponse->addMessage($exception->getMessageEnum());
            return $failResponse->makeResponse()->setStatus($exception->getCodeEnum());
        }
    }
}
