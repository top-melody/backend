<?php

namespace App\Api\V1\Application\Controller;

use App\Api\V1\Application\Request\User as Request;
use App\Api\V1\Application\Response\User as Response;
use App\Api\V1\Domain\Exception as DomainException;
use App\Api\V1\Domain\Service as DomainService;
use App\Api\Shared\Domain\Enum as ApiSharedDomainEnum;
use App\Api\Shared\Domain\Response as ApiSharedDomainResponse;
use App\Shared\Domain\Enum as SharedDomainEnum;

use Nelmio\ApiDocBundle\Annotation as NA;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="User")
 */
#[Route('/v1/user')]
#[AsController]
class User
{
    public function __construct(
        readonly private DomainService\PrepareRequest $prepareRequestService,
        readonly private DomainService\User\Create $createService,
        readonly private DomainService\User\Login $loginService,
        readonly private DomainService\User\Logout $logoutService,
        readonly private DomainService\User\RefreshTokenPair $refreshTokenPairService,
        readonly private DomainService\User\PasswordRecovery $passwordRecoveryService,
        readonly private DomainService\User\SetNewPassword $setNewPasswordService,
        readonly private DomainService\User\GetProfile $getProfileService,
    ) {
    }

    /**
     * Авторизация
     *
     * Получение access и refresh токенов
     *
     * @OA\RequestBody(
     *     @NA\Model(type=Request\Login::class)
     * )
     * @OA\Response(
     *     response="200",
     *     description="Успех",
     *     @NA\Model(type=Response\Login::class)
     * )
     * @OA\Response(
     *     response="403",
     *     description="Неверный логин или пароль",
     *     @NA\Model(type=Response\Login::class)
     * )
     * @OA\Response(
     *     response="422",
     *     description="Ошибка валидации",
     *     @NA\Model(type=Response\Login::class)
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=ApiSharedDomainResponse\Exception::class)
     * )
     */
    #[Route('/login', methods: ['POST'])]
    public function login(): ApiSharedDomainResponse\Json
    {
        $response = new Response\Login();
        $preparedRequest = $this->prepareRequestService
            ->setClass(Request\Login::class)
            ->service();

        if ($preparedRequest->errors) {
            $response->addMessage(SharedDomainEnum\Message::ValidationError);
            $response->setValidationErrors($preparedRequest->errors);
            return $response->makeResponse()->setStatus(ApiSharedDomainEnum\HttpCode::UnprocessableEntity);
        }

        try {
            $tokenPair = $this->loginService
                ->setAuthDataProvider($preparedRequest->request)
                ->service();

            $response->token = $tokenPair->token;
            $response->refreshToken = $tokenPair->refreshToken;
        } catch (DomainException\UserNotFound | DomainException\UserPasswordNotMatch $exception) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus($exception->getCodeEnum());
        }

        return $response->makeResponse();
    }

    /**
     * Выход из системы
     *
     * Аннулирование refresh-токена
     *
     * @OA\Response(
     *     response="200",
     *     description="Успех",
     *     @NA\Model(type=Response\Logout::class)
     * )
     * @OA\Response(
     *     response="401",
     *     ref="#/components/responses/AuthenticationFailure"
     * )
     * @OA\Response(
     *     response="403",
     *     ref="#/components/responses/JWTInvalid"
     * )
     * @OA\Response(
     *     response="404",
     *     ref="#/components/responses/JWTNotFound"
     * )
     * @OA\Response(
     *     response="406",
     *     ref="#/components/responses/JWTExpired"
     * )
     * @OA\Response(
     *     response="422",
     *     description="Пользователь уже вышел из системы",
     *     @NA\Model(type=Response\Logout::class)
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=ApiSharedDomainResponse\Exception::class)
     * )
     * @NA\Security(name="Bearer")
     */
    #[Route('/logout', methods: ['POST'])]
    public function logout(): ApiSharedDomainResponse\Json
    {
        $response = new Response\Logout();

        try {
            $this->logoutService->service();
        } catch (DomainException\UserNotFound | DomainException\UserRefreshTokenNotFound $exception) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus($exception->getCodeEnum());
        }

        return $response->makeResponse();
    }

    /**
     * Регистрация
     *
     * Создание нового пользователя системы
     *
     * @OA\RequestBody(
     *     @NA\Model(type=Request\Register::class)
     * )
     * @OA\Response(
     *     response="201",
     *     description="Успех",
     *     @NA\Model(type=Response\Register::class)
     * )
     * @OA\Response(
     *     response="409",
     *     description="Пользователь с таким email уже существует",
     *     @NA\Model(type=Response\Register::class)
     * )
     * @OA\Response(
     *     response="422",
     *     description="Ошибка валидации",
     *     @NA\Model(type=Response\Register::class)
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=ApiSharedDomainResponse\Exception::class)
     * )
     */
    #[Route('/register', methods: ['PUT'])]
    public function register(): ApiSharedDomainResponse\Json
    {
        $response = new Response\Register();
        $preparedRequest = $this->prepareRequestService
            ->setClass(Request\Register::class)
            ->service();

        if ($preparedRequest->errors) {
            $response->addMessage(SharedDomainEnum\Message::ValidationError);
            $response->setValidationErrors($preparedRequest->errors);
            return $response->makeResponse()->setStatus(ApiSharedDomainEnum\HttpCode::UnprocessableEntity);
        }

        try {
            $this->createService
                ->setAuthDataProvider($preparedRequest->request)
                ->service();
        } catch (DomainException\UserAlreadyExist $exception) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus($exception->getCodeEnum());
        }

        return $response->makeResponse()->setStatus(ApiSharedDomainEnum\HttpCode::Created);
    }

    /**
     * Обновление токенов
     *
     * Получение новой пары токенов по refresh-токену
     *
     * @OA\RequestBody(
     *     @NA\Model(type=Request\RefreshTokenPair::class)
     * )
     * @OA\Response(
     *     response="200",
     *     description="Успех",
     *     @NA\Model(type=Response\RefreshTokenPair::class)
     * )
     * @OA\Response(
     *     response="401",
     *     description="Пользователь не авторизован",
     *     @NA\Model(type=Response\RefreshTokenPair::class)
     * )
     * @OA\Response(
     *     response="406",
     *     description="Refresh-токен просрочен. Авторизуйтесь заново",
     *     @NA\Model(type=Response\RefreshTokenPair::class)
     * )
     * @OA\Response(
     *     response="422",
     *     description="Ошибка валидации",
     *     @NA\Model(type=Response\RefreshTokenPair::class)
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=ApiSharedDomainResponse\Exception::class)
     * )
     */
    #[Route('/refresh-token-pair', methods: ['POST'])]
    public function refreshTokenPair(): ApiSharedDomainResponse\Json
    {
        $response = new Response\RefreshTokenPair();
        $preparedRequest = $this->prepareRequestService
            ->setClass(Request\RefreshTokenPair::class)
            ->service();

        if ($preparedRequest->errors) {
            $response->addMessage(SharedDomainEnum\Message::ValidationError);
            $response->setValidationErrors($preparedRequest->errors);
            return $response->makeResponse()->setStatus(ApiSharedDomainEnum\HttpCode::UnprocessableEntity);
        }

        try {
            $tokenPair = $this->refreshTokenPairService
                ->setRefreshTokenProvider($preparedRequest->request)
                ->service();

            $response->token = $tokenPair->token;
            $response->refreshToken = $tokenPair->refreshToken;
        } catch (DomainException\RefreshTokenNotFound | DomainException\RefreshTokenNotValid $exception) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus($exception->getCodeEnum());
        }

        return $response->makeResponse();
    }

    /**
     * Восстановление пароля
     *
     * Отправляет письмо на почту для восстановления пароля
     *
     * @OA\RequestBody(
     *     @NA\Model(type=Request\PasswordRecovery::class)
     * )
     * @OA\Response(
     *     response="200",
     *     description="Успех",
     *     @NA\Model(type=Response\PasswordRecovery::class)
     * )
     * @OA\Response(
     *     response="403",
     *     description="Пользователь не найден",
     *     @NA\Model(type=Response\PasswordRecovery::class)
     * )
     * @OA\Response(
     *     response="422",
     *     description="Ошибка валидации",
     *     @NA\Model(type=Response\PasswordRecovery::class)
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=ApiSharedDomainResponse\Exception::class)
     * )
     */
    #[Route('/password-recovery', methods: ['POST'])]
    public function passwordRecovery(): ApiSharedDomainResponse\Json
    {
        $response = new Response\PasswordRecovery();
        $preparedRequest = $this->prepareRequestService
            ->setClass(Request\PasswordRecovery::class)
            ->service();

        if ($preparedRequest->errors) {
            $response->addMessage(SharedDomainEnum\Message::ValidationError);
            $response->setValidationErrors($preparedRequest->errors);
            return $response->makeResponse()->setStatus(ApiSharedDomainEnum\HttpCode::UnprocessableEntity);
        }

        try {
            $this->passwordRecoveryService
                ->setEmailProvider($preparedRequest->request)
                ->service();
        } catch (DomainException\UserNotFound $exception) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus($exception->getCodeEnum());
        }

        return $response->makeResponse();
    }

    /**
     * Установка нового пароля
     *
     * Устанавливает новый пароль пользователя, токен которого передан в запрос
     *
     * @OA\RequestBody(
     *     @NA\Model(type=Request\SetNewPassword::class)
     * )
     * @OA\Response(
     *     response="200",
     *     description="Успех",
     *     @NA\Model(type=Response\SetNewPassword::class)
     * )
     * @OA\Response(
     *     response="401",
     *     ref="#/components/responses/AuthenticationFailure"
     * )
     * @OA\Response(
     *     response="403",
     *     ref="#/components/responses/JWTInvalid"
     * )
     * @OA\Response(
     *     response="404",
     *     ref="#/components/responses/JWTNotFound"
     * )
     * @OA\Response(
     *     response="406",
     *     ref="#/components/responses/JWTExpired"
     * )
     * @OA\Response(
     *     response="422",
     *     description="Ошибка валидации",
     *     @NA\Model(type=Response\SetNewPassword::class)
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=ApiSharedDomainResponse\Exception::class)
     * )
     * @NA\Security(name="Bearer")
     */
    #[Route('/set-new-password', methods: ['PATCH'])]
    public function setNewPassword(): ApiSharedDomainResponse\Json
    {
        $response = new Response\SetNewPassword();
        $preparedRequest = $this->prepareRequestService
            ->setClass(Request\SetNewPassword::class)
            ->service();

        if ($preparedRequest->errors) {
            $response->addMessage(SharedDomainEnum\Message::ValidationError);
            $response->setValidationErrors($preparedRequest->errors);
            return $response->makeResponse()->setStatus(ApiSharedDomainEnum\HttpCode::UnprocessableEntity);
        }

        try {
            $this->setNewPasswordService
                ->setAuthDataProvider($preparedRequest->request)
                ->service();
        } catch (DomainException\UserNotFound $exception) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus($exception->getCodeEnum());
        }

        return $response->makeResponse();
    }

    /**
     * Запрос данных профиля
     *
     * Комбинированный ответ из методов /dictionary/integrations,
     * /integration/user-config-list и /integration/last-user-scan-list
     *
     * @OA\Response(
     *     response="200",
     *     description="Успех",
     *     @NA\Model(type=Response\Profile::class)
     * )
     * @OA\Response(
     *     response="401",
     *     ref="#/components/responses/AuthenticationFailure"
     * )
     * @OA\Response(
     *     response="403",
     *     ref="#/components/responses/JWTInvalid"
     * )
     * @OA\Response(
     *     response="404",
     *     ref="#/components/responses/JWTNotFound"
     * )
     * @OA\Response(
     *     response="406",
     *     ref="#/components/responses/JWTExpired"
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=ApiSharedDomainResponse\Exception::class)
     * )
     * @NA\Security(name="Bearer")
     */
    #[Route('/profile', methods: ['GET'])]
    public function getProfile(): ApiSharedDomainResponse\Json
    {
        $response = new Response\Profile();

        try {
            $serviceResultDTO = $this->getProfileService->service();
            $response->integrations = $serviceResultDTO->integrations;
        } catch (DomainException\UserNotFound $exception) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus($exception->getCodeEnum());
        }

        return $response->makeResponse();
    }
}
