<?php

namespace App\Api\V1\Application\Controller;

use App\Api\V1\Application\Response\Dictionary as Response;
use App\Api\V1\Domain\Service\Dictionary as DomainService;
use App\Api\Shared\Domain\Response as SharedDomainResponse;

use Nelmio\ApiDocBundle\Annotation as NA;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Dictionary")
 */
#[Route('/v1/dictionary')]
#[AsController]
class Dictionary
{
    public function __construct(
        readonly private DomainService\GetShortIntegrationList $getIntegrationListService,
    ) {
    }

    /**
     * Интеграции
     *
     * Список интеграций
     *
     * @OA\Response(
     *     response="200",
     *     description="Успех",
     *     @NA\Model(type=Response\Items::class)
     * )
     * @OA\Response(
     *     response="401",
     *     ref="#/components/responses/AuthenticationFailure"
     * )
     * @OA\Response(
     *     response="403",
     *     ref="#/components/responses/JWTInvalid"
     * )
     * @OA\Response(
     *     response="404",
     *     ref="#/components/responses/JWTNotFound"
     * )
     * @OA\Response(
     *     response="406",
     *     ref="#/components/responses/JWTExpired"
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=SharedDomainResponse\Exception::class)
     * )
     * @NA\Security(name="Bearer")
     */
    #[Route('/integrations', methods: ['GET'])]
    public function integrations(): SharedDomainResponse\Json
    {
        $response = new Response\Items();
        $response->items = $this->getIntegrationListService->service();
        return $response->makeResponse();
    }
}
