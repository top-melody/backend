<?php

namespace App\Api\V1\Application\Controller;

use App\Api\V1\Application\Request\Integration as Request;
use App\Api\V1\Application\Response\Integration as Response;
use App\Api\V1\Domain\Service as DomainService;
use App\Api\V1\Domain\Exception as DomainException;
use App\Api\Shared\Domain\Response as SharedDomainResponse;
use App\Api\Shared\Domain\Enum as ApiSharedDomainEnum;
use App\Shared\Domain\Exception as SharedDomainException;
use App\Shared\Domain\Enum as SharedDomainEnum;

use Nelmio\ApiDocBundle\Annotation as NA;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Integration")
 */
#[Route('/v1/integration')]
#[AsController]
class Integration
{
    public function __construct(
        readonly private DomainService\PrepareRequest $prepareRequestService,
        readonly private DomainService\Integration\GetUserConfigList $getUserConfigListService,
        readonly private DomainService\Integration\SetUserConfigList $setUserConfigListService,
        readonly private DomainService\Integration\GetLastUserScanList $getLastUserScanListService,
    ) {
    }

    /**
     * Конфигурации интеграций пользователя
     *
     * Получение списка пользовательских конфигураций интеграций
     *
     * @OA\Response(
     *     response="200",
     *     description="Успех",
     *     @NA\Model(type=Response\GetIntegrationConfigList::class)
     * )
     * @OA\Response(
     *     response="401",
     *     ref="#/components/responses/AuthenticationFailure"
     * )
     * @OA\Response(
     *     response="403",
     *     ref="#/components/responses/JWTInvalid"
     * )
     * @OA\Response(
     *     response="404",
     *     ref="#/components/responses/JWTNotFound"
     * )
     * @OA\Response(
     *     response="406",
     *     ref="#/components/responses/JWTExpired"
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=SharedDomainResponse\Exception::class)
     * )
     * @NA\Security(name="Bearer")
     */
    #[Route('/user-config-list', methods: ['GET'])]
    public function getUserConfigList(): SharedDomainResponse\Json
    {
        $response = new Response\GetIntegrationConfigList();
        try {
            $response->userIntegrationConfigList = $this->getUserConfigListService->service();
        } catch (DomainException\UserNotFound $exception) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus($exception->getCodeEnum());
        }

        return $response->makeResponse();
    }

    /**
     * Конфигурации интеграций пользователя
     *
     * Установка списка пользовательских конфигураций интеграций
     *
     * @OA\RequestBody(
     *     @NA\Model(type=Request\SetUserConfigList::class)
     * )
     * @OA\Response(
     *     response="200",
     *     description="Успех",
     *     @NA\Model(type=Response\SetUserConfigList::class)
     * )
     * @OA\Response(
     *     response="401",
     *     ref="#/components/responses/AuthenticationFailure"
     * )
     * @OA\Response(
     *     response="403",
     *     ref="#/components/responses/JWTInvalid"
     * )
     * @OA\Response(
     *     response="404",
     *     ref="#/components/responses/JWTNotFound"
     * )
     * @OA\Response(
     *     response="406",
     *     ref="#/components/responses/JWTExpired"
     * )
     * @OA\Response(
     *     response="422",
     *     description="Ошибка валидации | Интеграция не найдена",
     *     @NA\Model(type=Response\SetUserConfigList::class)
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=SharedDomainResponse\Exception::class)
     * )
     * @OA\Response(
     *     response="503",
     *     description="Ошибка доступа к базе данных | Ошибка транзакционной операции",
     *     @NA\Model(type=Response\SetUserConfigList::class)
     * )
     * @NA\Security(name="Bearer")
     */
    #[Route('/user-config-list', methods: ['PATCH'])]
    public function setUserConfigList(): SharedDomainResponse\Json
    {
        $response = new Response\SetUserConfigList();
        $preparedRequest = $this->prepareRequestService
            ->setClass(Request\SetUserConfigList::class)
            ->service();

        if ($preparedRequest->errors) {
            $response->addMessage(SharedDomainEnum\Message::ValidationError);
            $response->setValidationErrors($preparedRequest->errors);
            return $response->makeResponse()->setStatus(ApiSharedDomainEnum\HttpCode::UnprocessableEntity);
        }

        try {
            $this->setUserConfigListService
                ->setUserIntegrationConfigListProvider($preparedRequest->request)
                ->service();
        } catch (DomainException\IntegrationNotFound | DomainException\UserNotFound $exception) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus($exception->getCodeEnum());
        } catch (SharedDomainException\Transaction | SharedDomainException\Database $exception) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus(ApiSharedDomainEnum\HttpCode::ServiceUnavailable);
        }

        return $response->makeResponse();
    }

    /**
     * Список пользовательских сканирований интеграций
     *
     * Получение списка последних пользовательских сканирований интеграций с группировкой по интеграции
     *
     * @OA\Response(
     *     response="200",
     *     description="Успех",
     *     @NA\Model(type=Response\GetLastUserScanList::class)
     * )
     * @OA\Response(
     *     response="401",
     *     ref="#/components/responses/AuthenticationFailure"
     * )
     * @OA\Response(
     *     response="403",
     *     ref="#/components/responses/JWTInvalid"
     * )
     * @OA\Response(
     *     response="404",
     *     ref="#/components/responses/JWTNotFound"
     * )
     * @OA\Response(
     *     response="406",
     *     ref="#/components/responses/JWTExpired"
     * )
     * @OA\Response(
     *     response="500",
     *     description="Ошибка сервера",
     *     @NA\Model(type=SharedDomainResponse\Exception::class)
     * )
     * @OA\Response(
     *     response="503",
     *     description="Ошибка доступа к базе данных",
     *     @NA\Model(type=Response\GetLastUserScanList::class)
     * )
     * @NA\Security(name="Bearer")
     */
    #[Route('/last-user-scan-list', methods: ['GET'])]
    public function getLastUserScanList(): SharedDomainResponse\Json
    {
        $response = new Response\GetLastUserScanList();

        try {
            $response->lastUserScanList = $this->getLastUserScanListService->service();
        } catch (DomainException\UserNotFound $exception) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus($exception->getCodeEnum());
        } catch (SharedDomainException\Database $exception) {
            $response->addMessage($exception->getMessageEnum());
            return $response->makeResponse()->setStatus(ApiSharedDomainEnum\HttpCode::ServiceUnavailable);
        }

        return $response->makeResponse();
    }
}
