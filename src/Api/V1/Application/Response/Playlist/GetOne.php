<?php

namespace App\Api\V1\Application\Response\Playlist;

use App\Api\Shared\Domain\Response as ApiSharedDomainResponse;
use App\Api\V1\Domain\DTO;

class GetOne extends ApiSharedDomainResponse\BaseApi
{
    /** @var array<\App\Api\V1\Domain\DTO\Track> */
    public array $trackList = [];
    public ?DTO\Pagination $pagination = null;
}
