<?php

namespace App\Api\V1\Application\Response\Playlist;

use App\Api\Shared\Domain\Response as ApiSharedDomainResponse;

class GetList extends ApiSharedDomainResponse\BaseApi
{
    /** @var array<\App\Api\V1\Domain\DTO\DictionaryItem> */
    public array $items = [];
}
