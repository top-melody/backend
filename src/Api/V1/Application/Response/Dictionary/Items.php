<?php

namespace App\Api\V1\Application\Response\Dictionary;

use App\Api\Shared\Domain\Response as ApiSharedDomainResponse;

class Items extends ApiSharedDomainResponse\BaseApi
{
    /** @var array<\App\Api\V1\Domain\DTO\DictionaryItem> */
    public array $items = [];
}
