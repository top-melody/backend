<?php

namespace App\Api\V1\Application\Response\Integration;

use App\Api\Shared\Domain\Response as ApiSharedDomainResponse;

class GetLastUserScanList extends ApiSharedDomainResponse\BaseApi
{
    /** @var array<\App\Api\V1\Domain\DTO\UserScan> */
    public array $lastUserScanList = [];
}
