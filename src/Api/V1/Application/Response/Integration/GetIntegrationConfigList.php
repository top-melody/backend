<?php

namespace App\Api\V1\Application\Response\Integration;

use App\Api\Shared\Domain\Response as ApiSharedDomainResponse;

class GetIntegrationConfigList extends ApiSharedDomainResponse\BaseApi
{
    /** @var array<\App\Api\V1\Domain\DTO\UserIntegrationConfig> */
    public array $userIntegrationConfigList = [];
}
