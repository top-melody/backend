<?php

namespace App\Api\V1\Application\Response\User;

use App\Api\Shared\Domain\Response as ApiSharedDomainResponse;

class Logout extends ApiSharedDomainResponse\BaseApi
{
}
