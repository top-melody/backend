<?php

namespace App\Api\V1\Application\Response\User;

use App\Api\Shared\Domain\Response as ApiSharedDomainResponse;

class Profile extends ApiSharedDomainResponse\BaseApi
{
    /**
     * @var array<\App\Api\V1\Domain\DTO\UserProfileIntegration>
     */
    public array $integrations = [];
}
