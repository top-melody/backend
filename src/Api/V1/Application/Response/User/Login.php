<?php

namespace App\Api\V1\Application\Response\User;

use App\Api\Shared\Domain\Response as ApiSharedDomainResponse;

class Login extends ApiSharedDomainResponse\BaseApi
{
    public ?string $token = null;
    public ?string $refreshToken = null;
}
