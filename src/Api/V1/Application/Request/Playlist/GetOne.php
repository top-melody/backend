<?php

namespace App\Api\V1\Application\Request\Playlist;

use App\Api\V1\Domain\DataProvider\PlaylistParams;
use Symfony\Component\Validator\Constraints as Assert;

class GetOne implements PlaylistParams
{
    /**
     * Можно задать тип плейлиста: my, new или id-шку, например, 123
     */
    #[Assert\NotBlank(message: 'Необходимо заполнить Id')]
    public ?string $id = null;

    #[Assert\GreaterThan(0, message: 'Страница должна быть положительным числом')]
    public int $page = 1;

    public ?string $query = null;

    public bool $shuffle = false;

    #[Assert\NotBlank(message: 'Необходимо заполнить количество элементов')]
    #[Assert\GreaterThan(0, message: 'Количество элементов должно быть положительным числом')]
    public int $perPage = 255;

    public function getId(): string
    {
        return $this->id;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getQuery(): ?string
    {
        return $this->query;
    }

    public function getShuffle(): bool
    {
        return $this->shuffle;
    }

    public function getPerPage(): int
    {
        return $this->perPage;
    }
}
