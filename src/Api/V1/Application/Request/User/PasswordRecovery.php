<?php

namespace App\Api\V1\Application\Request\User;

use App\Api\V1\Domain\DataProvider;

use Symfony\Component\Validator\Constraints as Assert;

class PasswordRecovery implements DataProvider\Email
{
    #[Assert\NotBlank]
    #[Assert\Email]
    public ?string $email = null;

    public function getEmail(): string
    {
        return $this->email;
    }
}
