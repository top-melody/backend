<?php

namespace App\Api\V1\Application\Request\User;

use App\Api\V1\Domain\DataProvider;

use Symfony\Component\Validator\Constraints as Assert;

class RefreshTokenPair implements DataProvider\RefreshToken
{
    #[Assert\NotBlank]
    public ?string $refreshToken = null;

    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }
}
