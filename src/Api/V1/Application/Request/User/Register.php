<?php

namespace App\Api\V1\Application\Request\User;

use App\Api\V1\Domain\DataProvider;

use Symfony\Component\Validator\Constraints as Assert;

class Register implements DataProvider\AuthData
{
    #[Assert\NotBlank]
    #[Assert\Email]
    public ?string $email = null;

    #[Assert\NotBlank]
    #[Assert\Length(min: 8, minMessage: 'Пароль должен содержать минимум 8 символов')]
    #[Assert\Regex('/[A-zА-яёЁ]+/u', message: 'Пароль должен содержать буквы')]
    #[Assert\Regex('/[0-9]+/', message: 'Пароль должен содержать цифры')]
    public ?string $password = null;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
