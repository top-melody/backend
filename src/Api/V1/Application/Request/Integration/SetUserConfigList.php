<?php

namespace App\Api\V1\Application\Request\Integration;

use App\Api\V1\Domain\DataProvider;

use Symfony\Component\Validator\Constraints as Assert;

class SetUserConfigList implements DataProvider\UserIntegrationConfigList
{
    /**
     * @var array<\App\Api\V1\Domain\DTO\UserIntegrationConfig>
     */
    #[Assert\NotBlank]
    public ?array $userIntegrationConfigList = null;

    public function getUserIntegrationConfigList(): array
    {
        return $this->userIntegrationConfigList;
    }
}
