<?php

namespace App\Shared\Meta;

interface ServiceInterface
{
    public function service();
}
