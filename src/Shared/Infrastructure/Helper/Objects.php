<?php

namespace App\Shared\Infrastructure\Helper;

use ClassTransformer\ClassTransformer;
use ReflectionObject;
use ReflectionProperty;
use stdClass;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Throwable;

class Objects
{
    public function __construct(
        readonly private ValidatorInterface $validator
    ) {
    }

    public function fill(string $class, array $data): mixed
    {
        try {
            return ClassTransformer::transform($class, $data);
        } catch (Throwable $exception) {
            return new $class();
        }
    }

    /**
     * Метод рекурсивно валидирует объект и возвращает ошибки в массиве
     * такой же структуры, как и объект.
     * Валидация работает на основе атрибутов объектов
     */
    public function getErrors(mixed $object): array
    {
        $errors = [];
        $violations = $this->validator->validate($object);

        /** @var ConstraintViolation $violation */
        foreach ($violations as $violation) {
            $errors[$violation->getPropertyPath()][] = $violation->getMessage();
        }

        $publicProperties = (new ReflectionObject($object))->getProperties(ReflectionProperty::IS_PUBLIC);
        foreach ($publicProperties as $property) {
            $name = $property->getName();
            $type = $property->getType()->getName();

            if (!$property->getType()->isBuiltin()) {
                $itemErrors = self::getErrors($object->$name);
                if ($itemErrors) {
                    $errors[$name] = $itemErrors;
                }
            };

            if ($type == 'array' && $object->$name) {
                foreach ($object->$name as $key => $arrayItem) {
                    if (!is_object($arrayItem) || ($arrayItem instanceof stdClass)) {
                        break;
                    }

                    $itemErrors = self::getErrors($arrayItem);
                    if ($itemErrors) {
                        $errors[$name][$key] = $itemErrors;
                    }
                }
            }
        }

        return $errors;
    }
}
