<?php

namespace App\Shared\Infrastructure\Component;

use Doctrine\DBAL\Exception as DoctrineException;
use Doctrine\ORM\EntityManagerInterface;
use App\Shared\Infrastructure\Exception as InfrastructureException;

class Transaction
{
    public function __construct(
        readonly private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * @throws InfrastructureException\Transaction
     */
    public function begin(): void
    {
        try {
            $this->entityManager->getConnection()->beginTransaction();
        } catch (DoctrineException) {
            throw new InfrastructureException\Transaction();
        }
    }

    /**
     * @throws InfrastructureException\Transaction
     */
    public function commit(): void
    {
        try {
            $this->entityManager->getConnection()->commit();
        } catch (DoctrineException) {
            throw new InfrastructureException\Transaction();
        }
    }

    /**
     * @throws InfrastructureException\Transaction
     */
    public function rollback(): void
    {
        try {
            $this->entityManager->getConnection()->rollBack();
        } catch (DoctrineException) {
            throw new InfrastructureException\Transaction();
        }
    }
}
