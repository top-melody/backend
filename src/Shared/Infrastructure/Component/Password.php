<?php

namespace App\Shared\Infrastructure\Component;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

class Password
{
    public function __construct(
        readonly private UserPasswordHasherInterface $hasher,
    ) {
    }

    public function getHash(PasswordAuthenticatedUserInterface $user, string $password): string
    {
        return $this->hasher->hashPassword($user, $password);
    }

    public function isValid(PasswordAuthenticatedUserInterface $user, string $password): bool
    {
        return $this->hasher->isPasswordValid($user, $password);
    }
}
