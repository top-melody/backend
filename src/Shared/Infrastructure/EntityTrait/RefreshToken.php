<?php

namespace App\Shared\Infrastructure\EntityTrait;

use App\Shared\Domain\Entity;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

trait RefreshToken
{
    public static function createForUserWithTtl(string $refreshToken, UserInterface $user, int $ttl): RefreshTokenInterface
    {
        /** @var Entity\RefreshToken $entity */
        /** @var Entity\User $user */
        $entity = parent::createForUserWithTtl($refreshToken, $user, $ttl);
        $entity->setUser($user);

        return $entity;
    }
}
