<?php

namespace App\Shared\Infrastructure\EntityTrait;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

trait CreatedAt
{
    #[ORM\Column]
    private ?DateTimeImmutable $createdAt = null;

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    #[ORM\PrePersist]
    public function autoSetCreatedAt(): void
    {
        $this->createdAt = new DateTimeImmutable('now');
    }
}
