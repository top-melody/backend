<?php

namespace App\Shared\Infrastructure\EntityTrait;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

trait UpdatedAt
{
    #[ORM\Column]
    private ?DateTimeImmutable $updatedAt = null;

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function autoSetUpdatedAt(): void
    {
        $this->updatedAt = new DateTimeImmutable('now');
    }
}
