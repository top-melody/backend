<?php

namespace App\Shared\Infrastructure\EntityTrait;

trait User
{
    public function eraseCredentials(): void
    {
    }

    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }
}
