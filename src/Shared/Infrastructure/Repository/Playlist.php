<?php

namespace App\Shared\Infrastructure\Repository;

use App\Shared\Domain\Entity;
use Doctrine\ORM;

/**
 * @method Entity\Playlist|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entity\Playlist|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entity\Playlist[]    findAll()
 * @method Entity\Playlist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method void                 save(Entity\Playlist $entity)
 * @method void                 saveMany(Entity\Playlist[] $entities)
 */
class Playlist extends Base
{
    protected function getEntityClass(): string
    {
        return Entity\Playlist::class;
    }

    public function getByUserAndName(Entity\User $user, string $name): ?Entity\Playlist
    {
        try {
            return $this->createQueryBuilder('p')
                ->join('p.users', 'u')
                ->where('u = :user')
                ->setParameter('user', $user)
                ->andWhere('p.name = :name')
                ->setParameter('name', $name)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (ORM\NonUniqueResultException) {
            return null;
        }
    }

    /**
     * @param Entity\User $user
     * @return Entity\Playlist[]
     */
    public function getByUser(Entity\User $user): array
    {
        return $this->createQueryBuilder('p')
            ->join('p.users', 'u')
            ->where('u = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    public function getById(int $id): ?Entity\Playlist
    {
        return $this->findOneBy(['id' => $id]);
    }

    public function isUserHavePlaylist(Entity\User $user, Entity\Playlist $playlist): bool
    {
        try {
            return (bool) $this->createQueryBuilder('p')
                ->leftJoin('p.users', 'u')
                ->where('u = :user')
                ->setParameter('user', $user)
                ->andWhere('p = :playlist')
                ->setParameter('playlist', $playlist)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (ORM\NonUniqueResultException) {
            return false;
        }
    }

    public function getByName(string $name): ?Entity\Playlist
    {
        try {
            return $this->createQueryBuilder('p')
                ->andWhere('p.name = :name')
                ->setParameter('name', $name)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (ORM\NonUniqueResultException) {
            return null;
        }
    }
}
