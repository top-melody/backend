<?php

namespace App\Shared\Infrastructure\Repository;

use App\Shared\Domain\Entity;
use Doctrine\ORM\Exception\ORMException;
use App\Shared\Infrastructure\Exception as InfrastructureException;

/**
 * @method Entity\Integration|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entity\Integration|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entity\Integration[]    findAll()
 * @method Entity\Integration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method void                    save(Entity\Integration $entity)
 * @method void                    saveMany(Entity\Integration[] $entities)
 */
class Integration extends Base
{
    protected function getEntityClass(): string
    {
        return Entity\Integration::class;
    }

    public function getIdNameArray(): array
    {
        return $this->createQueryBuilder('i')
            ->select(['i.id', 'i.name'])
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Entity\Integration[]
     */
    public function getAll(): array
    {
        return $this->createQueryBuilder('i')
            ->getQuery()
            ->getResult();
    }

    /**
     * @throws InfrastructureException\Database
     */
    public function getProxy(int $id): Entity\Integration
    {
        try {
            return $this->getEntityManager()->getReference(Entity\Integration::class, $id);
        } catch (ORMException) {
            throw new InfrastructureException\Database();
        }
    }

    /**
     * @return int[]
     */
    public function getIdList(): array
    {
        return $this->createQueryBuilder('i')
            ->select(['i.id'])
            ->getQuery()
            ->getSingleColumnResult();
    }

    /**
     * @return Entity\Integration[]
     * @throws InfrastructureException\Database
     */
    public function getIdListAsProxy(): array
    {
        return array_map(
            fn (int $id) => $this->getProxy($id),
            $this->getIdList(),
        );
    }
}
