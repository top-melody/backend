<?php

namespace App\Shared\Infrastructure\Repository;

use App\Shared\Domain\Entity;
use Doctrine\ORM;

/**
 * @method Entity\Track|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entity\Track|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entity\Track[]    findAll()
 * @method Entity\Track[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method void              save(Entity\Track $entity)
 * @method void              saveMany(Entity\Track[] $entities)
 */
class Track extends Base
{
    protected function getEntityClass(): string
    {
        return Entity\Track::class;
    }

    public function getById(int $id): ?Entity\Track
    {
        return $this->findOneBy(['id' => $id]);
    }

    /**
     * @return Entity\Track[]
     */
    public function getByPlaylistWithEagerAuthors(Entity\Playlist $playlist, int $offset = 0, int $limit = 255, ?string $searchString = null): array
    {
        $query = $this->createQueryBuilder('t')
            ->join('t.trackToPlaylists', 'ttp')
            ->where('ttp.playlist = :playlist')
            ->setParameter('playlist', $playlist)
            ->orderBy('ttp.createdAt', 'DESC');

        if ($searchString) {
            $query->join('t.authors', 'a')
                ->andWhere('a.name like :searchString OR t.name like :searchString')
                ->setParameter('searchString', $searchString . '%');
        }

        return $query
            ->getQuery()
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->setFetchMode(Entity\Author::class, 'authors', ORM\Mapping\ClassMetadataInfo::FETCH_EAGER)
            ->getResult();
    }

    /**
     * @return Entity\Track[]
     */
    public function getAllWithEagerAuthors(int $offset = 0, int $limit = 255, ?string $searchString = null): array
    {
        $query = $this->createQueryBuilder('t')
            ->orderBy('t.createdAt', 'DESC');

        if ($searchString) {
            $query->leftJoin('t.authors', 'a')
                ->andWhere('a.name like :searchString OR t.name like :searchString')
                ->setParameter('searchString', $searchString . '%');
        }

        return $query
            ->getQuery()
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->setFetchMode(Entity\Author::class, 'authors', ORM\Mapping\ClassMetadataInfo::FETCH_EAGER)
            ->getResult();
    }

    public function getCount(): int
    {
        try {
            return $this->createQueryBuilder('t')
                ->select('count(t.id)')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (ORM\NonUniqueResultException | ORM\NoResultException) {
            return 0;
        }
    }
}
