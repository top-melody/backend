<?php

namespace App\Shared\Infrastructure\Repository;

use App\Shared\Domain\Entity;
use Doctrine\ORM;

/**
 * @method Entity\OperationScan|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entity\OperationScan|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entity\OperationScan[]    findAll()
 * @method Entity\OperationScan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method void                      save(Entity\OperationScan $entity)
 * @method void                      saveMany(Entity\OperationScan[] $entities)
 */
class OperationScan extends Base
{
    protected function getEntityClass(): string
    {
        return Entity\OperationScan::class;
    }

    public function getLastUserScanForIntegration(Entity\User $user, Entity\Integration $integration): ?Entity\OperationScan
    {
        try {
            return $this->createQueryBuilder('oi')
                ->where('oi.user = :user')
                ->setParameter('user', $user)
                ->andWhere('oi.integration = :integration')
                ->setParameter('integration', $integration)
                ->orderBy('oi.id', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleResult();
        } catch (ORM\NoResultException | ORM\NonUniqueResultException) {
            return null;
        }
    }
}
