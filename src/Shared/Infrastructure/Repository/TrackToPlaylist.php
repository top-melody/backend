<?php

namespace App\Shared\Infrastructure\Repository;

use App\Shared\Domain\Entity;
use Doctrine\ORM;

/**
 * @method Entity\TrackToPlaylist|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entity\TrackToPlaylist|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entity\TrackToPlaylist[]    findAll()
 * @method Entity\TrackToPlaylist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method void                        save(Entity\TrackToPlaylist $entity)
 * @method void                        saveMany(Entity\TrackToPlaylist[] $entities)
 */
class TrackToPlaylist extends Base
{
    protected function getEntityClass(): string
    {
        return Entity\TrackToPlaylist::class;
    }

    /**
     * @return int[]
     */
    public function getTrackIdList(Entity\Playlist $playlist): array
    {
        return $this->createQueryBuilder('ttp')
            ->where('ttp.playlist = :playlist')
            ->setParameter('playlist', $playlist)
            ->select('IDENTITY(ttp.track)')
            ->getQuery()
            ->getSingleColumnResult();
    }

    public function getTrackCountInPlaylist(Entity\Playlist $playlist): int
    {
        try {
            return $this->createQueryBuilder('ttp')
                ->where('ttp.playlist = :playlist')
                ->setParameter('playlist', $playlist)
                ->select('count(ttp.id)')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (ORM\NoResultException | ORM\NonUniqueResultException) {
            return 0;
        }
    }

    public function isTrackInPlaylist(Entity\Playlist $playlist, Entity\Track $track): bool
    {
        try {
            return (bool) $this->createQueryBuilder('ttp')
                ->where('ttp.track = :track')
                ->setParameter('track', $track)
                ->andWhere('ttp.playlist = :playlist')
                ->setParameter('playlist', $playlist)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (ORM\NonUniqueResultException) {
            return false;
        }
    }

    public function delete(Entity\Playlist $playlist, Entity\Track $track): void
    {
        $this->getEntityManager()
            ->createQueryBuilder()
            ->delete(Entity\TrackToPlaylist::class, 'ttp')
            ->where('ttp.track = :track')
            ->setParameter('track', $track)
            ->andWhere('ttp.playlist = :playlist')
            ->setParameter('playlist', $playlist)
            ->getQuery()
            ->execute();
    }
}
