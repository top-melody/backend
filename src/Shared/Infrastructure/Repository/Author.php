<?php

namespace App\Shared\Infrastructure\Repository;

use App\Shared\Domain\Entity;

/**
 * @method Entity\Author|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entity\Author|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entity\Author[]    findAll()
 * @method Entity\Author[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method void               save(Entity\Author $entity)
 * @method void               saveMany(Entity\Author[] $entities)
 */
class Author extends Base
{
    protected function getEntityClass(): string
    {
        return Entity\Author::class;
    }
}
