<?php

namespace App\Shared\Infrastructure\Repository;

use App\Shared\Domain\Entity;

/**
 * @method Entity\IntegrationLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entity\IntegrationLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entity\IntegrationLog[]    findAll()
 * @method Entity\IntegrationLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method void                       save(Entity\IntegrationLog $entity)
 * @method void                       saveMany(Entity\IntegrationLog[] $entities)
 */
class IntegrationLog extends Base
{
    protected function getEntityClass(): string
    {
        return Entity\IntegrationLog::class;
    }
}
