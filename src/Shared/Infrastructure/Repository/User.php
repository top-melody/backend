<?php

namespace App\Shared\Infrastructure\Repository;

use App\Shared\Domain\Entity;

/**
 * @method Entity\User|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entity\User|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entity\User[]    findAll()
 * @method Entity\User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method void             save(Entity\User $entity)
 * @method void             saveMany(Entity\User[] $entities)
 */
class User extends Base
{
    protected function getEntityClass(): string
    {
        return Entity\User::class;
    }

    public function isExistByEmail(string $email): bool
    {
        return null !== $this->getByEmail($email);
    }

    public function getByEmail(string $email): ?Entity\User
    {
        return $this->findOneBy(['email' => $email]);
    }

    public function getById(int $id): ?Entity\User
    {
        return $this->findOneBy(['id' => $id]);
    }
}
