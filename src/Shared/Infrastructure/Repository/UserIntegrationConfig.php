<?php

namespace App\Shared\Infrastructure\Repository;

use App\Shared\Domain\Entity;
use Doctrine\ORM;

/**
 * @method Entity\UserIntegrationConfig|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entity\UserIntegrationConfig|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entity\UserIntegrationConfig[]    findAll()
 * @method Entity\UserIntegrationConfig[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method void                              save(Entity\UserIntegrationConfig $entity)
 * @method void                              saveMany(Entity\UserIntegrationConfig[] $entities)
 */
class UserIntegrationConfig extends Base
{
    protected function getEntityClass(): string
    {
        return Entity\UserIntegrationConfig::class;
    }

    public function removeAllForUser(Entity\User $user): void
    {
        $this->getEntityManager()->createQueryBuilder()
            ->delete(Entity\UserIntegrationConfig::class, 'uic')
            ->where('uic.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->execute();
    }

    public function getForIntegrationAndUser(Entity\Integration $integration, Entity\User $user): ?Entity\UserIntegrationConfig
    {
        try {
            return $this->createQueryBuilder('uic')
                ->where('uic.integration = :integration')
                ->setParameter('integration', $integration)
                ->andWhere('uic.user = :user')
                ->setParameter('user', $user)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (ORM\NonUniqueResultException) {
            return null;
        }
    }
}
