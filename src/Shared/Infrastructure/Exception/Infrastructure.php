<?php

namespace App\Shared\Infrastructure\Exception;

use Exception;

abstract class Infrastructure extends Exception
{
    public function __construct()
    {
        parent::__construct();
    }
}
