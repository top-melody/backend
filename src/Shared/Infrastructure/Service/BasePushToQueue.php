<?php

namespace App\Shared\Infrastructure\Service;

use App\Shared\Meta\ServiceInterface;
use OldSound\RabbitMqBundle\RabbitMq\Producer;

abstract class BasePushToQueue extends Producer implements ServiceInterface
{
    private array|object $request = [];

    public function setRequest(array|object $request): self
    {
        $this->request = $request;

        return $this;
    }

    public function service(): void
    {
        $message = json_encode($this->request);
        $this->publish($message);
    }
}
