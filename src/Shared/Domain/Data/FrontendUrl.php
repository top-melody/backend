<?php

namespace App\Shared\Domain\Data;

class FrontendUrl
{
    public const V1_SCHEMA = 'http://';
    public const V1_DOMAIN = 'top-melody.ru';
    public const V1_NEW_PASSWORD = '/new-password';
}
