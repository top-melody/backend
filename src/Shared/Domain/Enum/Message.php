<?php

namespace App\Shared\Domain\Enum;

enum Message: string
{
    case InvalidToken = 'Неверный токен';
    case TokenExpired = 'Токен просрочен. Необходимо получить новый с помощью refresh-токена';
    case TokenNotFound = 'Необходимо указать Bearer-токен';
    case RefreshTokenExpired = 'Refresh-токен просрочен. Авторизуйтесь заново';
    case InvalidEmailOrPassword = 'Неверный email или пароль';
    case UserNotAuthorized = 'Пользователь не авторизован';
    case UserEmailIsBusy = 'Пользователь с таким email уже существует';
    case UserNotFound = 'Пользователь не найден';
    case ServerError = 'Ошибка сервера';
    case TransactionError = 'Ошибка транзакционной операции';
    case DatabaseError = 'Ошибка доступа к базе данных';
    case ValidationError = 'Ошибка валидации';
    case EmailTypeNotFound = 'Тип письма не определен';
    case EmailGeneratorNotFound = 'Генератор письма не объявлен в фабрике';
    case EmailRequestNotFound = 'Request письма не объявлен в фабрике';
    case IntegrationNotFound = 'Интеграция не найдена';
    case CantCreateSDK = 'Проблема с инициализацией SDK';
    case TrackNotFound = 'Трек не найден';
    case IntegrationError = 'Ошибка доступа к стороннему сервису';
    case PlaylistNotFound = 'Плейлист не найден';
    case PlaylistIdNotValid = 'Неверный id плейлиста';
    case UserHaveNotPlaylist = 'Плейлист недоступен пользователю';
    case CantAddTrackToNewPlaylist = 'Нельзя добавлять трек в плейлист "Новинки"';
    case CantDeleteTrackFromNewPlaylist = 'Нельзя удалять треки из плейлиста "Новинки"';
    case TrackAlreadyInPlaylist = 'Трек уже добавлен в плейлист';
    case TrackIsNotInPlaylist = 'Трек не находится в плейлисте';
    case MaxRecursionTimes = 'Превышено количество входов в рекурсию вызова SDK';
    case AuthFail = 'Ошибка аутентификации';
    case GetStreamFail = 'Ошибка получения стрима трека';
    case CollectUserInfoFail = 'Ошибка получения информации о пользователе';

}
