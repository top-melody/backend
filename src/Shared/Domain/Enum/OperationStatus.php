<?php

namespace App\Shared\Domain\Enum;

enum OperationStatus: string
{
    case Created = 'created';
    case InProgress = 'inProgress';
    case CompletedSuccessfully = 'completedSuccessfully';
    case CompletedWithError = 'completedWithError';
    case Cancelled = 'cancelled';
}
