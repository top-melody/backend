<?php

namespace App\Shared\Domain\Enum;

enum QueueStatus: int
{
    case Ok = 1;
    case OkAndRequeue = 2;
    case FailAndRequeue = 0;
    case Fail = -1;
}
