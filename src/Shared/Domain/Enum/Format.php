<?php

namespace App\Shared\Domain\Enum;

class Format
{
    public const DATETIME = 'd.m.Y H:i:s';
}
