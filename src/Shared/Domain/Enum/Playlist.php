<?php

namespace App\Shared\Domain\Enum;

enum Playlist: string
{
    case My = 'my';
    case New = 'new';

    public function getName(): string
    {
        return match ($this) {
            self::My => 'Мой плейлист',
            self::New => 'Новинки',
        };
    }
}
