<?php

namespace App\Shared\Domain\Entity;

use App\Shared\Infrastructure\Repository;
use App\Shared\Infrastructure\EntityTrait;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: Repository\Playlist::class)]
#[ORM\Table(name: '`playlist`')]
#[ORM\HasLifecycleCallbacks]
class Playlist
{
    use EntityTrait\CreatedAt;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'playlist', targetEntity: TrackToPlaylist::class, cascade: ['persist'])]
    private Collection $trackToPlaylists;

    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'playlists', cascade: ['persist'])]
    #[ORM\JoinTable(name: 'user_to_playlist')]
    private Collection $users;

    public function __construct()
    {
        $this->trackToPlaylists = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, TrackToPlaylist>
     */
    public function getTrackToPlaylists(): Collection
    {
        return $this->trackToPlaylists;
    }

    public function addTrackToPlaylist(TrackToPlaylist $trackToPlaylist): self
    {
        if (!$this->trackToPlaylists->contains($trackToPlaylist)) {
            $this->trackToPlaylists[] = $trackToPlaylist;
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }
}
