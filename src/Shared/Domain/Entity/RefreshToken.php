<?php

namespace App\Shared\Domain\Entity;

use DateTimeImmutable;
use App\Shared\Infrastructure\EntityTrait;

use Doctrine\ORM\Mapping as ORM;
use Gesdinet\JWTRefreshTokenBundle\Entity\RefreshToken as BaseRefreshToken;
use Gesdinet\JWTRefreshTokenBundle\Entity\RefreshTokenRepository;

#[ORM\Entity(repositoryClass: RefreshTokenRepository::class)]
#[ORM\Table(name: '`refresh_token`')]
#[ORM\HasLifecycleCallbacks]
class RefreshToken extends BaseRefreshToken
{
    use EntityTrait\RefreshToken;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['persist'])]
    private User $user;

    #[ORM\Column]
    private ?DateTimeImmutable $createdAt;

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    #[ORM\PrePersist]
    public function autoSetCreatedAt(): void
    {
        $this->createdAt = new DateTimeImmutable('now');
    }
}
