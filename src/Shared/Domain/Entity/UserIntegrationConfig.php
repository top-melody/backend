<?php

namespace App\Shared\Domain\Entity;

use App\Shared\Infrastructure\Repository;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: Repository\UserIntegrationConfig::class)]
#[ORM\Table(name: '`user_integration_config`')]
class UserIntegrationConfig
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?array $config = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['persist'])]
    private ?User $user = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: Integration::class, cascade: ['persist'])]
    private ?Integration $integration = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConfig(): ?array
    {
        return $this->config;
    }

    public function setConfig(?array $config): self
    {
        $this->config = $config;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIntegration(): ?Integration
    {
        return $this->integration;
    }

    public function setIntegration(Integration $integration): self
    {
        $this->integration = $integration;

        return $this;
    }
}
