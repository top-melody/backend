<?php

namespace App\Shared\Domain\Entity;

use App\Shared\Infrastructure\Repository;
use App\Shared\Infrastructure\EntityTrait;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: Repository\User::class)]
#[ORM\Table(name: '`user`')]
#[ORM\HasLifecycleCallbacks]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use EntityTrait\User;
    use EntityTrait\CreatedAt;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    #[ORM\ManyToMany(targetEntity: Playlist::class, inversedBy: 'users', cascade: ['persist'])]
    #[ORM\JoinTable(name: 'user_to_playlist')]
    private Collection $playlists;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: OperationScan::class, cascade: ['persist'])]
    private Collection $operationScans;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: UserIntegrationConfig::class, cascade: ['persist'])]
    private Collection $userIntegrationConfigs;

    public function __construct()
    {
        $this->playlists = new ArrayCollection();
        $this->operationScans = new ArrayCollection();
        $this->userIntegrationConfigs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    #[ORM\Column]
    private ?string $password = null;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRoles(): array
    {
        return array_unique($this->roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection<int, Playlist>
     */
    public function getPlaylists(): Collection
    {
        return $this->playlists;
    }

    public function addPlaylist(Playlist $playlist): self
    {
        if (!$this->playlists->contains($playlist)) {
            $this->playlists[] = $playlist;
        }

        return $this;
    }

    /**
     * @return Collection<int, OperationScan>
     */
    public function getOperationScans(): Collection
    {
        return $this->operationScans;
    }

    public function addOperationScan(OperationScan $operationScan): self
    {
        if (!$this->operationScans->contains($operationScan)) {
            $this->operationScans[] = $operationScan;
        }

        return $this;
    }

    /**
     * @return Collection<int, UserIntegrationConfig>
     */
    public function getUserIntegrationConfigs(): Collection
    {
        return $this->userIntegrationConfigs;
    }

    public function addUserIntegrationConfig(UserIntegrationConfig $userIntegrationConfig): self
    {
        if (!$this->userIntegrationConfigs->contains($userIntegrationConfig)) {
            $this->userIntegrationConfigs[] = $userIntegrationConfig;
        }

        return $this;
    }
}
