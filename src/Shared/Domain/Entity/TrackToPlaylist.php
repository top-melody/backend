<?php

namespace App\Shared\Domain\Entity;

use App\Shared\Infrastructure\Repository;
use App\Shared\Infrastructure\EntityTrait;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: Repository\TrackToPlaylist::class)]
#[ORM\Table(name: '`track_to_playlist`')]
#[ORM\HasLifecycleCallbacks]
class TrackToPlaylist
{
    use EntityTrait\CreatedAt;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: Track::class, cascade: ['persist'])]
    private ?Track $track = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: Playlist::class, cascade: ['persist'])]
    private ?Playlist $playlist = null;

    public function getTrack(): ?Track
    {
        return $this->track;
    }

    public function setTrack(?Track $track): self
    {
        $this->track = $track;

        return $this;
    }

    public function getPlaylist(): ?Playlist
    {
        return $this->playlist;
    }

    public function setPlaylist(?Playlist $playlist): self
    {
        $this->playlist = $playlist;

        return $this;
    }
}
