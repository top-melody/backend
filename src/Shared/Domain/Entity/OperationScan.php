<?php

namespace App\Shared\Domain\Entity;

use App\Shared\Infrastructure\EntityTrait;
use App\Shared\Infrastructure\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: Repository\OperationScan::class)]
#[ORM\Table(name: '`operation_scan`')]
#[ORM\HasLifecycleCallbacks]
class OperationScan
{
    use EntityTrait\CreatedAt;
    use EntityTrait\UpdatedAt;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 63, nullable: false)]
    private ?string $status = null;

    /**
     * @var string[]
     */
    #[ORM\Column(nullable: true)]
    private array $errors = [];

    #[ORM\Column(length: 1023, nullable: true)]
    private ?array $stackTrace = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: Integration::class, cascade: ['persist'])]
    private ?Integration $integration;

    #[ORM\JoinColumn(nullable: true)]
    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['persist'])]
    private ?User $user;

    #[ORM\ManyToMany(targetEntity: Track::class, inversedBy: 'operationScans', cascade: ['persist'])]
    #[ORM\JoinTable(name: 'operation_scan_to_track')]
    private Collection $tracks;

    public function __construct()
    {
        $this->tracks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param string[] $errors
     */
    public function setErrors(array $errors): self
    {
        $this->errors = $errors;

        return $this;
    }

    public function addError(string $error): self
    {
        $this->errors[] = $error;

        return $this;
    }

    public function getStackTrace(): ?array
    {
        return $this->stackTrace;
    }

    public function setStackTrace(?array $stackTrace): self
    {
        $this->stackTrace = $stackTrace;

        return $this;
    }

    public function getIntegration(): Integration
    {
        return $this->integration;
    }

    public function setIntegration(Integration $integration): self
    {
        $this->integration = $integration;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Track>
     */
    public function getTracks(): Collection
    {
        return $this->tracks;
    }

    public function addTrack(Track $track): self
    {
        if (!$this->tracks->contains($track)) {
            $this->tracks[] = $track;
        }

        return $this;
    }

    /**
     * @param Track[] $tracks
     */
    public function setTracks(array $tracks): self
    {
        $this->tracks->clear();
        foreach ($tracks as $track) {
            $this->addTrack($track);
        }

        return $this;
    }
}
