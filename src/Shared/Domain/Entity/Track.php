<?php

namespace App\Shared\Domain\Entity;

use App\Shared\Infrastructure\EntityTrait;
use App\Shared\Infrastructure\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: Repository\Track::class)]
#[ORM\Table(name: '`track`')]
#[ORM\HasLifecycleCallbacks]
class Track
{
    use EntityTrait\CreatedAt;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column]
    private ?int $duration = null;

    #[ORM\Column(length: 1024)]
    private ?string $downloadUrl = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: Integration::class, cascade: ['persist'])]
    private ?Integration $integration;

    #[ORM\ManyToMany(targetEntity: Author::class, inversedBy: 'tracks', cascade: ['persist'])]
    #[ORM\JoinTable(name: 'track_to_author')]
    private Collection $authors;

    #[ORM\OneToMany(mappedBy: 'track', targetEntity: TrackToPlaylist::class, cascade: ['persist'])]
    private Collection $trackToPlaylists;

    #[ORM\ManyToMany(targetEntity: OperationScan::class, mappedBy: 'tracks', cascade: ['persist'])]
    #[ORM\JoinTable(name: 'operation_scan_to_track')]
    private Collection $operationScans;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
        $this->trackToPlaylists = new ArrayCollection();
        $this->operationScans = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getDownloadUrl(): ?string
    {
        return $this->downloadUrl;
    }

    public function setDownloadUrl(?string $downloadUrl): self
    {
        $this->downloadUrl = $downloadUrl;

        return $this;
    }

    public function getIntegration(): ?Integration
    {
        return $this->integration;
    }

    public function setIntegration(?Integration $integration): self
    {
        $this->integration = $integration;

        return $this;
    }

    /**
     * @return Collection<int, Author>
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function addAuthor(Author $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors[] = $author;
        }

        return $this;
    }

    /**
     * @param Author[] $authors
     */
    public function setAuthors(array $authors): self
    {
        $this->authors->clear();

        foreach ($authors as $author) {
            $this->addAuthor($author);
        }

        return $this;
    }

    /**
     * @return Collection<int, TrackToPlaylist>
     */
    public function getTrackToPlaylists(): Collection
    {
        return $this->trackToPlaylists;
    }

    public function addTrackToPlaylist(TrackToPlaylist $trackToPlaylist): self
    {
        if (!$this->trackToPlaylists->contains($trackToPlaylist)) {
            $this->trackToPlaylists[] = $trackToPlaylist;
        }

        return $this;
    }

    /**
     * @return Collection<int, OperationScan>
     */
    public function getOperationScans(): Collection
    {
        return $this->operationScans;
    }

    public function addOperationScan(OperationScan $operationScan): self
    {
        if (!$this->operationScans->contains($operationScan)) {
            $this->operationScans[] = $operationScan;
        }

        return $this;
    }
}
