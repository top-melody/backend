<?php

namespace App\Shared\Domain\Entity;

use App\Shared\Infrastructure\Repository;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: Repository\IntegrationLog::class)]
#[ORM\Table(name: '`integration_log`')]
class IntegrationLog
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: false)]
    private ?bool $isSuccess = null;

    /**
     * @var string[]
     */
    #[ORM\Column(nullable: true)]
    private array $errors = [];

    #[ORM\Column(length: 127)]
    private ?string $type = null;

    #[ORM\Column(length: 511, nullable: true)]
    private ?string $requestUrl = null;

    #[ORM\Column(type: 'text', length: 64000, nullable: true)]
    private ?string $request = null;

    #[ORM\Column(type: 'text', length: 64000, nullable: true)]
    private ?string $response = null;

    #[ORM\Column(nullable: true)]
    private array $requestHeaders = [];

    #[ORM\Column(nullable: true)]
    private array $responseHeaders = [];

    #[ORM\Column(nullable: true)]
    private ?DateTimeImmutable $executionStartDateTime = null;

    #[ORM\Column(nullable: true)]
    private ?DateTimeImmutable $executionEndDateTime = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: Integration::class, cascade: ['persist'])]
    private ?Integration $integration;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsSuccess(): ?bool
    {
        return $this->isSuccess;
    }

    public function setIsSuccess(bool $isSuccess): void
    {
        $this->isSuccess = $isSuccess;
    }

    /**
     * @return string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param string[] $errors
     */
    public function setErrors(array $errors): self
    {
        $this->errors = $errors;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getRequestUrl(): ?string
    {
        return $this->requestUrl;
    }

    public function setRequestUrl(?string $requestUrl): self
    {
        $this->requestUrl = $requestUrl ? mb_strimwidth($requestUrl, 0, 511) : null;

        return $this;
    }

    public function getRequest(): ?string
    {
        return $this->request;
    }

    public function setRequest(?string $request): self
    {
        $this->request = $request ? mb_strimwidth($request, 0, 64000) : null;

        return $this;
    }

    public function getResponse(): ?string
    {
        return $this->response;
    }

    public function setResponse(?string $response): self
    {
        $this->response = $response ? mb_strimwidth($response, 0, 64000) : null;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getRequestHeaders(): array
    {
        return $this->requestHeaders;
    }

    /**
     * @param string[] $requestHeaders
     */
    public function setRequestHeaders(array $requestHeaders): self
    {
        $this->requestHeaders = $requestHeaders;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getResponseHeaders(): array
    {
        return $this->responseHeaders;
    }

    /**
     * @param string[] $responseHeaders
     */
    public function setResponseHeaders(array $responseHeaders): self
    {
        $this->responseHeaders = $responseHeaders;

        return $this;
    }

    public function getExecutionStartDateTime(): ?DateTimeImmutable
    {
        return $this->executionStartDateTime;
    }

    public function setExecutionStartDateTime(?DateTimeImmutable $executionStartDateTime): self
    {
        $this->executionStartDateTime = $executionStartDateTime;

        return $this;
    }

    public function getExecutionEndDateTime(): ?DateTimeImmutable
    {
        return $this->executionEndDateTime;
    }

    public function setExecutionEndDateTime(?DateTimeImmutable $executionEndDateTime): self
    {
        $this->executionEndDateTime = $executionEndDateTime;

        return $this;
    }

    public function getIntegration(): ?Integration
    {
        return $this->integration;
    }

    public function setIntegration(?Integration $integration): self
    {
        $this->integration = $integration;

        return $this;
    }
}
