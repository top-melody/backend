<?php

namespace App\Shared\Domain\Entity;

use App\Shared\Infrastructure\Repository;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

#[ORM\Entity(repositoryClass: Repository\Integration::class)]
#[ORM\Table(name: '`integration`')]
class Integration
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $name = null;

    #[ORM\Column(nullable: true)]
    private ?array $config = null;

    #[ORM\Column(length: 511, nullable: false)]
    private ?string $class = null;

    #[ORM\OneToMany(mappedBy: 'integration', targetEntity: Track::class, cascade: ['persist'])]
    private Collection $tracks;

    #[ORM\OneToMany(mappedBy: 'integration', targetEntity: IntegrationLog::class, cascade: ['persist'])]
    private Collection $integrationLogs;

    #[ORM\OneToMany(mappedBy: 'integration', targetEntity: OperationScan::class, cascade: ['persist'])]
    private Collection $operationScans;

    #[ORM\OneToMany(mappedBy: 'integration', targetEntity: UserIntegrationConfig::class, cascade: ['persist'])]
    private Collection $userIntegrationConfigs;

    public function __construct()
    {
        $this->tracks = new ArrayCollection();
        $this->integrationLogs = new ArrayCollection();
        $this->operationScans = new ArrayCollection();
        $this->userIntegrationConfigs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function setConfig(array $config): self
    {
        $this->config = $config;

        return $this;
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function setClass(string $class): self
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @return Collection<int, Track>
     */
    public function getTracks(): Collection
    {
        return $this->tracks;
    }

    public function addTrack(Track $track): self
    {
        if (!$this->tracks->contains($track)) {
            $this->tracks[] = $track;
        }

        return $this;
    }

    /**
     * @return Collection<int, IntegrationLog>
     */
    public function getIntegrationLogs(): Collection
    {
        return $this->integrationLogs;
    }

    public function addIntegrationLog(IntegrationLog $integrationLog): self
    {
        if (!$this->integrationLogs->contains($integrationLog)) {
            $this->integrationLogs[] = $integrationLog;
        }

        return $this;
    }

    /**
     * @return Collection<int, OperationScan>
     */
    public function getOperationScans(): Collection
    {
        return $this->operationScans;
    }

    public function addOperationScan(OperationScan $operationScan): self
    {
        if (!$this->operationScans->contains($operationScan)) {
            $this->operationScans[] = $operationScan;
        }

        return $this;
    }

    /**
     * @return Collection<int, UserIntegrationConfig>
     */
    public function getUserIntegrationConfigs(): Collection
    {
        return $this->userIntegrationConfigs;
    }

    public function addUserIntegrationConfig(UserIntegrationConfig $userIntegrationConfig): self
    {
        if (!$this->userIntegrationConfigs->contains($userIntegrationConfig)) {
            $this->userIntegrationConfigs[] = $userIntegrationConfig;
        }

        return $this;
    }
}
