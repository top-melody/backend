<?php

namespace App\Shared\Domain\Service;

use App\Shared\Meta\ServiceInterface;
use App\Shared\Domain\Entity;

class GetTrackFileName implements ServiceInterface
{
    private const TEMPLATE = '%s - %s';

    private ?Entity\Track $track = null;

    public function setTrack(Entity\Track $track): self
    {
        $this->track = $track;

        return $this;
    }

    public function service(): string
    {
        return sprintf(self::TEMPLATE, $this->getAuthorsPart(), $this->track->getName());
    }

    private function getAuthorsPart(): string
    {
        $authors = $this->track->getAuthors()->toArray();
        $authorNames = array_map(fn (Entity\Author $author) => $author->getName(), $authors);
        return implode(', ', $authorNames) ?: 'Неизвестный автор';
    }
}
