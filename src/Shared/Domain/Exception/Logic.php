<?php

namespace App\Shared\Domain\Exception;

use App\Shared\Domain\Enum as DomainEnum;
use Exception;

abstract class Logic extends Exception
{
    public function __construct()
    {
        parent::__construct($this->getMessageEnum()->value);
    }

    abstract public function getMessageEnum(): DomainEnum\Message;
}
