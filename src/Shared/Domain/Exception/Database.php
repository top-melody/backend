<?php

namespace App\Shared\Domain\Exception;

use App\Shared\Domain\Enum as DomainEnum;

class Database extends Logic
{
    public function getMessageEnum(): DomainEnum\Message
    {
        return DomainEnum\Message::DatabaseError;
    }
}
