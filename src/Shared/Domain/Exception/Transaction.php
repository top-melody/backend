<?php

namespace App\Shared\Domain\Exception;

use App\Shared\Domain\Enum as DomainEnum;

class Transaction extends Logic
{
    public function getMessageEnum(): DomainEnum\Message
    {
        return DomainEnum\Message::TransactionError;
    }
}
