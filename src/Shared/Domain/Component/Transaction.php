<?php

namespace App\Shared\Domain\Component;

use App\Shared\Infrastructure\Component as InfrastructureComponent;
use App\Shared\Infrastructure\Exception as InfrastructureException;
use App\Shared\Domain\Exception as DomainException;

class Transaction
{
    public function __construct(
        readonly private InfrastructureComponent\Transaction $transactionComponent,
    ) {
    }

    /**
     * @throws DomainException\Transaction
     */
    public function begin(): void
    {
        try {
            $this->transactionComponent->begin();
        } catch (InfrastructureException\Transaction) {
            throw new DomainException\Transaction();
        }
    }

    /**
     * @throws DomainException\Transaction
     */
    public function commit(): void
    {
        try {
            $this->transactionComponent->commit();
        } catch (InfrastructureException\Transaction) {
            throw new DomainException\Transaction();
        }
    }

    /**
     * @throws DomainException\Transaction
     */
    public function rollback(): void
    {
        try {
            $this->transactionComponent->rollback();
        } catch (InfrastructureException\Transaction) {
            throw new DomainException\Transaction();
        }
    }
}
