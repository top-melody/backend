### Тестирование

##### Настройка
```shell
docker exec -it tm-php symfony console secrets:set AKISMET_KEY --env=test
docker exec -it tm-php symfony console doctrine:database:create --env=test
```
Почитать больше о настройке можно здесь - https://symfony.com/doc/current/the-fast-track/ru/17-tests.html#nastrojka-testovogo-okruzenia

##### Запуск тестов API из консоли
```shell
make -f ./../Makefile test-all
```

##### Принципы
Тесты используют отдельную БД с постфиксом _test. По умолчанию база пуста. Все,
созданные в рамках теста, сущности должны удаляться сразу после завершения
теста или класса теста.

Переменная среды APP_ENV во время тестирования устанавливается в значение
"test" (автоматически).
