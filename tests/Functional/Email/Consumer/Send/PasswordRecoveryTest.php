<?php

namespace App\Tests\Functional\Email\Consumer\Send;

use App\Email\Domain\Enum;
use App\Email\Domain\Request;
use App\Shared\Domain\Enum as SharedEnum;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Trait;

class PasswordRecoveryTest extends WebTestCase
{
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Entity\Remove\Users;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Create\RegularUser;
    use Trait\Dependency\SetUpRegularUser;
    use Trait\Dependency\SetUpSendEmailConsumer;
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Action\LoginRegularUserAndGetTokenPair;

    private const FAKE_USER_TOKEN = 'token123456';
    private const LOCAL_FRONTEND_URL = 'http://localhost:8080/new-password';
    private const FAKE_USER_ID = -1;
    private const FAKE_USER_EMAIL = 'test@test.test';

    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();
        $this->createRegularUser($this->entityManager, $this->hasher);
        $this->setUpRegularUser($this->entityManager);
        $this->setUpSendEmailConsumer();
    }

    protected function tearDown(): void
    {
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testSuccess(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $request = new Request\PasswordRecovery();
        $request->token = $tokenPair->token;
        $request->frontendUrl = self::LOCAL_FRONTEND_URL;
        $request->type = Enum\Type::PASSWORD_RECOVERY;
        $request->userId = $this->regularUser->getId();
        $request->userEmail = $this->regularUser->getEmail();

        $messageBody = json_encode($request);
        $message = new AMQPMessage($messageBody);

        $status = $this->sendEmailConsumer->execute($message);
        $this->assertEquals(SharedEnum\QueueStatus::Ok->value, $status);
    }

    public function testUserNotFound(): void
    {
        $request = new Request\PasswordRecovery();
        $request->token = self::FAKE_USER_TOKEN;
        $request->frontendUrl = self::LOCAL_FRONTEND_URL;
        $request->type = Enum\Type::PASSWORD_RECOVERY;
        $request->userId = self::FAKE_USER_ID;
        $request->userEmail = self::FAKE_USER_EMAIL;

        $messageBody = json_encode($request);
        $message = new AMQPMessage($messageBody);

        $status = $this->sendEmailConsumer->execute($message);
        $this->assertEquals(SharedEnum\QueueStatus::Fail->value, $status);
    }
}
