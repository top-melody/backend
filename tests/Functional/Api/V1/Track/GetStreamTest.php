<?php

namespace App\Tests\Functional\Api\V1\Track;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Data;
use TestsComponents\Trait;

class GetStreamTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Dependency\SetUpVkIntegration;
    use Trait\Dependency\SetUpIntegrationWithFakeClassPath;
    use Trait\Dependency\SetUpRegularUser;
    use Trait\Dependency\SetUpTrack;
    use Trait\Dependency\SetUpTrackWithFakeDownloadUrl;
    use Trait\Dependency\SetUpTrackWithIntegrationWhereFakeClassPath;
    use Trait\Entity\Create\RegularUser;
    use Trait\Entity\Create\VkIntegration;
    use Trait\Entity\Create\IntegrationWithFakeClassPath;
    use Trait\Entity\Create\Track;
    use Trait\Entity\Create\TrackWithFakeDownloadUrl;
    use Trait\Entity\Create\TrackWithIntegrationWhereFakeClassPath;
    use Trait\Entity\Remove\Tracks;
    use Trait\Entity\Remove\Integrations;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Remove\Users;
    use Trait\Entity\Remove\IntegrationLogs;
    use Trait\Action\LoginRegularUserAndGetTokenPair;

    private const METHOD = 'GET';

    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();

        $this->createRegularUser($this->entityManager, $this->hasher);
        $this->createVkIntegration($this->entityManager);
        $this->createIntegrationWithFakeClassPath($this->entityManager);

        $this->setUpRegularUser($this->entityManager);
        $this->setUpVkIntegration($this->entityManager);
        $this->setUpIntegrationWithFakeClassPath($this->entityManager);

        $this->createTrack($this->entityManager, $this->vkIntegration);
        $this->createTrackWithFakeDownloadUrl($this->entityManager, $this->vkIntegration);
        $this->createTrackWithIntegrationWhereFakeClassPath($this->entityManager, $this->integrationWithFakeClassPath);

        $this->setUpTrack($this->entityManager);
        $this->setUpTrackWithFakeDownloadUrl($this->entityManager);
        $this->setUpTrackWithIntegrationWhereFakeClassPath($this->entityManager);
    }

    protected function tearDown(): void
    {
        $this->removeIntegrationLogs($this->entityManager);
        $this->removeTracks($this->entityManager);
        $this->removeIntegrations($this->entityManager);
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testSuccess(): void
    {
        $downloadUrlHeaders = get_headers(Data\Track::DOWNLOAD_URL);
        $hasContentLength = false;
        if ($downloadUrlHeaders && is_array($downloadUrlHeaders)) {
            foreach ($downloadUrlHeaders as $header) {
                $explodedHeader = explode(': ', $header);
                $name = $explodedHeader[0] ?? null;
                $content = $explodedHeader[1] ?? null;

                if ($name == 'Content-Length' && intval($content) > 0) {
                    $hasContentLength = true;
                    break;
                }
            }
        }
        $this->assertTrue(
            $hasContentLength,
            'Битая ссылка на скачивание файла: ' . Data\Track::DOWNLOAD_URL
        );

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            sprintf(Data\V1\Url::TRACK_GET_STREAM, $this->track->getId()),
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $this->assertEquals(206, $this->kernelBrowser->getResponse()->getStatusCode());
        $this->assertFalse($this->kernelBrowser->getResponse()->isEmpty(), 'Нет контента');
    }

    public function testCantCreateSDK(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $this->kernelBrowser->request(
            self::METHOD,
            sprintf(Data\V1\Url::TRACK_GET_STREAM, $this->trackWithIntegrationWhereFakeClassPath->getId()),
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertEquals(500, $this->kernelBrowser->getResponse()->getStatusCode(), 'Не могу создать SDK');
    }

    public function testIntegrationFail(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $this->kernelBrowser->request(
            self::METHOD,
            sprintf(Data\V1\Url::TRACK_GET_STREAM, $this->trackWithFakeDownloadUrl->getId()),
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertEquals(503, $this->kernelBrowser->getResponse()->getStatusCode());
    }
}
