<?php

namespace App\Tests\Functional\Api\V1\Integration;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Data;
use TestsComponents\Trait;
use App\Api\V1\Application\Request\Integration as Request;
use App\Api\V1\Application\Response\Integration as Response;
use App\Api\V1\Domain\DTO;

class SetUserConfigListTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Dependency\SetUpVkIntegration;
    use Trait\Dependency\SetUpPromoDjIntegration;
    use Trait\Dependency\SetUpObjectsHelper;
    use Trait\Entity\Remove\Users;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Remove\Integrations;
    use Trait\Entity\Remove\UserIntegrationConfigs;
    use Trait\Entity\Create\RegularUser;
    use Trait\Entity\Create\VkIntegration;
    use Trait\Entity\Create\PromoDjIntegration;
    use Trait\Action\LoginRegularUserAndGetTokenPair;

    private const GET_METHOD = 'GET';
    private const SET_METHOD = 'PATCH';

    private const TEST_VK_CONFIG = ['test' => 'VK Config'];
    private const TEST_PROMO_DJ_CONFIG = ['test' => 'Promo DJ Config'];
    private const INVALID_INTEGRATION_ID = -1;

    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();
        $this->setUpObjectsHelper();

        $this->createRegularUser($this->entityManager, $this->hasher);
        $this->createVkIntegration($this->entityManager);
        $this->createPromoDjIntegration($this->entityManager);

        $this->setUpVkIntegration($this->entityManager);
        $this->setUpPromoDjIntegration($this->entityManager);
    }

    protected function tearDown(): void
    {
        $this->removeUserIntegrationConfigs($this->entityManager);
        $this->removeIntegrations($this->entityManager);
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testSuccess(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $vkDTO = new DTO\UserIntegrationConfig();
        $vkDTO->integrationId = $this->vkIntegration->getId();
        $vkDTO->userConfig = self::TEST_VK_CONFIG;

        $promoDjDTO = new DTO\UserIntegrationConfig();
        $promoDjDTO->integrationId = $this->promoDjIntegration->getId();
        $promoDjDTO->userConfig = self::TEST_PROMO_DJ_CONFIG;

        $request = new Request\SetUserConfigList();
        $request->userIntegrationConfigList[] = $vkDTO;
        $request->userIntegrationConfigList[] = $promoDjDTO;
        $requestArray = json_decode(json_encode($request), true);

        $this->kernelBrowser->request(
            self::SET_METHOD,
            Data\V1\Url::INTEGRATION_USER_CONFIG_LIST,
            $requestArray,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );
        $this->assertResponseIsSuccessful();
        $this->assertEquals(200, $this->kernelBrowser->getResponse()->getStatusCode());

        $this->kernelBrowser->request(
            self::GET_METHOD,
            Data\V1\Url::INTEGRATION_USER_CONFIG_LIST,
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );
        $this->assertResponseIsSuccessful();

        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\GetIntegrationConfigList $response */
        $response = $this->objectsHelper->fill(Response\GetIntegrationConfigList::class, $responseArray);
        foreach ($response->userIntegrationConfigList as $configDTO) {
            switch ($configDTO->integrationId) {
                case $this->vkIntegration->getId():
                    $this->assertEquals(
                        self::TEST_VK_CONFIG,
                        $configDTO->userConfig,
                        'Полученный конфиг не совпадает с сохраненным'
                    );
                    break;
                case $this->promoDjIntegration->getId():
                    $this->assertEquals(
                        self::TEST_PROMO_DJ_CONFIG,
                        $configDTO->userConfig,
                        'Полученный конфиг не совпадает с сохраненным'
                    );
                    break;
            }
        }
    }

    public function testInvalidRequest(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $request = new Request\SetUserConfigList();

        $this->kernelBrowser->request(
            self::SET_METHOD,
            Data\V1\Url::INTEGRATION_USER_CONFIG_LIST,
            (array) $request,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );
        $this->assertEquals(422, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testInvalidIntegrationId(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $integrationDTO = new DTO\UserIntegrationConfig();
        $integrationDTO->integrationId = self::INVALID_INTEGRATION_ID;
        $integrationDTO->userConfig = [];

        $request = new Request\SetUserConfigList();
        $request->userIntegrationConfigList[] = $integrationDTO;
        $requestArray = json_decode(json_encode($request), true);

        $this->kernelBrowser->request(
            self::SET_METHOD,
            Data\V1\Url::INTEGRATION_USER_CONFIG_LIST,
            $requestArray,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );
        $this->assertEquals(
            422,
            $this->kernelBrowser->getResponse()->getStatusCode(),
            'Нет валидации на отсутствие интеграции'
        );

        $this->kernelBrowser->request(
            self::GET_METHOD,
            Data\V1\Url::INTEGRATION_USER_CONFIG_LIST,
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );
        $this->assertResponseIsSuccessful();

        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\GetIntegrationConfigList $response */
        $response = $this->objectsHelper->fill(Response\GetIntegrationConfigList::class, $responseArray);
        foreach ($response->userIntegrationConfigList as $configDTO) {
            switch ($configDTO->integrationId) {
                case $this->vkIntegration->getId():
                    $this->assertEquals(
                        $this->vkIntegration->getConfig(),
                        $configDTO->userConfig,
                        'Полученный конфиг не совпадает со стартовым конфигом'
                    );
                    break;
                case $this->promoDjIntegration->getId():
                    $this->assertEquals(
                        $this->promoDjIntegration->getConfig(),
                        $configDTO->userConfig,
                        'Полученный конфиг не совпадает со стартовым конфигом'
                    );
                    break;
            }
        }
    }
}
