<?php

namespace App\Tests\Functional\Api\V1\Integration;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Data;
use TestsComponents\Trait;
use App\Api\V1\Application\Response\Integration as Response;
use App\Shared\Domain\Enum as SharedDomainEnum;

class GetLastUserScanListTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Dependency\SetUpObjectsHelper;
    use Trait\Dependency\SetUpVkIntegration;
    use Trait\Dependency\SetUpPromoDjIntegration;
    use Trait\Dependency\SetUpRegularUser;
    use Trait\Entity\Create\RegularUser;
    use Trait\Entity\Create\VkIntegration;
    use Trait\Entity\Create\PromoDjIntegration;
    use Trait\Entity\Create\ErrorOperationScan;
    use Trait\Entity\Create\SuccessOperationScan;
    use Trait\Entity\Remove\Integrations;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Remove\Users;
    use Trait\Entity\Remove\OperationScans;
    use Trait\Action\LoginRegularUserAndGetTokenPair;

    private const METHOD = 'GET';

    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();
        $this->setUpObjectsHelper();

        $this->createRegularUser($this->entityManager, $this->hasher);
        $this->createVkIntegration($this->entityManager);
        $this->createPromoDjIntegration($this->entityManager);

        $this->setUpRegularUser($this->entityManager);
        $this->setUpVkIntegration($this->entityManager);
        $this->setUpPromoDjIntegration($this->entityManager);

        $this->createSuccessOperationScan($this->entityManager, $this->vkIntegration, $this->regularUser);
        $this->createErrorOperationScan($this->entityManager, $this->vkIntegration, $this->regularUser);
        $this->createSuccessOperationScan($this->entityManager, $this->promoDjIntegration, $this->regularUser);
        $this->createErrorOperationScan($this->entityManager, $this->promoDjIntegration, $this->regularUser);
    }

    protected function tearDown(): void
    {
        $this->removeOperationScans($this->entityManager);
        $this->removeIntegrations($this->entityManager);
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testSuccess(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::INTEGRATION_LAST_USER_SCAN_LIST,
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $this->assertEquals(200, $this->kernelBrowser->getResponse()->getStatusCode());

        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\GetLastUserScanList $response */
        $response = $this->objectsHelper->fill(Response\GetLastUserScanList::class, $responseArray);

        $this->assertCount(2, $response->lastUserScanList, 'Вернулось не 2 сканирования');

        $this->assertEquals(
            SharedDomainEnum\OperationStatus::CompletedWithError->value,
            $response->lastUserScanList[0]->status,
            'Вернулась не последняя операция сканирования'
        );
        $this->assertEquals(
            SharedDomainEnum\OperationStatus::CompletedWithError->value,
            $response->lastUserScanList[1]->status,
            'Вернулась не последняя операция сканирования'
        );
    }
}
