<?php

namespace App\Tests\Functional\Api\V1\Integration;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Data;
use TestsComponents\Trait;
use App\Api\V1\Application\Response\Integration as Response;

class GetUserConfigListTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Dependency\SetUpObjectsHelper;
    use Trait\Entity\Remove\Users;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Create\RegularUser;
    use Trait\Action\LoginRegularUserAndGetTokenPair;
    use Trait\Entity\Create\VkIntegration;
    use Trait\Entity\Create\PromoDjIntegration;
    use Trait\Entity\Remove\Integrations;

    private const METHOD = 'GET';

    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();
        $this->setUpObjectsHelper();

        $this->createRegularUser($this->entityManager, $this->hasher);
        $this->createVkIntegration($this->entityManager);
        $this->createPromoDjIntegration($this->entityManager);
    }

    protected function tearDown(): void
    {
        $this->removeIntegrations($this->entityManager);
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testSuccess(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::INTEGRATION_USER_CONFIG_LIST,
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $this->assertEquals(200, $this->kernelBrowser->getResponse()->getStatusCode());

        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\GetIntegrationConfigList $response */
        $response = $this->objectsHelper->fill(Response\GetIntegrationConfigList::class, $responseArray);
        $this->assertCount(2, $response->userIntegrationConfigList, 'Вернулось не 2 интеграции');
    }
}
