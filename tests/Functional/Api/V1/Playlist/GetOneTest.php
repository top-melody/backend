<?php

namespace App\Tests\Functional\Api\V1\Playlist;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Data;
use TestsComponents\Trait;
use App\Api\V1\Application\Request\Playlist as Request;
use App\Api\V1\Application\Response\Playlist as Response;
use App\Shared\Domain\Enum as SharedDomainEnum;

class GetOneTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Dependency\SetUpObjectsHelper;
    use Trait\Dependency\SetUpVkIntegration;
    use Trait\Dependency\SetUpRegularUser;
    use Trait\Entity\Create\RegularUser;
    use Trait\Entity\Create\VkIntegration;
    use Trait\Entity\Remove\Integrations;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Remove\Users;
    use Trait\Action\LoginRegularUserAndGetTokenPair;
    use Trait\Entity\Create\Author;
    use Trait\Entity\Create\Track;
    use Trait\Entity\Create\AnotherTrack;
    use Trait\Dependency\SetUpAuthor;
    use Trait\Dependency\SetUpTrack;
    use Trait\Dependency\SetUpAnotherTrack;
    use Trait\Entity\Remove\Tracks;
    use Trait\Entity\Create\MyPlaylist;
    use Trait\Dependency\SetUpMyPlaylist;
    use Trait\Entity\Remove\Playlists;
    use Trait\Entity\Create\TrackToPlaylist;
    use Trait\Entity\Remove\TrackToPlaylist;
    use Trait\Entity\Remove\UserToPlaylists;
    use Trait\Entity\Remove\TrackToAuthors;
    use Trait\Entity\Remove\Authors;

    private const METHOD = 'POST';
    private const PAGE = 1;
    private const PER_PAGE = 2;
    private const RANDOM_SEARCH_STRING = 'dfkjkgjdovdwpmvkwvp';
    private const RANDOM_PLAYLIST_ENUM_VALUE = 'ioigujuiopokpvokednvp';
    private const RANDOM_PLAYLIST_ID = -1;

    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();
        $this->setUpObjectsHelper();

        $this->createRegularUser($this->entityManager, $this->hasher);
        $this->setUpRegularUser($this->entityManager);

        $this->createVkIntegration($this->entityManager);
        $this->setUpVkIntegration($this->entityManager);

        $this->createAuthor($this->entityManager);
        $this->setUpAuthor($this->entityManager);

        $this->createTrack($this->entityManager, $this->vkIntegration, $this->author);
        $this->setUpTrack($this->entityManager);

        $this->createMyPlaylist($this->entityManager, $this->regularUser);
        $this->setUpMyPlaylist($this->entityManager, $this->regularUser);

        $this->createTrackToPlaylist($this->entityManager, $this->track, $this->myPlaylist);
    }

    protected function tearDown(): void
    {
        $this->removeTrackToAuthors($this->entityManager);
        $this->removeAuthors($this->entityManager);
        $this->removeUserToPlaylists($this->entityManager);
        $this->removeTrackToPlaylists($this->entityManager);
        $this->removePlaylists($this->entityManager);
        $this->removeTracks($this->entityManager);
        $this->removeIntegrations($this->entityManager);
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testSuccessMy(): void
    {
        $request = new Request\GetOne();
        $request->id = SharedDomainEnum\Playlist::My->value;
        $request->page = self::PAGE;
        $request->query = null;
        $request->shuffle = false;
        $request->perPage = self::PER_PAGE;

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::PLAYLIST_GET_ONE,
            (array) $request,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\GetOne $response */
        $response = $this->objectsHelper->fill(Response\GetOne::class, $responseArray);

        $this->assertCount(1, $response->trackList);

        foreach ($response->trackList as $trackDTO) {
            $this->assertTrue($trackDTO->isOnMyPlaylist, 'Трек не в моём плейлисте');
        }
    }

    public function testSuccessNew(): void
    {
        $this->createAnotherTrack($this->entityManager, $this->vkIntegration);

        $request = new Request\GetOne();
        $request->id = SharedDomainEnum\Playlist::New->value;
        $request->page = self::PAGE;
        $request->query = null;
        $request->shuffle = false;
        $request->perPage = self::PER_PAGE;

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::PLAYLIST_GET_ONE,
            (array) $request,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\GetOne $response */
        $response = $this->objectsHelper->fill(Response\GetOne::class, $responseArray);
        $this->assertCount(2, $response->trackList);

        $countOfTracksInMyPlaylist = 0;
        foreach ($response->trackList as $trackDTO) {
            if ($trackDTO->isOnMyPlaylist) {
                $countOfTracksInMyPlaylist++;
            }
        }
        $this->assertEquals(1, $countOfTracksInMyPlaylist, 'Неверное количество треков в моём плейлисте');
    }

    public function testFailSearch(): void
    {
        $request = new Request\GetOne();
        $request->id = SharedDomainEnum\Playlist::New->value;
        $request->page = self::PAGE;
        $request->query = self::RANDOM_SEARCH_STRING;
        $request->shuffle = false;
        $request->perPage = self::PER_PAGE;

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::PLAYLIST_GET_ONE,
            (array) $request,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\GetOne $response */
        $response = $this->objectsHelper->fill(Response\GetOne::class, $responseArray);
        $this->assertCount(0, $response->trackList, 'Количество найденных треков неверно');
    }

    public function testEnumValueNotValid(): void
    {
        $request = new Request\GetOne();
        $request->id = self::RANDOM_PLAYLIST_ENUM_VALUE;
        $request->page = self::PAGE;
        $request->query = null;
        $request->shuffle = false;
        $request->perPage = self::PER_PAGE;

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::PLAYLIST_GET_ONE,
            (array) $request,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertEquals(422, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testIdNotValid(): void
    {
        $request = new Request\GetOne();
        $request->id = self::RANDOM_PLAYLIST_ID;
        $request->page = self::PAGE;
        $request->query = null;
        $request->shuffle = false;
        $request->perPage = self::PER_PAGE;

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::PLAYLIST_GET_ONE,
            (array) $request,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertEquals(422, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testSearchByAuthorName(): void
    {
        $request = new Request\GetOne();
        $request->id = SharedDomainEnum\Playlist::New->value;
        $request->page = self::PAGE;
        $request->query = Data\Author::NAME;
        $request->shuffle = false;
        $request->perPage = self::PER_PAGE;

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::PLAYLIST_GET_ONE,
            (array) $request,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\GetOne $response */
        $response = $this->objectsHelper->fill(Response\GetOne::class, $responseArray);
        $this->assertCount(1, $response->trackList, 'Количество найденных треков неверно');
        $this->assertEquals(Data\Author::NAME, current(current($response->trackList)->authorNames), 'Неправильно работает поиск по автору');
    }

    public function testFindByTrackName(): void
    {
        $request = new Request\GetOne();
        $request->id = SharedDomainEnum\Playlist::New->value;
        $request->page = self::PAGE;
        $request->query = Data\Track::NAME;
        $request->shuffle = false;
        $request->perPage = self::PER_PAGE;

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::PLAYLIST_GET_ONE,
            (array) $request,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\GetOne $response */
        $response = $this->objectsHelper->fill(Response\GetOne::class, $responseArray);
        $this->assertCount(1, $response->trackList, 'Количество найденных треков неверно');
        $this->assertEquals(Data\Track::NAME, current($response->trackList)->name, 'Неправильно работает поиск по треку');
    }

    public function testShuffle(): void
    {
        $this->createAnotherTrack($this->entityManager, $this->vkIntegration);

        $request = new Request\GetOne();
        $request->id = SharedDomainEnum\Playlist::New->value;
        $request->page = self::PAGE;
        $request->query = null;
        $request->shuffle = true;
        $request->perPage = self::PER_PAGE;

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $firstTrackName = null;
        for ($i = 0; $i < 10; $i++) {
            $this->kernelBrowser->request(
                self::METHOD,
                Data\V1\Url::PLAYLIST_GET_ONE,
                (array) $request,
                [],
                ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
            );
            $this->assertResponseIsSuccessful();
            $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
            /** @var Response\GetOne $response */
            $response = $this->objectsHelper->fill(Response\GetOne::class, $responseArray);

            if ($firstTrackName && current($response->trackList)->name != $firstTrackName) {
                $this->assertTrue(true);
                return;
            }

            $firstTrackName = current($response->trackList)->name;
        }

        $this->fail('Рандомизация плейлиста не работает');
    }

    public function testPagination(): void
    {
        $this->createAnotherTrack($this->entityManager, $this->vkIntegration);

        $page = 2;
        $perPage = 1;

        $request = new Request\GetOne();
        $request->id = SharedDomainEnum\Playlist::New->value;
        $request->page = $page;
        $request->query = null;
        $request->shuffle = false;
        $request->perPage = $perPage;

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::PLAYLIST_GET_ONE,
            (array) $request,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\GetOne $response */
        $response = $this->objectsHelper->fill(Response\GetOne::class, $responseArray);

        $this->assertCount(1, $response->trackList, 'Количество найденных треков неверно');
        $this->assertEquals($page, $response->pagination->currentPage, 'Текущая страница не верна');
        $this->assertEquals($perPage, $response->pagination->perPage, 'Количество треков на странице неверно');
    }

    public function testOrderOnMyPlaylist(): void
    {
        sleep(1); // Нужен, чтобы время добавления в плейлист у треков было разным
        $this->createAnotherTrack($this->entityManager, $this->vkIntegration);
        $this->setUpAnotherTrack($this->entityManager);
        $this->createTrackToPlaylist($this->entityManager, $this->anotherTrack, $this->myPlaylist);

        $request = new Request\GetOne();
        $request->id = SharedDomainEnum\Playlist::My->value;
        $request->page = self::PAGE;
        $request->query = null;
        $request->shuffle = false;
        $request->perPage = self::PER_PAGE;

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::PLAYLIST_GET_ONE,
            (array) $request,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\GetOne $response */
        $response = $this->objectsHelper->fill(Response\GetOne::class, $responseArray);

        $this->assertCount(2, $response->trackList, 'Количество треков неверно');
        $this->assertEquals(Data\AnotherTrack::NAME, current($response->trackList)->name, 'Неправильно работает сортировка по моему плейлисту');
    }

    public function testOrderOnNewPlaylist(): void
    {
        sleep(1); // Нужен, чтобы время создания у треков было разным
        $this->createAnotherTrack($this->entityManager, $this->vkIntegration);

        $request = new Request\GetOne();
        $request->id = SharedDomainEnum\Playlist::New->value;
        $request->page = self::PAGE;
        $request->query = null;
        $request->shuffle = false;
        $request->perPage = self::PER_PAGE;

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::PLAYLIST_GET_ONE,
            (array) $request,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\GetOne $response */
        $response = $this->objectsHelper->fill(Response\GetOne::class, $responseArray);

        $this->assertCount(2, $response->trackList, 'Количество треков неверно');
        $this->assertEquals(Data\AnotherTrack::NAME, current($response->trackList)->name, 'Неправильно работает сортировка по плейлисту новых треков');
    }
}
