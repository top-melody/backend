<?php

namespace App\Tests\Functional\Api\V1\Playlist;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Data;
use TestsComponents\Trait;
use App\Api\V1\Application\Response\Playlist as Response;

class GetListTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Dependency\SetUpObjectsHelper;
    use Trait\Dependency\SetUpRegularUser;
    use Trait\Entity\Create\RegularUser;
    use Trait\Entity\Create\RegularPlaylist;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Remove\Users;
    use Trait\Action\LoginRegularUserAndGetTokenPair;
    use Trait\Entity\Create\MyPlaylist;
    use Trait\Dependency\SetUpMyPlaylist;
    use Trait\Entity\Remove\Playlists;
    use Trait\Entity\Remove\UserToPlaylists;

    private const METHOD = 'GET';

    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();
        $this->setUpObjectsHelper();

        $this->createRegularUser($this->entityManager, $this->hasher);
        $this->setUpRegularUser($this->entityManager);
        $this->createRegularPlaylist($this->entityManager, $this->regularUser);
    }

    protected function tearDown(): void
    {
        $this->removeUserToPlaylists($this->entityManager);
        $this->removePlaylists($this->entityManager);
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testWhenMyPlaylistExists(): void
    {
        $this->createMyPlaylist($this->entityManager, $this->regularUser);
        $this->setUpMyPlaylist($this->entityManager, $this->regularUser);

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::PLAYLIST_GET_LIST,
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\GetList $response */
        $response = $this->objectsHelper->fill(Response\GetList::class, $responseArray);

        $this->assertCount(3, $response->items, 'Количество плейлистов неверно');
    }

    public function testWhenMyPlaylistNotExists(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::PLAYLIST_GET_LIST,
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\GetList $response */
        $response = $this->objectsHelper->fill(Response\GetList::class, $responseArray);

        $this->assertCount(3, $response->items, 'Количество плейлистов неверно');
    }
}
