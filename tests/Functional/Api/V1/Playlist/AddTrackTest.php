<?php

namespace App\Tests\Functional\Api\V1\Playlist;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Data;
use TestsComponents\Trait;
use App\Api\V1\Application\Response\Playlist as Response;
use App\Shared\Domain\Enum as SharedDomainEnum;

class AddTrackTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Dependency\SetUpObjectsHelper;
    use Trait\Dependency\SetUpVkIntegration;
    use Trait\Dependency\SetUpRegularUser;
    use Trait\Entity\Create\RegularUser;
    use Trait\Entity\Create\VkIntegration;
    use Trait\Entity\Remove\Integrations;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Remove\Users;
    use Trait\Action\LoginRegularUserAndGetTokenPair;
    use Trait\Entity\Create\Track;
    use Trait\Dependency\SetUpTrack;
    use Trait\Entity\Remove\Tracks;
    use Trait\Entity\Create\MyPlaylist;
    use Trait\Entity\Create\RegularPlaylist;
    use Trait\Dependency\SetUpMyPlaylist;
    use Trait\Dependency\SetUpRegularPlaylist;
    use Trait\Entity\Remove\Playlists;
    use Trait\Entity\Create\TrackToPlaylist;
    use Trait\Entity\Remove\TrackToPlaylist;
    use Trait\Entity\Remove\UserToPlaylists;

    private const METHOD = 'PUT';
    private const INVALID_PLAYLIST_ENUM_VALUE = 'dkjkgkgjd';
    private const INVALID_PLAYLIST_ID = -1;
    private const INVALID_TRACK_ID = -1;

    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();
        $this->setUpObjectsHelper();

        $this->createRegularUser($this->entityManager, $this->hasher);
        $this->setUpRegularUser($this->entityManager);

        $this->createVkIntegration($this->entityManager);
        $this->setUpVkIntegration($this->entityManager);

        $this->createTrack($this->entityManager, $this->vkIntegration);
        $this->setUpTrack($this->entityManager);
    }

    protected function tearDown(): void
    {
        $this->removeUserToPlaylists($this->entityManager);
        $this->removeTrackToPlaylists($this->entityManager);
        $this->removePlaylists($this->entityManager);
        $this->removeTracks($this->entityManager);
        $this->removeIntegrations($this->entityManager);
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testAddToMyPlaylist(): void
    {
        $this->createMyPlaylist($this->entityManager, $this->regularUser);

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            sprintf(Data\V1\Url::PLAYLIST_ADD_TRACK, SharedDomainEnum\Playlist::My->value, $this->track->getId()),
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\AddTrack $response */
        $response = $this->objectsHelper->fill(Response\AddTrack::class, $responseArray);

        $this->assertTrue($response->isSuccess, 'Не могу добавить трек в мой плейлист');
    }

    public function testAddToMyPlaylistWhenItIsNotInited(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            sprintf(Data\V1\Url::PLAYLIST_ADD_TRACK, SharedDomainEnum\Playlist::My->value, $this->track->getId()),
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\AddTrack $response */
        $response = $this->objectsHelper->fill(Response\AddTrack::class, $responseArray);

        $this->assertTrue($response->isSuccess, 'Не могу добавить трек в мой плейлист без его инициализации');
    }

    public function testAddToMyPlaylistViaId(): void
    {
        $this->createMyPlaylist($this->entityManager, $this->regularUser);
        $this->setUpMyPlaylist($this->entityManager, $this->regularUser);

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            sprintf(Data\V1\Url::PLAYLIST_ADD_TRACK, $this->myPlaylist->getId(), $this->track->getId()),
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\AddTrack $response */
        $response = $this->objectsHelper->fill(Response\AddTrack::class, $responseArray);

        $this->assertTrue($response->isSuccess, 'Не могу добавить трек в мой плейлист по id');
    }

    public function testAddToPlaylist(): void
    {
        $this->createRegularPlaylist($this->entityManager, $this->regularUser);
        $this->setUpRegularPlaylist($this->entityManager);

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            sprintf(Data\V1\Url::PLAYLIST_ADD_TRACK, $this->regularPlaylist->getId(), $this->track->getId()),
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\AddTrack $response */
        $response = $this->objectsHelper->fill(Response\AddTrack::class, $responseArray);

        $this->assertTrue($response->isSuccess, 'Не могу добавить трек в плейлист');
    }

    public function testAddToNewPlaylist(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            sprintf(Data\V1\Url::PLAYLIST_ADD_TRACK, SharedDomainEnum\Playlist::New->value, $this->track->getId()),
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertEquals(422, $this->kernelBrowser->getResponse()->getStatusCode());
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\AddTrack $response */
        $response = $this->objectsHelper->fill(Response\AddTrack::class, $responseArray);

        $this->assertFalse($response->isSuccess, 'Могу добавлять треки в плейлист "Новинки"');
    }

    public function testInvalidPlaylistEnum(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            sprintf(Data\V1\Url::PLAYLIST_ADD_TRACK, self::INVALID_PLAYLIST_ENUM_VALUE, $this->track->getId()),
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertEquals(422, $this->kernelBrowser->getResponse()->getStatusCode());
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\AddTrack $response */
        $response = $this->objectsHelper->fill(Response\AddTrack::class, $responseArray);

        $this->assertFalse($response->isSuccess, 'Нет валидации на enum плейлиста');
    }

    public function testPlaylistNotFoundById(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            sprintf(Data\V1\Url::PLAYLIST_ADD_TRACK, self::INVALID_PLAYLIST_ID, $this->track->getId()),
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertEquals(422, $this->kernelBrowser->getResponse()->getStatusCode());
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\AddTrack $response */
        $response = $this->objectsHelper->fill(Response\AddTrack::class, $responseArray);

        $this->assertFalse($response->isSuccess, 'Нет валидации на id плейлиста');
    }

    public function testUserHaveNoPlaylist(): void
    {
        $this->createRegularPlaylist($this->entityManager);
        $this->setUpRegularPlaylist($this->entityManager);

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            sprintf(Data\V1\Url::PLAYLIST_ADD_TRACK, $this->regularPlaylist->getId(), $this->track->getId()),
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertEquals(403, $this->kernelBrowser->getResponse()->getStatusCode());
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\AddTrack $response */
        $response = $this->objectsHelper->fill(Response\AddTrack::class, $responseArray);

        $this->assertFalse($response->isSuccess, 'Нет валидации на принадлежность плейлиста юзеру');
    }

    public function testTrackNotFound(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            sprintf(Data\V1\Url::PLAYLIST_ADD_TRACK, SharedDomainEnum\Playlist::My->value, self::INVALID_TRACK_ID),
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertEquals(404, $this->kernelBrowser->getResponse()->getStatusCode());
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\AddTrack $response */
        $response = $this->objectsHelper->fill(Response\AddTrack::class, $responseArray);

        $this->assertFalse($response->isSuccess, 'Нет валидации на корректность id трека');
    }

    public function testTrackAlreadyInPlaylist(): void
    {
        $this->createMyPlaylist($this->entityManager, $this->regularUser);
        $this->setUpMyPlaylist($this->entityManager, $this->regularUser);
        $this->createTrackToPlaylist($this->entityManager, $this->track, $this->myPlaylist);

        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);
        $this->kernelBrowser->request(
            self::METHOD,
            sprintf(Data\V1\Url::PLAYLIST_ADD_TRACK, SharedDomainEnum\Playlist::My->value, $this->track->getId()),
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertEquals(422, $this->kernelBrowser->getResponse()->getStatusCode());
        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\AddTrack $response */
        $response = $this->objectsHelper->fill(Response\AddTrack::class, $responseArray);

        $this->assertFalse($response->isSuccess, 'Нет валидации на присутствие трека в плейлисте');
    }
}
