<?php

namespace App\Tests\Functional\Api\V1\Dictionary;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Data;
use TestsComponents\Trait;
use App\Api\V1\Application\Response\Dictionary as Response;

class IntegrationsTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Dependency\SetUpObjectsHelper;
    use Trait\Entity\Remove\Users;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Create\RegularUser;
    use Trait\Action\LoginRegularUserAndGetTokenPair;
    use Trait\Entity\Create\VkIntegration;
    use Trait\Entity\Remove\Integrations;

    private const METHOD = 'GET';

    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();
        $this->setUpObjectsHelper();

        $this->createRegularUser($this->entityManager, $this->hasher);
        $this->createVkIntegration($this->entityManager);
    }

    protected function tearDown(): void
    {
        $this->removeIntegrations($this->entityManager);
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testSuccess(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::DICTIONARY_INTEGRATIONS,
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $this->assertEquals(200, $this->kernelBrowser->getResponse()->getStatusCode());

        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\Items $response */
        $response = $this->objectsHelper->fill(Response\Items::class, $responseArray);
        $this->assertEquals(Data\VkIntegration::NAME, $response->items[0]->name);
    }
}
