<?php

namespace App\Tests\Functional\Api\V1\User;

use App\Api\V1\Application\Request\User as Request;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Data;
use TestsComponents\Trait;

class LoginTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Entity\Remove\Users;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Create\RegularUser;

    private const METHOD = 'POST';
    private const INVALID_PASSWORD = 'not_valid_password';
    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();

        $this->createRegularUser($this->entityManager, $this->hasher);
    }

    protected function tearDown(): void
    {
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testSuccess(): void
    {
        $request = new Request\Login();
        $request->email = Data\RegularUser::EMAIL;
        $request->password = Data\RegularUser::PASSWORD;

        $this->kernelBrowser->request(self::METHOD, Data\V1\Url::USER_LOGIN, (array) $request);

        $this->assertResponseIsSuccessful();
        $this->assertEquals(200, $this->kernelBrowser->getResponse()->getStatusCode());

        $content = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        $this->assertArrayHasKey('token', $content);
        $this->assertArrayHasKey('refreshToken', $content);
    }

    public function testIncorrectLoginData(): void
    {
        $request = new Request\Login();
        $request->email = Data\RegularUser::EMAIL;
        $request->password = self::INVALID_PASSWORD;

        $this->kernelBrowser->request(self::METHOD, Data\V1\Url::USER_LOGIN, (array) $request);

        $this->assertEquals(403, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testInvalidRequest(): void
    {
        $request = new Request\Login();
        $request->email = Data\RegularUser::EMAIL;
        $request->password = null;

        $this->kernelBrowser->request(self::METHOD, Data\V1\Url::USER_LOGIN, (array) $request);

        $this->assertEquals(422, $this->kernelBrowser->getResponse()->getStatusCode());
    }
}
