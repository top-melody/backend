<?php

namespace App\Tests\Functional\Api\V1\User;

use App\Api\V1\Application\Request\User as Request;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Trait;
use TestsComponents\Data;

class PasswordRecoveryTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Entity\Remove\Users;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Create\RegularUser;

    private const METHOD = 'post';
    private const FAKE_USER_EMAIL = 'fake@mail.ru';

    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();

        $this->createRegularUser($this->entityManager, $this->hasher);
    }

    protected function tearDown(): void
    {
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testSuccess(): void
    {
        $request = new Request\PasswordRecovery();
        $request->email = Data\RegularUser::EMAIL;

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_PASSWORD_RECOVERY,
            (array) $request,
        );

        $this->assertResponseIsSuccessful();
        $this->assertEquals(200, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testUserNotFound(): void
    {
        $request = new Request\PasswordRecovery();
        $request->email = self::FAKE_USER_EMAIL;

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_PASSWORD_RECOVERY,
            (array) $request,
        );

        $this->assertEquals(403, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testInvalidRequest(): void
    {
        $request = new Request\PasswordRecovery();

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_PASSWORD_RECOVERY,
            (array) $request,
        );

        $this->assertEquals(422, $this->kernelBrowser->getResponse()->getStatusCode());
    }
}
