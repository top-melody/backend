<?php

namespace App\Tests\Functional\Api\V1\User;

use App\Api\V1\Application\Request\User as Request;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Data;
use TestsComponents\Trait;

class RegisterTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Entity\Remove\Users;

    private const METHOD = 'PUT';

    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
    }

    protected function tearDown(): void
    {
        $this->removeUsers($this->entityManager);
    }

    public function testSuccess(): void
    {
        $request = new Request\Register();
        $request->email = Data\RegularUser::EMAIL;
        $request->password = Data\RegularUser::PASSWORD;

        $this->kernelBrowser->request(self::METHOD, Data\V1\Url::USER_REGISTER, (array) $request);
        $this->assertResponseIsSuccessful();
        $this->assertEquals(201, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testAlreadyExistUser(): void
    {
        $request = new Request\Register();
        $request->email = Data\RegularUser::EMAIL;
        $request->password = Data\RegularUser::PASSWORD;

        $this->kernelBrowser->request(self::METHOD, Data\V1\Url::USER_REGISTER, (array) $request);
        $this->assertResponseIsSuccessful();

        $this->kernelBrowser->request(self::METHOD, Data\V1\Url::USER_REGISTER, (array) $request);
        $this->assertEquals(409, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testInvalidRequest(): void
    {
        $request = new Request\Register();
        $request->email = null;
        $request->password = 'regular';

        $this->kernelBrowser->request(self::METHOD, Data\V1\Url::USER_REGISTER, (array) $request);
        $this->assertEquals(422, $this->kernelBrowser->getResponse()->getStatusCode());
    }
}
