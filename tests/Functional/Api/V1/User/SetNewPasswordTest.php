<?php

namespace App\Tests\Functional\Api\V1\User;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Data;
use TestsComponents\Trait;
use App\Api\V1\Application\Request\User as Request;

class SetNewPasswordTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Entity\Remove\Users;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Create\RegularUser;
    use Trait\Action\LoginRegularUserAndGetTokenPair;

    private const METHOD = 'PATCH';
    private const LOGIN_METHOD = 'POST';
    private const NEW_VALID_PASSWORD = 'q1w2e3r4t5y6';
    private const NEW_INVALID_PASSWORD = '1';

    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();

        $this->createRegularUser($this->entityManager, $this->hasher);
    }

    protected function tearDown(): void
    {
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testSuccess(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $request = new Request\SetNewPassword();
        $request->password = self::NEW_VALID_PASSWORD;

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_SET_NEW_PASSWORD,
            (array) $request,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $this->assertEquals(200, $this->kernelBrowser->getResponse()->getStatusCode());

        $loginRequest = new Request\Login();
        $loginRequest->email = Data\RegularUser::EMAIL;
        $loginRequest->password = self::NEW_VALID_PASSWORD;

        $this->kernelBrowser->request(
            self::LOGIN_METHOD,
            Data\V1\Url::USER_LOGIN,
            (array) $loginRequest,
        );

        $this->assertResponseIsSuccessful();
        $this->assertEquals(200, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testInvalidNewPassword(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $request = new Request\SetNewPassword();
        $request->password = self::NEW_INVALID_PASSWORD;

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_SET_NEW_PASSWORD,
            (array) $request,
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertEquals(422, $this->kernelBrowser->getResponse()->getStatusCode());
    }
}
