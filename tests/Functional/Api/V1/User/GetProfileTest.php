<?php

namespace App\Tests\Functional\Api\V1\User;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Data;
use TestsComponents\Trait;
use App\Api\V1\Application\Response\User as Response;

class GetProfileTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Dependency\SetUpObjectsHelper;
    use Trait\Dependency\SetUpVkIntegration;
    use Trait\Dependency\SetUpRegularUser;
    use Trait\Entity\Create\RegularUser;
    use Trait\Entity\Create\VkIntegration;
    use Trait\Entity\Create\SuccessOperationScan;
    use Trait\Entity\Create\UserIntegrationConfig;
    use Trait\Entity\Remove\Integrations;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Remove\Users;
    use Trait\Entity\Remove\OperationScans;
    use Trait\Entity\Remove\UserIntegrationConfigs;
    use Trait\Action\LoginRegularUserAndGetTokenPair;

    private const METHOD = 'GET';

    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();
        $this->setUpObjectsHelper();

        $this->createRegularUser($this->entityManager, $this->hasher);
        $this->createVkIntegration($this->entityManager);

        $this->setUpRegularUser($this->entityManager);
        $this->setUpVkIntegration($this->entityManager);

        $this->createSuccessOperationScan($this->entityManager, $this->vkIntegration, $this->regularUser);
        $this->createUserIntegrationConfig($this->entityManager, $this->vkIntegration, $this->regularUser);
    }

    protected function tearDown(): void
    {
        $this->removeUserIntegrationConfigs($this->entityManager);
        $this->removeOperationScans($this->entityManager);
        $this->removeIntegrations($this->entityManager);
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testSuccess(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_GET_PROFILE,
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $this->assertEquals(200, $this->kernelBrowser->getResponse()->getStatusCode());

        $responseArray = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        /** @var Response\Profile $response */
        $response = $this->objectsHelper->fill(Response\Profile::class, $responseArray);

        $this->assertCount(1, $response->integrations, 'Вернулось не 1 интеграция');
        $this->assertTrue(
            $response->integrations[0]->lastUserScan !== null,
            'Отсутствует информация о последнем сканировании'
        );
        $this->assertTrue(
            $response->integrations[0]->userConfig !== null,
            'Отсутствует конфиг интеграции пользователя',
        );
    }
}
