<?php

namespace App\Tests\Functional\Api\V1\User;

use App\Api\V1\Application\Request\User as Request;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Data;
use TestsComponents\Trait;

class RefreshTokenPairTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Entity\Remove\Users;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Create\RegularUser;
    use Trait\Action\LoginRegularUserAndGetTokenPair;

    private const METHOD = 'POST';
    private const INVALID_REFRESH_TOKEN = '12345';
    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();

        $this->createRegularUser($this->entityManager, $this->hasher);
    }

    protected function tearDown(): void
    {
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testSuccess(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $request = new Request\RefreshTokenPair();
        $request->refreshToken = $tokenPair->refreshToken;

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_REFRESH_TOKEN_PAIR,
            (array) $request,
        );

        $this->assertResponseIsSuccessful();
        $this->assertEquals(200, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testUserUnauthorized(): void
    {
        $request = new Request\RefreshTokenPair();
        $request->refreshToken = self::INVALID_REFRESH_TOKEN;

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_REFRESH_TOKEN_PAIR,
            (array) $request,
        );

        $this->assertEquals(401, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testRefreshTokenExpired(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $this->entityManager->createQuery("
            update App\Shared\Domain\Entity\RefreshToken rt
            set rt.valid = '2023-01-05 21:48:58'
            where rt.refreshToken = '{$tokenPair->refreshToken}'
        ")->execute();

        $request = new Request\RefreshTokenPair();
        $request->refreshToken = $tokenPair->refreshToken;

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_REFRESH_TOKEN_PAIR,
            (array) $request,
        );

        $this->assertEquals(406, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testInvalidRequest(): void
    {
        $request = new Request\RefreshTokenPair();

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_REFRESH_TOKEN_PAIR,
            (array) $request,
        );

        $this->assertEquals(422, $this->kernelBrowser->getResponse()->getStatusCode());
    }
}
