<?php

namespace App\Tests\Functional\Api\V1\User;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TestsComponents\Data;
use TestsComponents\Trait;

class LogoutTest extends WebTestCase
{
    use Trait\Dependency\SetUpKernelBrowser;
    use Trait\Dependency\SetUpEntityManager;
    use Trait\Dependency\SetUpHasher;
    use Trait\Entity\Remove\Users;
    use Trait\Entity\Remove\RefreshTokens;
    use Trait\Entity\Create\RegularUser;
    use Trait\Action\LoginRegularUserAndGetTokenPair;

    private const METHOD = 'POST';
    private const EXPIRED_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2NzMzNDE3MzAsImV4cCI6MTY3MzM0NTMzMCwicm9sZXMiOltdLCJ1c2VybmFtZSI6InJlZ3VsYXJAbWFpbC5ydSIsImlkIjoxNTV9.BBqkruQ15AvtVKxUSkW40Y6vq_musRkoKD7znfPZYIyO54pJB1QYeJgeUqquzfhxh1_ePy27fFZiBpxzcZCIPHStXSKLXelIrwpp9JuJq1xfww58q5qQLfvVtxJw98LtW7SYp4RA4SKZKXnS400QNX9fpLk2nWdHT63knwukT32iveunWNc3cDbc5aTpiiKDCxshUbZgWgIZ9c2pxrioD3uLmr90yc9ZGsPDoOLgGBq6fvk9-DLl9XdRkcTidEv9qeLiOQ7r6t3UlvHaxzYhWIZtv7rx3Gg9Rs09EcsHMpxTSgm0ZoKmVtXaMsV6opo2vTMvw6EdFrJjUUvK9v5rNA';
    private const INVALID_TOKEN = '12345' . self::EXPIRED_TOKEN;

    protected function setUp(): void
    {
        $this->setUpKernelBrowser();
        $this->setUpEntityManager();
        $this->setUpHasher();

        $this->createRegularUser($this->entityManager, $this->hasher);
    }

    protected function tearDown(): void
    {
        $this->removeRefreshTokens($this->entityManager);
        $this->removeUsers($this->entityManager);
    }

    public function testSuccess(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_LOGOUT,
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );

        $this->assertResponseIsSuccessful();
        $this->assertEquals(200, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testInvalidToken(): void
    {
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_LOGOUT,
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer " . self::INVALID_TOKEN],
        );

        $this->assertEquals(403, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testWithoutToken(): void
    {
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_LOGOUT,
        );

        $this->assertEquals(404, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testExpiredToken(): void
    {
        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_LOGOUT,
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer " . self::EXPIRED_TOKEN],
        );

        $this->assertEquals(406, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testDoubleLogout(): void
    {
        $tokenPair = $this->loginRegularUserAndGetTokenPair($this->kernelBrowser);

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_LOGOUT,
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );
        $this->assertResponseIsSuccessful();

        $this->kernelBrowser->request(
            self::METHOD,
            Data\V1\Url::USER_LOGOUT,
            [],
            [],
            ['HTTP_AUTHORIZATION' => "Bearer {$tokenPair->token}"],
        );
        $this->assertEquals(422, $this->kernelBrowser->getResponse()->getStatusCode());
    }
}
