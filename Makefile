default: start

COMPOSE_FILE = docker-compose.yml

start:
	docker-compose -f $(COMPOSE_FILE) up -d
stop:
	docker-compose -f $(COMPOSE_FILE) down
restart:
	make stop
	make start
build:
	docker-compose -f $(COMPOSE_FILE) build
composer-install:
	docker exec -it tm-php composer install
composer-update:
	docker exec -it tm-php composer update
composer-update-core:
	docker exec -it tm-php composer update top-melody/backend-integration-core
composer-update-vk:
	docker exec -it tm-php composer update top-melody/backend-vk-integration
cache-clear:
	docker exec -it tm-php php bin/console cache:clear
migration:
	docker exec -it tm-php php bin/console doctrine:migrations:generate
migrate:
	docker exec -it tm-php php bin/console doctrine:migrations:migrate -n
migrate-down:
	docker exec -it tm-php php bin/console doctrine:migrations:migrate prev -n
migrate-diff:
	docker exec -it tm-php php bin/console doctrine:migrations:diff -n
code-fix:
	docker run --rm -v `pwd`/src:/data/src -v `pwd`/tests:/data/tests -v `pwd`/tests-components:/data/tests-components cytopia/php-cs-fixer fix .
test-init:
	docker exec -it tm-postgres psql -U top-melody -c 'CREATE DATABASE "top-melody_test"'
	docker exec -it tm-postgres psql -U top-melody -c 'GRANT ALL PRIVILEGES ON DATABASE "top-melody_test" TO "top-melody"'
test-all:
	docker exec -it tm-php symfony console doctrine:migrations:migrate -n --env=test
	docker exec -it tm-php rm -f .phpunit.result.cache
	docker exec -it tm-php symfony php bin/phpunit tests
queue-email:
	docker exec -it tm-php php bin/console rabbitmq:consumer send_email -vvv
queue-email-clear:
	docker exec -it tm-php php bin/console rabbitmq:purge send_email -n
