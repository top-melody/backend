<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230313213625 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Удалил createdAt из таблицы с логами';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE integration_log DROP created_at');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "integration_log" ADD created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('COMMENT ON COLUMN "integration_log".created_at IS \'(DC2Type:datetime_immutable)\'');
    }
}
