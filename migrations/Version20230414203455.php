<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230414203455 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Добавил новую версии таблицы связи трека с плейлистом';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE "track_to_playlist_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "track_to_playlist" (id INT NOT NULL, track_id INT NOT NULL, playlist_id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5B65246D5ED23C43 ON "track_to_playlist" (track_id)');
        $this->addSql('CREATE INDEX IDX_5B65246D6BBD148 ON "track_to_playlist" (playlist_id)');
        $this->addSql('COMMENT ON COLUMN "track_to_playlist".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE "track_to_playlist" ADD CONSTRAINT FK_5B65246D5ED23C43 FOREIGN KEY (track_id) REFERENCES "track" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "track_to_playlist" ADD CONSTRAINT FK_5B65246D6BBD148 FOREIGN KEY (playlist_id) REFERENCES "playlist" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE "track_to_playlist_id_seq" CASCADE');
        $this->addSql('ALTER TABLE "track_to_playlist" DROP CONSTRAINT FK_5B65246D5ED23C43');
        $this->addSql('ALTER TABLE "track_to_playlist" DROP CONSTRAINT FK_5B65246D6BBD148');
        $this->addSql('DROP TABLE "track_to_playlist"');
    }
}
