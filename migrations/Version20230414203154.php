<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230414203154 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Удалил таблицу связи трека с плейлистом';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE track_to_playlist DROP CONSTRAINT FK_5B65246D5ED23C43');
        $this->addSql('ALTER TABLE track_to_playlist DROP CONSTRAINT FK_5B65246D6BBD148');
        $this->addSql('DROP TABLE track_to_playlist');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE track_to_playlist (track_id INT NOT NULL, playlist_id INT NOT NULL, PRIMARY KEY(track_id, playlist_id))');
        $this->addSql('CREATE INDEX IDX_5B65246D5ED23C43 ON track_to_playlist (track_id)');
        $this->addSql('CREATE INDEX IDX_5B65246D6BBD148 ON track_to_playlist (playlist_id)');
        $this->addSql('ALTER TABLE track_to_playlist ADD CONSTRAINT FK_5B65246D5ED23C43 FOREIGN KEY (track_id) REFERENCES "track" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE track_to_playlist ADD CONSTRAINT FK_5B65246D6BBD148 FOREIGN KEY (playlist_id) REFERENCES "playlist" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
