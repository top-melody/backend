<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230415172236 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Индексы для поиска треков';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE INDEX IDX_TRACK_NAME ON "track" (name)');
        $this->addSql('CREATE INDEX IDX_AUTHOR_NAME ON "author" (name)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX "idx_track_name"');
        $this->addSql('DROP INDEX "idx_author_name"');
    }
}
