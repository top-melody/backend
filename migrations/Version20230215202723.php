<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230215202723 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Добавления поля "Класс" для инициализации интеграции';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE integration ADD class VARCHAR(511) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "integration" DROP class');
    }
}
