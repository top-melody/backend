<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230626205426 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ошибки в OperationScan - теперь массив';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX idx_author_name');
        $this->addSql('ALTER TABLE operation_scan ADD errors JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE operation_scan DROP error');
        $this->addSql('DROP INDEX idx_track_name');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "operation_scan" ADD error VARCHAR(511) DEFAULT NULL');
        $this->addSql('ALTER TABLE "operation_scan" DROP errors');
        $this->addSql('CREATE INDEX idx_track_name ON "track" (name)');
        $this->addSql('CREATE INDEX idx_author_name ON "author" (name)');
    }
}
