<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230129175626 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Основные таблицы';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE "author_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "integration_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "integration_log_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "operation_scan_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "playlist_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "track_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_integration_config_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "author" (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BDAFD8C85E237E06 ON "author" (name)');
        $this->addSql('CREATE TABLE "integration" (id INT NOT NULL, name VARCHAR(255) NOT NULL, config JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FDE96D9B5E237E06 ON "integration" (name)');
        $this->addSql('CREATE TABLE "integration_log" (id INT NOT NULL, integration_id INT NOT NULL, type VARCHAR(127) NOT NULL, request JSON DEFAULT NULL, response JSON DEFAULT NULL, is_success BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A4AA89ED9E82DDEA ON "integration_log" (integration_id)');
        $this->addSql('COMMENT ON COLUMN "integration_log".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE "operation_scan" (id INT NOT NULL, integration_id INT NOT NULL, user_id INT DEFAULT NULL, status VARCHAR(63) NOT NULL, error VARCHAR(511) DEFAULT NULL, stack_trace JSON DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EBC3EE3F9E82DDEA ON "operation_scan" (integration_id)');
        $this->addSql('CREATE INDEX IDX_EBC3EE3FA76ED395 ON "operation_scan" (user_id)');
        $this->addSql('COMMENT ON COLUMN "operation_scan".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "operation_scan".updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE operation_scan_to_track (operation_scan_id INT NOT NULL, track_id INT NOT NULL, PRIMARY KEY(operation_scan_id, track_id))');
        $this->addSql('CREATE INDEX IDX_15BBDAF18BE62A1E ON operation_scan_to_track (operation_scan_id)');
        $this->addSql('CREATE INDEX IDX_15BBDAF15ED23C43 ON operation_scan_to_track (track_id)');
        $this->addSql('CREATE TABLE "playlist" (id INT NOT NULL, name VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN "playlist".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE "track" (id INT NOT NULL, integration_id INT NOT NULL, name VARCHAR(255) NOT NULL, duration INT NOT NULL, download_url VARCHAR(1024) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D6E3F8A69E82DDEA ON "track" (integration_id)');
        $this->addSql('COMMENT ON COLUMN "track".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE track_to_author (track_id INT NOT NULL, author_id INT NOT NULL, PRIMARY KEY(track_id, author_id))');
        $this->addSql('CREATE INDEX IDX_9AC57AA15ED23C43 ON track_to_author (track_id)');
        $this->addSql('CREATE INDEX IDX_9AC57AA1F675F31B ON track_to_author (author_id)');
        $this->addSql('CREATE TABLE track_to_playlist (track_id INT NOT NULL, playlist_id INT NOT NULL, PRIMARY KEY(track_id, playlist_id))');
        $this->addSql('CREATE INDEX IDX_5B65246D5ED23C43 ON track_to_playlist (track_id)');
        $this->addSql('CREATE INDEX IDX_5B65246D6BBD148 ON track_to_playlist (playlist_id)');
        $this->addSql('CREATE TABLE user_to_playlist (user_id INT NOT NULL, playlist_id INT NOT NULL, PRIMARY KEY(user_id, playlist_id))');
        $this->addSql('CREATE INDEX IDX_B3C04616A76ED395 ON user_to_playlist (user_id)');
        $this->addSql('CREATE INDEX IDX_B3C046166BBD148 ON user_to_playlist (playlist_id)');
        $this->addSql('CREATE TABLE "user_integration_config" (id INT NOT NULL, user_id INT NOT NULL, integration_id INT NOT NULL, config JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_89907B92A76ED395 ON "user_integration_config" (user_id)');
        $this->addSql('CREATE INDEX IDX_89907B929E82DDEA ON "user_integration_config" (integration_id)');
        $this->addSql('ALTER TABLE "integration_log" ADD CONSTRAINT FK_A4AA89ED9E82DDEA FOREIGN KEY (integration_id) REFERENCES "integration" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "operation_scan" ADD CONSTRAINT FK_EBC3EE3F9E82DDEA FOREIGN KEY (integration_id) REFERENCES "integration" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "operation_scan" ADD CONSTRAINT FK_EBC3EE3FA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE operation_scan_to_track ADD CONSTRAINT FK_15BBDAF18BE62A1E FOREIGN KEY (operation_scan_id) REFERENCES "operation_scan" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE operation_scan_to_track ADD CONSTRAINT FK_15BBDAF15ED23C43 FOREIGN KEY (track_id) REFERENCES "track" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "track" ADD CONSTRAINT FK_D6E3F8A69E82DDEA FOREIGN KEY (integration_id) REFERENCES "integration" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE track_to_author ADD CONSTRAINT FK_9AC57AA15ED23C43 FOREIGN KEY (track_id) REFERENCES "track" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE track_to_author ADD CONSTRAINT FK_9AC57AA1F675F31B FOREIGN KEY (author_id) REFERENCES "author" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE track_to_playlist ADD CONSTRAINT FK_5B65246D5ED23C43 FOREIGN KEY (track_id) REFERENCES "track" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE track_to_playlist ADD CONSTRAINT FK_5B65246D6BBD148 FOREIGN KEY (playlist_id) REFERENCES "playlist" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_to_playlist ADD CONSTRAINT FK_B3C04616A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_to_playlist ADD CONSTRAINT FK_B3C046166BBD148 FOREIGN KEY (playlist_id) REFERENCES "playlist" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user_integration_config" ADD CONSTRAINT FK_89907B92A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user_integration_config" ADD CONSTRAINT FK_89907B929E82DDEA FOREIGN KEY (integration_id) REFERENCES "integration" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE "author_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "integration_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "integration_log_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "operation_scan_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "playlist_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "track_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "user_integration_config_id_seq" CASCADE');
        $this->addSql('ALTER TABLE "integration_log" DROP CONSTRAINT FK_A4AA89ED9E82DDEA');
        $this->addSql('ALTER TABLE "operation_scan" DROP CONSTRAINT FK_EBC3EE3F9E82DDEA');
        $this->addSql('ALTER TABLE "operation_scan" DROP CONSTRAINT FK_EBC3EE3FA76ED395');
        $this->addSql('ALTER TABLE operation_scan_to_track DROP CONSTRAINT FK_15BBDAF18BE62A1E');
        $this->addSql('ALTER TABLE operation_scan_to_track DROP CONSTRAINT FK_15BBDAF15ED23C43');
        $this->addSql('ALTER TABLE "track" DROP CONSTRAINT FK_D6E3F8A69E82DDEA');
        $this->addSql('ALTER TABLE track_to_author DROP CONSTRAINT FK_9AC57AA15ED23C43');
        $this->addSql('ALTER TABLE track_to_author DROP CONSTRAINT FK_9AC57AA1F675F31B');
        $this->addSql('ALTER TABLE track_to_playlist DROP CONSTRAINT FK_5B65246D5ED23C43');
        $this->addSql('ALTER TABLE track_to_playlist DROP CONSTRAINT FK_5B65246D6BBD148');
        $this->addSql('ALTER TABLE user_to_playlist DROP CONSTRAINT FK_B3C04616A76ED395');
        $this->addSql('ALTER TABLE user_to_playlist DROP CONSTRAINT FK_B3C046166BBD148');
        $this->addSql('ALTER TABLE "user_integration_config" DROP CONSTRAINT FK_89907B92A76ED395');
        $this->addSql('ALTER TABLE "user_integration_config" DROP CONSTRAINT FK_89907B929E82DDEA');
        $this->addSql('DROP TABLE "author"');
        $this->addSql('DROP TABLE "integration"');
        $this->addSql('DROP TABLE "integration_log"');
        $this->addSql('DROP TABLE "operation_scan"');
        $this->addSql('DROP TABLE operation_scan_to_track');
        $this->addSql('DROP TABLE "playlist"');
        $this->addSql('DROP TABLE "track"');
        $this->addSql('DROP TABLE track_to_author');
        $this->addSql('DROP TABLE track_to_playlist');
        $this->addSql('DROP TABLE user_to_playlist');
        $this->addSql('DROP TABLE "user_integration_config"');
    }
}
