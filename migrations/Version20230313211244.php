<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230313211244 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Пересмотрел поля таблицы для логов';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE integration_log ADD errors JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE integration_log ADD request_url VARCHAR(511) DEFAULT NULL');
        $this->addSql('ALTER TABLE integration_log ADD request_headers JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE integration_log ADD response_headers JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE integration_log ADD execution_start_date_time TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE integration_log ADD execution_end_date_time TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE integration_log ALTER request TYPE TEXT');
        $this->addSql('ALTER TABLE integration_log ALTER response TYPE TEXT');
        $this->addSql('COMMENT ON COLUMN integration_log.execution_start_date_time IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN integration_log.execution_end_date_time IS \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "integration_log" DROP errors');
        $this->addSql('ALTER TABLE "integration_log" DROP request_url');
        $this->addSql('ALTER TABLE "integration_log" DROP request_headers');
        $this->addSql('ALTER TABLE "integration_log" DROP response_headers');
        $this->addSql('ALTER TABLE "integration_log" DROP execution_start_date_time');
        $this->addSql('ALTER TABLE "integration_log" DROP execution_end_date_time');
        $this->addSql('ALTER TABLE "integration_log" ALTER request TYPE JSON');
        $this->addSql('ALTER TABLE "integration_log" ALTER response TYPE JSON');
    }
}
