### Адрес Swagger
http://api.top-melody.local/v1/doc

### Адрес MailHog Management
http://localhost:8026/

### RabbitMQ Management
http://localhost:15672/
login: top-melody
password: top-melody

### hosts
127.0.0.1 api.top-melody.local # Top Melody Backend

### Сборка контейнера
```shell
make build
```

### Запуск контейнера для разработки
```shell
make start
```

### Остановка контейнера
```shell
make stop
```

### Установка composer-зависимостей
```shell
make composer-install
```

### Просмотр активных контейнеров проекта
```shell
docker ps -a | grep tm-*
```

### Просмотр логов по контейнеру nginx
```shell
docker logs tm-nginx
```

### Сброс кеша
```shell
make cache-clear
```

### Создать миграцию
```shell
make migration
```

### Применить миграции
```shell
make migrate
```

### Откатить миграцию
```shell
make migrate-down
```

### Создать миграцию по модели
```shell
make migrate-diff
```

### Генерация ключей JWT авторизации
```shell
docker exec -it tm-php php bin/console lexik:jwt:generate-keypair
```

### Автоформатирование кода через PHP CS Fixer
```shell
make code-fix
```

### Настройка RabbitMQ
```shell
docker exec -it tm-rabbitmq rabbitmqctl set_user_tags top-melody administrator
```

### Запуск исполнителя отправки почты
```shell
docker exec -it tm-php php bin/console rabbitmq:consumer send_email -vvv
```
